#! /usr/bin/perl -w
use utf8;
use encoding 'utf8';
use open ':utf8'; # input and output default layer will be UTF-8
my $subtree="book";
mkdir $subtree;
mkdir "${subtree}/+";
my $digits=2;
my ($t,$s);
my $item_ndone=0;
my @stack;
my ($dir,$fn);
my ($n,$old_N)=(0,0);
my %rr;
open TOC, ">${subtree}/+/1";
push @stack, sprintf("%0${digits}d",0);

while(<>) {
chomp;
if (/^\s*$/) {

} elsif (/^\@(!+)$/) {
  $depth=length($1)-1;
  while($#stack>$depth) {pop @stack;}
} elsif (/^\@(!+) (.*)\s*$/) {
  $title=$2;
  $depth=length($1)-1;
  while($#stack>$depth) {pop @stack;}
  $i=pop @stack; $i+=1; push @stack,sprintf("%0${digits}d", $i);
  $fn=join '/', @stack; $fn=~s/\/$//;
  mkdir "${subtree}/${fn}";
    printf TOC "%s\t5\t%s\n", $fn, $title;
  push @stack,sprintf("%0${digits}d", 0);
  $last_title=$title;
  close I or $p=0;
  $new_chapter=1;
} elsif (/^\@\* (.*)\s*$/) {
  $new_chapter=0;
  $title=$1;
  $i=pop @stack; $i+=1; push @stack,sprintf("%0${digits}d", $i);  
  $fn=join '/', @stack; $fn=~s/\/$//;
    close I or $p=0;
    open I, ">${subtree}/${fn}" or die;
    printf TOC "%s\t2\t%s\n", $fn, $title;
    printf I "%s\n\n", $title;
} else {
  $s=$_;
  #printf I "%s\n\n", $s or die "trying to print[$s] after [$last_title]\n";
  printf I "%s\n\n", $s or fix_it();
  $item_ndone=0;
}


}
close I;
close TOC;
sub fix_it(){
  printf("fixing\n");
  if (not $new_chapter) {die "trying to print[$s] after [$last_title]\n";}
  $new_chapter=0;
  $title=$last_title;
  $i=pop @stack; $i+=1; push @stack,sprintf("%0${digits}d", $i);  
  $fn=join '/', @stack; $fn=~s/\/$//;
    close I or $p=0;
    open I, ">${subtree}/${fn}" or die;
    printf TOC "%s\t2\t%s\n", $fn, $title;
    printf I "%s\n\n%s\n\n", $title,$s;
 return 0;
}
