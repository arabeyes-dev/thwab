#! /usr/bin/perl -w
# a script to import books (not tafseer) from http://www.shamela.ws (release 1)
# Copyright (c) 2007, Arabeyes.org by Muayyad Alsadi<alsadi@gmail.com>
# Released under terms of GPL 2

# open it in MS Access then export the book and it's TOC into two xml files then type
# rm -r book; extract-shamela1.pl book-toc.xml book.xml digits
# where digits any number >= 2 (it then suggests the best number)
# then enter book/+/ and create 0 info file
# run the th-gen-stub-search-cache script inside book
# then run th-pack

use utf8;
use encoding 'utf8';
use open ':utf8'; # input and output default layer will be UTF-8

use XML::Parser;
my $subtree="book";
my $maxlen=2;
my $toc_xml=shift or die "no toc argument";
my $book_xml=shift or die "no book argument";
my $digits=shift or die "no digits argument";
my ($toc_id,$last_toc_id, $toc_title, $toc_lvl, $tmp)=(-1, -1, "", -1, "");
my @toc_stack; # file	type	title
my %toc_by_id;
my @stack;
push @stack,sprintf("%0${digits}d", 0);
my $is_started=0;
my ($ref1,$ref2, $last_ref1, $last_ref2)= (-1,-1,-1,-1);
my ($txt, $last_fn)=("","");
my $file;
my $out;
my $myref=\$tmp;
mkdir $subtree or die "could not create subtree";
mkdir "${subtree}/+" or die "could not create subtree";
open TOC, ">${subtree}/+/1";
open REF, ">${subtree}/+/2";
my $toc_p = new XML::Parser(ErrorContext  => 2,
			Namespaces    => 1,
			ParseParamEnt => 1,
			Handlers      => {Start => \&toc_start_h,
					  End   => \&toc_end_h,
					  Char  => \&toc_chr_h,
					  #Proc  => \&proc_h,
					  Comment => \&comment_h
					 }
			);
my $p = new XML::Parser(ErrorContext  => 2,
			Namespaces    => 1,
			ParseParamEnt => 1,
			Handlers      => {Start => \&start_h,
					  End   => \&end_h,
					  Char  => \&chr_h,
					  #Proc  => \&proc_h,
					  Comment => \&comment_h
					 }
			);
print "Preparing TOC:\n";
$toc_p->parsefile($toc_xml);
print "Preparing TOC: Done\n";
for $i (@toc_stack) {print TOC "$i\n";}
#exit(-1);
print "Creating files :\n";
$p->parsefile($book_xml);
print "Creating files : done\n";
close TOC;
close REF;
printf "digits should be %d\n",$maxlen;
system("LC_ALL=C sort ./${subtree}/+/1 >./${subtree}/+/1.tmp && mv ./${subtree}/+/1.tmp ./${subtree}/+/1 || echo 'could not sort TOC file'");
system("LC_ALL=C sort ./${subtree}/+/2 >./${subtree}/+/2.tmp && mv ./${subtree}/+/2.tmp ./${subtree}/+/2 || echo 'could not sort REF file'");
#############
################
## End main
################
sub get_attr {
	my $a=shift;
	my @attr= @_;
	my $i;
	for ($i=0;$i<=$#attr;$i+=2) {
		if ($a eq $attr[$i]) {return $attr[$i+1];}
	}
	return 0;
}
sub toc_start_h {
  my $xp = shift;
  my $el = shift;
  my @attr = @_; 
  if ($el eq "dataroot" or $el=~/t\d+/o) { $myref=\$tmp; }
  elsif ($el eq "id") { $myref=\$toc_id; }
  elsif ($el eq "title") { $myref=\$toc_title;}
  elsif ($el eq "lvl") { $myref=\$toc_lvl;}
  else { $myref=\$tmp; print "unknown tag [$el]\n"}
  $$myref="";
}
sub toc_end_h {
  my ($xp, $el) = @_;
  if ($el=~/t\d+/o) {
    if ($#stack<$toc_lvl-1) {
      @a=split /\t/,$toc_stack[$#toc_stack];
      $toc_stack[$#toc_stack]=sprintf("%s\t%d\t%s",$a[0],5,$a[2]);
      mkdir $subtree."/".$a[0];
      # print "x$toc_stack[$#toc_stack]\n";
      if (not exists($toc_by_id{$toc_id})) {
        push @stack,sprintf("%0${digits}d", 0);
	$fn=join '/', @stack; $fn=~s/\/$//;
	$i=push @toc_stack, sprintf("%s\t%d\t%s",$fn,1,'_');
	$toc_by_id{$last_toc_id}=$i-1;
	#printf "_: i=%d \$\#=%d TOC=[%s]\n", $i, $#toc_stack, $toc_stack[$i-1];
	#printf "_: id=%d last_id=%d i=%d TOC=[%s]\n",$toc_id, $last_toc_id, $toc_by_id{$last_toc_id}, $toc_stack[$toc_by_id{$last_toc_id}];
	#print "adding id=$toc_id\n";
      } else { push @stack,sprintf("%0${digits}d", 0); }
    } elsif (exists($toc_by_id{$toc_id})) { # warn about two titles for same ID
      #print "adding id=$toc_id\n";
      printf "warning: id=%d exists, new titled=[%s], old value:\nX%s\n",$toc_id,$toc_title,$toc_stack[$toc_by_id{$toc_id}];
      #$i=$toc_by_id{$toc_id};
      #@a=split /\t/,$toc_stack[$i];
      #$toc_stack[$i]=sprintf("%s\t%d\t%s",$a[0],5,$a[2]);
      #mkdir $subtree."/".$a[0];
      #push @stack,sprintf("%0${digits}d", 0);
    }
    while($#stack>=$toc_lvl) {pop @stack;}
    $i=pop @stack; $i+=1; push @stack,sprintf("%0${digits}d", $i);
    $fn=join '/', @stack; $fn=~s/\/$//;
    push @toc_stack, sprintf("%s\t%d\t%s",$fn,2,$toc_title);
    if (length($i)>$maxlen) {$maxlen=length($i)}
    #print "$toc_stack[$#toc_stack]\n";
    $toc_by_id{$toc_id}=$#toc_stack;
    #print "adding id=$toc_id\n";
    $last_toc_id=$toc_id;
  }
  $myref = \$tmp;
}  # End endhndl

sub toc_chr_h {
  my ($xp, $data) = @_;
    chomp $data;
    #$data=~s/\r/\n/g;
    $data=~s/#/\n\n/g;
    $data=~s/A/صلى الله عليه وسلم/g;
    $data=~s/B/رضي الله عن/g;
    $data=~s/C/رحمه الله/g;
    $data=~s/D/عز وجل/g;
    $data=~s/E/عليه الصلاة والسلام/g;
    $$myref.=$data;
}  # End chrhndl

###############
sub start_h {
  my $xp = shift;
  my $el = shift;
  my @attr = @_; 
  if ($el eq "dataroot" or $el=~/b\d+/o) { $myref=\$tmp; }
  elsif ($el eq "part") { $myref=\$ref1; }
  elsif ($el eq "page") { $myref=\$ref2; }
  elsif ($el eq "id") { $myref=\$toc_id; }
  elsif ($el eq "nass") { $myref=\$txt; $txt="";}
  else { $myref=\$tmp; print "unknown tag [$el]\n"}
  $$myref="";
}

sub end_h {
  my ($xp, $el) = @_;
  if ($el=~/b\d+/o) {
    if (exists($toc_by_id{$toc_id})) {
      $i=$toc_by_id{$toc_id};
      @a=split /\t/,$toc_stack[$i];
      $last_fn=$fn=$a[0];
      #print "${subtree}/${fn}\n";
      open I, ">${subtree}/${fn}" or die "could not open for id=$toc_id";
      # print "\tOK\n";
      if ($last_ref1!=$ref1 || $last_ref2!=$ref2) {print REF "$fn\t$ref1:$ref2\n";}
    } else {
      $fn=$last_fn;
      open I, ">>${subtree}/${fn}" or die "could not open";
    }
    print I "$txt\n";
    close I;
  }
  $myref = \$tmp;
}  # End endhndl

sub chr_h {
  my ($xp, $data) = @_;
    chomp $data;
    $data=~s/#/\n\n/g;
    $data=~s/A/صلى الله عليه وسلم/g;
    $data=~s/B/رضي الله عن/g;
    $data=~s/C/رحمه الله/g;
    $data=~s/D/عز وجل/g;
    $data=~s/E/عليه الصلاة والسلام/g;
    $$myref.=$data;
}  # End chrhndl
sub comment_h {
}
sub proc_h {
  my ($xp, $target, $data) = @_;

  unless ($indoctype) {
    #print "<?$target $data?>";
    #print "\n" unless $inroot;
  }
}
