#! /usr/bin/python
"""LibITAR - routines to access compressed indexed TAR archives
Random access compressed TAR files
	by Muayyad Saleh Alsadi<alsadi@gmail.com>
	Released under terms of GNU GPL

 The archive is compatible with GZ/BZiped TAR format
 and can be extracted (or listed...etc) with unaware usual tools
 but it generate an index file which makes it very fast to extract
 a random file at a random position

This module is still under heavily development, so please only use the
constructor and member_contents(), every thing else may change
"""
import bz2
import os
from os.path import *
from struct import *
class iTar:
    """Access compressed indexed TAR archives"""
    def __init__(self, file, restore_t=None):
        """New iTar extractor, wehre
file: is a .tar.bz2 file name having a .tar.bz2.i index in the same directory
restore_t: usually ommited, but set it to the tuple returned by get_state() to restore state
"""
        self.file = file
        self.fh = open(file)
	self.chunk=''
	self.members={} # chunk members
	self.files=() # member files
	self._blank='\0'*512 # TAR EOF
	idx=file+".i"
	if restore_t and file==restore_t[0]: self.set_state(restore_t)
	elif isfile(idx):
		try: f = open(idx)
		except: f=None
		a=f.readlines(); n=len(a)
		for i in range(n):
			a[i]=a[i].rstrip()
			if (not a[i]): n=i
		b=a[0:n]; a=a[n+1:]; self.files=tuple(a);
		for i in range(len(b)): b[i]=b[i].split('\t'); b[i][0]=int(b[i][0],16); b[i][1]=int(b[i][1],16); # if base is to be autodetected use int(..,0)
		self.idx=b
		if f: f.close()
    def __del__(self):
        self.fh.close()
	self.chunk=''
	self.members={} # chunk members
	self.files=() # member files

    def get_chunk_by_file(self,fn):
        """return a tuple having (offset, i, fn_key,fn_next_key,size) having the file"""
	i=None; j=None
	for i in self.idx:
	    if i[2]>fn: break
	    j=i
	if j:
            if i[2]==j[2]: j.append(None); j.append(-1)
            else: j.append(i[2]); j.append(i[0]-j[0])
            j=tuple(j)
        return j
    def _load_chunk(self,off,size):
        """load and decompress a chunk by offset and size"""
        self.fh.seek(off); cchunk=self.fh.read(size)
        self.chunk=bz2.decompress(cchunk)
        self.last_off=off
    def _parse_header(self,h):
        try:
	    l=list(unpack('100s 8s 8s 8s 12s 12s 8s c 100s 8s 32s 32s 8s 8s 155s 12x',h))
            for i in (0,1,2,3,4,5,6,8,9,10,11,12,13,14): l[i]=l[i][:l[i].find('\0')]
            for i in (1,2,3,4,5,6,12,13): l[i]=int(l[i],8)
            return tuple(l)
	except:
            return None
#chunk[off:off+size] next_off=off+512+size+(512-size)%512
    def _parse_chunk(self):
        """Parse entire chunk"""
	d={}; i=0; l=len(self.chunk)
	while (i<l):
            h=self.chunk[i:i+512]
	    if h==self._blank: break
	    H=self._parse_header(h)
	    if H[7]=='0' or H[7]=='\0':
                nxt=i+512+H[4]+(512-H[4])%512
		d[H[0]]=(i+512,H[4])
	    else: nxt=i+512
	    i=nxt
	    self.members=d
    def get_state(self):
        """Return a tuple that is used with set_state"""
	return (self.file, self.idx, self.chunk, self.members, self.files)
    def set_state(self,t):
        """Restore  state from the tuple t that was obtained with get_state"""
	if (self.file==t[0]):
            self.file, self.idx, self.chunk, self.members, self.files=t
	    if (self.chunk and not self.members): self._parse_chunk()
    def member_contents(self,fn):
        """Return contents of a member file"""
	if (self.files and not fn in self.files): return None
	if (fn in self.members.keys()): o,s=self.members[fn]; return self.chunk[o:o+s]
	else:
	    c=self.get_chunk_by_file(fn)
	    self._load_chunk(c[0],c[4])
	    self._parse_chunk()
	    if (fn in self.members.keys()): o,s=self.members[fn]; return self.chunk[o:o+s]
	    else: return None
