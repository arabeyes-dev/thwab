/* Thwab-lib: by Muayyad Alsadi<alsadi@gmail.com>
 * released under terms of GNU GPL
 *
 * "in which there are valuable Books." V98:3
 *
 */
#define _GNU_SOURCE		/* 1st to get rid of non-GNU, 2nd to have strndup and alloca */
#include<stdio.h>
#include<unistd.h>
#include<sys/stat.h>
#include<gtk/gtk.h>
#include "libthwab-lib.h"
/* really needed to be global */
Thwab *th=NULL;
GtkWidget *win,*ix_dlg,*ix_tree,*e1;
GtkTooltips *tips;
GtkAccelGroup *accel_group;
/* should be members of a struct that is allocated for each viewer */
ThwabTopic *topic=NULL;
GtkWidget *txt, *toc_tree;
GtkTextBuffer *buff;
/* some needed and some are rubish */
GtkWidget *tree_,*tree;
GtkWidget *v1, *h1, *h2, *tb1, *hb, *vb, *tb, *w_tmp, *w1, *w2, *w3;
GtkPaned *hp;
GtkWidget *tools, *tools_bx, *tools_nb;
GtkWidget *xpander;
GtkExpander *expander;
GtkToolItem *ti;

void bad(GtkWindow *parent, char *msg)
{
  GtkDialog *dlg=gtk_message_dialog_new_with_markup
  (parent,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
  "<span size=\"xx-large\" color=\"darkred\">Error</span>\n%s",msg);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  gtk_widget_show_all (dlg);
  gtk_dialog_run(dlg);
  gtk_widget_destroy(dlg);
}
/*************************************
 * Here after, Splash related code *
 *************************************/
// generate a bitmap corresponding Magic Magenta
GdkBitmap *
pixbuf_gen_mask (GdkPixbuf * pixbuf)
{
  GdkBitmap *b;
  guchar *xbm, *xbm_p, *row;
  int xbm_line;
  int width, height, rowstride, n_channels;
  guchar *pixels, *p;
  int x, y;
/* another trick
GdkPixbuf*  gdk_pixbuf_add_alpha(const GdkPixbuf *pixbuf,
  TRUE,255,0,255); *//* Magic magenta */

  n_channels = gdk_pixbuf_get_n_channels (pixbuf);
  g_assert (gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
  g_assert (gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);
  g_assert (gdk_pixbuf_get_has_alpha (pixbuf));
  g_assert (n_channels == 4);

  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  xbm_line = (width + 7) >> 3;
  xbm = (guchar *) malloc (xbm_line * height);
  memset (xbm, 0, xbm_line * height);
  //g_assert (x >= 0 && x < width);
  //g_assert (y >= 0 && y < height);

  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  row = pixels = gdk_pixbuf_get_pixels (pixbuf);
  for (y = 0, xbm_p = xbm; y < height;
       ++y, row += rowstride, xbm_p += xbm_line)
    for (x = 0, p = row; x < width; ++x, p += n_channels)
      if (p[3] > 127 || (p[0] == 255 && p[1] == 0 && p[2] == 255))
	*(xbm_p + (x >> 3)) |= (1 << (x & 7));
  b = gdk_bitmap_create_from_data (NULL, xbm, width, height);
  free (xbm);
  return b;
  //p = pixels + y * rowstride + x * n_channels;
  //p[0] = red;
  //p[1] = green;
  //p[2] = blue;
  //p[3] = alpha;
}
GtkTextBuffer *splash_buff;
void splash_progress(char *title, GtkWidget *p)
{
  printf("book [%s] loaded\n",title);
  gtk_progress_bar_pulse (p);
  gtk_text_buffer_insert_at_cursor(splash_buff,"[",-1);
  gtk_text_buffer_insert_at_cursor(splash_buff,title,-1);
  gtk_text_buffer_insert_at_cursor(splash_buff,"]\n",-1);
  while (gtk_events_pending ()) gtk_main_iteration ();
  //sleep(1);
}
gboolean
splash_out(GtkWidget *splash)
{
  gtk_widget_hide (splash);
  return FALSE;
}
void
init_and_load_thwab ()
{
  GtkWidget *splash;
  GtkWidget *pixmap, *p, *txt, *fixed,*w;
  GdkPixbuf *pix;
  GdkBitmap *mask = NULL;
  GtkStyle *style;
  GError *err = NULL;
  GdkGC *gc;
  GdkCursor *cursor;
  char *splash_file;
  int width, height;
  gtk_window_set_auto_startup_notification (FALSE);
  /* splash = gtk_window_new (GTK_WINDOW_POPUP);	// GTK_WINDOW_TOPLEVEL); */
  /* gtk_window_set_position (splash, GTK_WIN_POS_CENTER); */
    /*
     gtk_window_set_keep_above(splash, TRUE);
     gtk_window_set_decorated(splash,FALSE);
    */
  splash = g_object_new(GTK_TYPE_WINDOW,
	"type",GTK_WINDOW_POPUP,"type-hint", GDK_WINDOW_TYPE_HINT_SPLASHSCREEN,
	"window-position",GTK_WIN_POS_CENTER,
	"modal",TRUE,
	/* "decorated",FALSE, */
	/*
	"default-width",640,
	"default-height",400, */
	NULL
  );

  

  /* To display the pixmap, we use a fixed widget to place the pixmap */
  fixed = gtk_fixed_new ();
  style = gtk_widget_get_default_style ();
  gc = style->black_gc;

  if ((splash_file = thwab_filename (th,"images/splash.png"))
      && (pix = gdk_pixbuf_new_from_file (splash_file, &err)))
    {
      pixmap = gtk_image_new_from_pixbuf (pix);
      width = gdk_pixbuf_get_width (pix);
      height = gdk_pixbuf_get_height (pix);
      gtk_widget_set_size_request (fixed, width, height);
      mask = pixbuf_gen_mask (pix);
      gtk_fixed_put (GTK_FIXED (fixed), pixmap, 0, 0);
    }
  if (splash_file)
    {
      free (splash_file);
      if (!pix)
	{
	  fprintf (stderr, "Error: %s\n", err->message);
	}
    }
  p = gtk_progress_bar_new ();
  txt = gtk_text_view_new ();
  w = gtk_button_new_from_stock(GTK_STOCK_OK);
  /* w = g_object_new(GTK_TYPE_BUTTON,"label",GTK_STOCK_OK,
	"use-stock", TRUE,"receives-default",TRUE,"is-focus",TRUE, NULL);*/
  gtk_widget_set_size_request (txt, 300, 100);
  gtk_widget_set_size_request (p, 150, 16);
  gtk_fixed_put (GTK_FIXED (fixed), txt, 50, 200);
  gtk_fixed_put (GTK_FIXED (fixed), p, 35, 360);
  gtk_fixed_put (GTK_FIXED (fixed), w, 35, 360);
  splash_buff = gtk_text_view_get_buffer (txt);
  gtk_text_view_set_editable (txt, FALSE);
  gtk_progress_bar_set_text (p, "Loading ...");
  gtk_progress_bar_set_fraction (p, 0.15);
  gtk_progress_bar_set_pulse_step (p, 0.17);

  gtk_container_add (GTK_CONTAINER (splash), fixed);
  gtk_widget_show (fixed);
  /* This masks out everything except for the image itself */
  if (mask)
    gtk_widget_shape_combine_mask (splash, mask, 0, 0);
  /* show the window */
  gtk_widget_show_all (splash);
  gtk_widget_show (p);
  gtk_widget_hide (w);

  /*
     gdk_drawable_get_size(p->window,&w,&h);
     gdk_window_get_position(p->window, gint *x, gint *y); */
  cursor = gdk_cursor_new (GDK_WATCH);
  gdk_window_set_cursor (splash->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending ())
    gtk_main_iteration ();
  thwab_init(th,splash_progress, p);
  gtk_text_buffer_insert_at_cursor(splash_buff,"done!\n",-1);
  gtk_widget_hide (p);
  gtk_widget_show (w);
  
  g_signal_connect_swapped (w,"clicked",G_CALLBACK (gtk_widget_hide),splash);
  while (gtk_events_pending ()) gtk_main_iteration ();
  g_timeout_add(4000,splash_out,splash);
  cursor = gdk_cursor_new (GDK_LEFT_PTR);
  gdk_window_set_cursor (splash->window, cursor);
  gdk_cursor_destroy (cursor);
  gtk_window_set_auto_startup_notification (TRUE);
  //gtk_widget_hide (splash);
}

/*************************************
 * Here after, TOC tree related code *
 *************************************/
enum { TOC_TREE_I ,TOC_TREE_ID, TOC_TREE_STR, TOC_TREE_N };
GtkWidget *
gen_toc_tree()
{
  GtkWidget *toc_tree;
  GtkTreeStore *store;
  GError *err = NULL;
  GdkPixbuf *pix;
  GtkCellRenderer *renderer,*r;
  GtkTreeViewColumn* col;
  char *fn;
  store = gtk_tree_store_new (TOC_TREE_N, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING);
  toc_tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  /* add columns */
  // title special column
  col=gtk_tree_view_column_new();
  gtk_tree_view_column_set_title  (col,"TOC");

  r = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (col, r, 0);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (col,renderer,TRUE);
  g_object_set (renderer, "xalign", 0.0, NULL);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TOC_TREE_STR);
  gtk_tree_view_column_set_attributes(col,renderer,"text", TOC_TREE_STR, NULL);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (toc_tree), col,-1);
  fn = thwab_filename (th, "images/file.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  // if (!pix) {printf("Error: %s\n",err->message);}
  free (fn);
  if (pix) g_object_set (r, "pixbuf", pix, NULL);
  fn = thwab_filename (th, "images/book-close.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-closed", pix, NULL);
  fn = thwab_filename (th, "images/book-open.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-open", pix, NULL);
  g_object_set (r, "xalign", 0.0, "xpad", 0, NULL);
  /* set tree attributes */
  gtk_tree_view_set_headers_visible (toc_tree, FALSE);
  //gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (toc_tree), TRUE); // it look nicer 
  gtk_tree_view_set_enable_search (toc_tree, TRUE);
  gtk_tree_view_set_search_column (toc_tree, TOC_TREE_STR);
  return toc_tree;
}
void
add_toc_item(GtkTreeStore *store, int i, char *id, char *title)
{
  int n,found=0;
  char *s,*s1,*s2,*s3;
  GtkTreeIter iter0,iter1;
  GtkTreeIter *iter=&iter0,*parent=NULL;
  if (id[0]=='/') ++id;
  s1=s=strdupa(id);
  if (gtk_tree_model_iter_children(store,iter, NULL))
  {
    while(*s1)
    {
      s2=strchr(s1,'/');
      if (!s2) break;
      *s2='\0';
      do
      {
        gtk_tree_model_get(store, iter, TOC_TREE_ID, &s3, -1);
        if (strcmp(s, s3)==0)
        {
          found=1;
	  free(s3);
	  iter1=*iter;
	  parent=&iter1;
          break;
        }
        free(s3);
      } while(gtk_tree_model_iter_next(store,iter));
      *s2='/';
      s1=s2+1;
      iter1=*iter;
      parent=&iter1;
      gtk_tree_model_iter_children(store,iter, parent);
    }
  }
  gtk_tree_store_append (store, iter, parent);
  gtk_tree_store_set (store, iter, TOC_TREE_I,i,TOC_TREE_ID,id,TOC_TREE_STR,title,-1);
}
void
gen_sample_toc(GtkWidget *toc_tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);  
  gtk_tree_store_clear (store);
  add_toc_item(store, 0, "01", "Chapter 1");
  add_toc_item(store, 1, "01/01", "Section 1.1");
  add_toc_item(store, 2, "01/02", "Section 1.2");
  add_toc_item(store, 3, "01/03", "Section 1.3");
  add_toc_item(store, 4, "02", "Chapter 2");
  add_toc_item(store, 5, "02/01", "Section 2.1");
  add_toc_item(store, 6, "02/02", "Section 2.2");
  add_toc_item(store, 7, "02/02/01", "Subsection 2.2.1");
  add_toc_item(store, 8, "02/02/02", "Subsection 2.2.2");
  add_toc_item(store, 9, "02/03", "Section 2.3");
}
/******************************************
 * Here after, Index dialog and tree code *
 ******************************************/
enum { TH_IX_ID, TH_IX_TITLE,TH_IX_SUBTITLE, TH_IX_AUTHORS, TH_IX_YEAR, TH_IX_GPG, TH_IX_SOURCE ,TH_IX_N };
enum { TH_IX_C_TITLE,TH_IX_C_SUBTITLE, TH_IX_C_AUTHORS, TH_IX_C_YEAR, TH_IX_C_GPG, TH_IX_C_SOURCE ,TH_IX_C_N };
GtkWidget *
gen_index_tree()
{
  GtkWidget *w, *tree;
  GtkTreeStore *store;
  GError *err = NULL;
  GdkPixbuf *pix;
  GtkCellRenderer *renderer,*r;
  GtkTreeViewColumn* col;
  int i;
  char *fn;
  
  store = gtk_tree_store_new (TH_IX_N, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN, G_TYPE_STRING);
  tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  // title special column
  col=gtk_tree_view_column_new();
  gtk_tree_view_column_set_title  (col,"Title");

  r = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (col,r,0);
  
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (col,renderer,TRUE);
  g_object_set (renderer, "xalign", 0.0, NULL);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_TITLE);
  gtk_tree_view_column_set_attributes(col,renderer,"text", TH_IX_TITLE, NULL);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (tree), col,-1);
  // the rest of columns
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "Subtitle", renderer, "text", TH_IX_SUBTITLE, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "Authors", renderer, "text", TH_IX_AUTHORS, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "Date", renderer, "text", TH_IX_YEAR, NULL);
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_GPG);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "Valid", renderer, "active", TH_IX_GPG, NULL);
  renderer = gtk_cell_renderer_pixbuf_new();
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_SOURCE);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "@", renderer, "stock-id", TH_IX_SOURCE, NULL);
  /* add icon to col #1 */
  fn = thwab_filename (th, "images/book-close.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  // if (!pix) {printf("Error: %s\n",err->message);}
  free (fn);
  if (pix) g_object_set (r, "pixbuf", pix, NULL);
  fn = thwab_filename (th, "images/drawer-close.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-closed", pix, NULL);
  fn = thwab_filename (th, "images/drawer-open.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-open", pix, NULL);
  g_object_set (r, "xalign", 0.0, "xpad", 0, NULL);
  
  /* tree attributes */
  col=gtk_tree_view_get_column (tree,TH_IX_C_TITLE);
   gtk_tree_view_column_set_resizable(col,TRUE);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_TITLE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_SUBTITLE);
   gtk_tree_view_column_set_resizable(col,TRUE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_AUTHORS);
   gtk_tree_view_column_set_resizable(col,TRUE);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_AUTHORS);
  col=gtk_tree_view_get_column (tree,TH_IX_C_YEAR);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_YEAR);
   gtk_tree_view_set_headers_visible (tree, TRUE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_YEAR);
  // hide extra columns
  for (i=TH_IX_C_SUBTITLE+1;i<TH_IX_C_N-1;++i)
  {
    col=gtk_tree_view_get_column (tree, i);
    gtk_tree_view_column_set_visible(col,FALSE);
  }
  //gtk_tree_view_set_headers_clickable(tree,TRUE);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (tree), TRUE);
  gtk_tree_view_set_enable_search (tree, TRUE);
  gtk_tree_view_set_search_column (tree, TH_IX_TITLE);
  
  return tree;
}
void
get_or_mk_index_class(GtkTreeStore *store, GtkTreeIter *parent, GtkTreeIter *iter, char *classify)
{
  int n,found=0;
  char *s,*str;
  GtkTreeIter iter0=*iter;
  s=strchr(classify,':');
  printf(">(%s)[%s]",classify,s);
  if (s!=NULL) {*s='\0';  printf("<%s>\n",s+1);}
  if (!gtk_tree_model_iter_children(store,iter, parent))
  {
    gtk_tree_store_append (store, iter, parent);
    gtk_tree_store_set (store, iter, TH_IX_TITLE,classify,-1);
    iter0=*iter;
    if (s) get_or_mk_index_class(store, iter, &iter0, s+1);
  } else // search children for class
  {
    do
    {
      gtk_tree_model_get(store, iter, TH_IX_TITLE, &str, -1);
      if (strcmp(classify, str)==0)
      {
        found=1;
        if (s) get_or_mk_index_class(store, iter, &iter0, s+1);
	free(str);
        break;
      }
      free(str);
    } while(gtk_tree_model_iter_next(store,iter));
    if (! found)
    {
      gtk_tree_store_append (store, &iter0, parent);
      gtk_tree_store_set (store, &iter0, TH_IX_TITLE,classify,-1);
      *iter=iter0;
      if (s) get_or_mk_index_class(store, iter, &iter0, s+1);
    }
  }
  *iter=iter0; // set iter to the iter we found or created
}
void
mk_new_index_class(GtkTreeStore *store, GtkTreeIter *parent, GtkTreeIter *iter, char *classify)
{
    char *s=classify;
    GtkTreeIter iter0=*iter;
    s=strchr(classify,':');
    printf("ix_new Called [%s]\n",classify);
    if (s!=NULL) {*s='\0';}
    gtk_tree_store_append (store, iter, parent);
    gtk_tree_store_set (store, iter, TH_IX_TITLE,classify,-1);
    iter0=*iter;
    if (s) get_or_mk_index_class(store, iter, &iter0, s+1);
    *iter=iter0; // set iter to the iter we found or created
}

void 
index_add(GtkTreeStore *store, char *classify, int id,char *title, char *subtitle,
	char *authors,int year,int gpg,char *source)
{
  GtkTreeIter iter,iter0;
  printf("@%s\n",title);
  if (gtk_tree_model_get_iter_first(store, &iter)==FALSE)
  {
    printf("@![%s]\n",classify);
//     gtk_tree_store_append (store, &iter, NULL);	/* Acquire a top-level iterator */
//     gtk_tree_store_set (store, &iter, TH_IX_TITLE,classify,-1);
    mk_new_index_class(store, NULL, &iter, classify);
  } else get_or_mk_index_class(store, NULL, &iter, classify);
  gtk_tree_store_append (store, &iter0, &iter);
  iter=iter0;
  gtk_tree_store_set(store, &iter, TH_IX_ID,id,TH_IX_TITLE, title, TH_IX_SUBTITLE, subtitle,TH_IX_AUTHORS,authors,TH_IX_YEAR,year, TH_IX_SOURCE, source,-1);
}
gboolean
gen_index_cb(gpointer key, ThwabTopicItem *item, GtkTreeStore *store)
{
  char *p=item->classify,*s,*s1=NULL;
  while (1)
  {
    s = strchr (p, ',');
    if (!s) break;
    s1 = strndup (p, s - p);
    printf("@@%s\n",item->title);
    index_add(store, s1,item->id, item->title, item->subtitle,
      item->authors,0,0,GTK_STOCK_HARDDISK);
    p=s+1;
    free(s1);
  }
  printf("@@%s\n",item->title);
  index_add(store, p, item->id,item->title, item->subtitle,
    item->authors,0,0,GTK_STOCK_HARDDISK);
  return FALSE;
}
void
gen_index(GtkWidget *tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(tree);
  gtk_tree_store_clear (store);
  thwab_foreach_topic(th, gen_index_cb,store);
}
void
ix_toggle_details(GtkToggleButton *b1, GtkWidget *w)
{
  GtkTreeViewColumn* col;
  int i,j=gtk_toggle_button_get_active (b1);
  // hide extra columns
  for (i=TH_IX_C_SUBTITLE+1;i<TH_IX_C_N-1;++i)
  {
    col=gtk_tree_view_get_column (w, i);
    gtk_tree_view_column_set_visible(col,j);
  }

}

GtkDialog*
index_dlg_new()
{
  int i,j;
  GtkWidget *hb,*b1,*w1,*w2;
  GtkTreeStore *store;
  GtkDialog *dlg=gtk_dialog_new_with_buttons("Index",win, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
  GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,NULL);
  gtk_widget_set_size_request (dlg, 400, 400);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  hb = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start ((GTK_DIALOG(dlg)->vbox), hb, FALSE, FALSE, 0);
  b1=gtk_toggle_button_new_with_label("Details");
  gtk_button_set_focus_on_click   (b1,FALSE);
  gtk_toggle_button_set_mode(b1,TRUE);
  gtk_box_pack_start (hb, b1, FALSE, FALSE, 0);
  // tree
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  w2 = gen_index_tree();
  gen_index(w2); //gen_sample_index(w2);
  gtk_tree_view_expand_all(w2);
  store=(GtkTreeStore *)gtk_tree_view_get_model(w2);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start ((GTK_DIALOG(dlg)->vbox), w1, TRUE, TRUE, 0);
  ix_tree=w2;
  g_signal_connect (b1,"toggled",ix_toggle_details,w2);
  return dlg;
}

void book_toc_cb(int i,const char *id,const char *title,GtkTreeStore *store)
{
  add_toc_item(store, i, id, title);
}
void
open_book(int id)
{
  GtkDialog *dlg;
  ThwabTopicItem* item=thwab_get_topic_item(th,id);
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);
  if (topic) thwab_topic_destroy(topic);
  topic=thwab_topic_new(th, item);
  gtk_tree_store_clear (store);

  dlg=gtk_message_dialog_new_with_markup(win,
    GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_NONE,
  "<span size=\"xx-large\" color=\"darkblue\">Please wait..</span>\nGenerating Table of contents");
  gtk_widget_show_all (dlg);
  while (gtk_events_pending ()) gtk_main_iteration ();  
  thwab_topic_foreach(topic, book_toc_cb, store);
  gtk_tree_view_expand_all(toc_tree);
  gtk_widget_destroy(dlg);
  
  gtk_text_buffer_set_text(buff, item->comment,-1);
  
}

void
index_cb()
{
  int i,j;
  char *s;
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(ix_tree);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeViewColumn *focus_column;
  do
  {
    j=1;
    gtk_widget_show_all (ix_dlg);
    i=gtk_dialog_run(ix_dlg);
    if (i==GTK_RESPONSE_ACCEPT)
    {
      gtk_tree_view_get_cursor(ix_tree, &path, &focus_column);
      if (!path) bad(ix_dlg, "Nothing Selected");
      else if (! gtk_tree_model_get_iter(store,&iter,path) ||
         gtk_tree_model_iter_has_child(store, &iter)) bad(ix_dlg, "Select a book!");
      else {
        gtk_tree_model_get (store, &iter, TH_IX_ID, &i, TH_IX_TITLE,&s,-1);
        printf("[%d][%s] OK\n",i,s); j=0;
	open_book(i);
	printf("[ok]\n");
	free(s);
	}
      gtk_tree_path_free(path);
    } else
    { // cancel
      j=0;
    }
  } while(j);
  //gtk_widget_destroy(dlg);
}

void
open_text_cb(GtkTreeView *toc_tree, gpointer user)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeViewColumn *focus_column;
  
  int i,l;
  char *s;
  gtk_tree_view_get_cursor(toc_tree, &path, &focus_column);
  if (!path || ! gtk_tree_model_get_iter(store,&iter,path) ) return;
  else
  {
    gtk_tree_model_get (store, &iter, TOC_TREE_I, &i,-1);
    printf("opening\n");
    s=thwab_topic_content(topic, i,&l);
    if (!s) return;
    gtk_text_buffer_set_text(buff, "Please Wait...",-1);
    while (gtk_events_pending ()) gtk_main_iteration ();
    gtk_text_buffer_set_text(buff, s, l);
    free(s);
    
  }
}
void
font_cb(GtkFontButton *fb, gpointer user)
{
  const char *fn=gtk_font_button_get_font_name  (fb);
  PangoFontDescription *font_desc = pango_font_description_from_string (fn);
  printf("new font [%s]\n",fn);
  gtk_widget_modify_font (txt, font_desc);
  pango_font_description_free (font_desc);
}
void xpander_cb(GtkToggleButton *tb, GtkWidget *w)
{
  int W1=0, H1=0,W=0;
  GtkRequisition r;
  gtk_widget_size_request(tb,&r);
  W1=r.width;
  W=(gtk_toggle_button_get_active(tb))?W1*8:W1;
  g_object_set(G_OBJECT(w),"visible", gtk_toggle_button_get_active(tb), NULL);
  GtkPaned *p=gtk_widget_get_parent (gtk_widget_get_parent (gtk_widget_get_parent (xpander)));
  gtk_paned_set_position(p,W);
}
void lookupkey_cb(GtkButton *b, gpointer   user)
{
    char *s,*id;
    int l;
    const char *k=gtk_entry_get_text(e1);
    id=thwab_topic_lookup_key(topic, k);
    if (!id) return;
    s=thwab_topic_content_by_id(topic, id,&l);
    if (!s) return;
    gtk_text_buffer_set_text(buff, "Please Wait...",-1);
    while (gtk_events_pending ()) gtk_main_iteration ();
    gtk_text_buffer_set_text(buff, s, l);
    free(s);
}
void clear_find_e_cb(GtkWidget *b,GtkWidget *e)
{
  gtk_entry_set_text(e,"");
}
int
main (int argc, char *argv[])
{
  char *fn;
  PangoFontDescription *font_desc;
  
  gtk_init (&argc, &argv);
  th=thwab_new(argv[0]);
  //prefix = get_prefix (argv[0]);
  init_and_load_thwab ();
  /* allocate the GUI members */
  /* win= gtk_window_new (GTK_WINDOW_TOPLEVEL); */
  /* gtk_widget_set_size_request (win, 640, 400); */
  GError *err=NULL;
  fn = thwab_filename (th, "images/thwab-lib.png");
  gtk_window_set_default_icon_from_file(fn,&err);
  free (fn);
  win = g_object_new(GTK_TYPE_WINDOW,
	"type",GTK_WINDOW_TOPLEVEL,
	"title","Thwab Library",
	/*"icon",icon, */
	"default-width",640,
	"default-height",400,
	"urgency-hint",TRUE,
	NULL
  );
  g_signal_connect (G_OBJECT (win), "destroy", G_CALLBACK (gtk_main_quit),
		    NULL);
  
  tips = gtk_tooltips_new ();
  accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW (win), accel_group);

  txt = gtk_text_view_new ();
  gtk_text_view_set_border_window_size (txt, GTK_TEXT_WINDOW_TOP, 1);
  gtk_text_view_set_wrap_mode (txt, GTK_WRAP_WORD_CHAR);
  buff = gtk_text_view_get_buffer (txt);

  vb = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (win), vb);
  tools = gtk_toolbar_new ();
  gtk_box_pack_start (vb, tools, FALSE, FALSE, 0);
  gtk_toolbar_set_show_arrow (tools, TRUE);
  gtk_toolbar_set_tooltips (tools, TRUE);

  ti = gtk_tool_button_new_from_stock (GTK_STOCK_INDEX);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (index_cb),NULL);
  fn = thwab_filename (th, "images/related.png");
  w1 = gtk_image_new_from_file (fn);
  free (fn);
  ti=gtk_tool_button_new(w1,"Related");
  gtk_tool_item_set_tooltip (ti, tips, "Open book related to the current one", NULL);
  gtk_toolbar_insert(tools, ti, -1);

  ti = gtk_tool_button_new_from_stock (GTK_STOCK_PROPERTIES);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_DIALOG_INFO);
  gtk_tool_button_set_label (ti, "info");
  gtk_tool_item_set_tooltip (ti, tips, "Display book infomation", NULL);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_DIALOG_AUTHENTICATION);
  gtk_tool_button_set_label (ti, "GPG");
  gtk_tool_item_set_tooltip (ti, tips, "validate book e-sign", NULL);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_CONVERT);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_CLOSE);
  gtk_toolbar_insert (tools, ti, -1);
  gtk_toolbar_insert (tools, gtk_separator_tool_item_new (), -1);

  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GOTO_FIRST);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_BACK);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_UP);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_FORWARD);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GOTO_LAST);
  gtk_toolbar_insert (tools, ti, -1);
  gtk_toolbar_insert (tools, gtk_separator_tool_item_new (), -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_PREFERENCES);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_HELP);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (gtk_main_quit),NULL);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_QUIT);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (gtk_main_quit),NULL);
  gtk_toolbar_insert (tools, ti, -1);

/*
  w_tmp=gtk_entry_new();
  ti=gtk_tool_button_new(icon_wid,"Label");
  gtk_toolbar_insert(tools,ti,-1);
  */
/*
  ti=gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(ti,FALSE);
  gtk_tool_item_set_expand(ti,FALSE);
  gtk_tool_item_set_tooltip(ti, tips,"Some tip",NULL);
  w_tmp=gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(ti),w_tmp);
  gtk_toolbar_insert(tools,ti,-1);
*/
  hb = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (vb, hb, FALSE, FALSE, 2);
  w1 = gtk_button_new();
  w2=gtk_image_new_from_stock (GTK_STOCK_CLEAR,GTK_ICON_SIZE_MENU);
  gtk_container_add(w1,w2);
  gtk_button_set_focus_on_click (w1, FALSE);
  e1 = gtk_entry_new ();
  g_signal_connect (w1,"clicked",G_CALLBACK (clear_find_e_cb),e1);
  
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 2);
  w1 = gtk_label_new ("Find");
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 2);
  gtk_box_pack_start (hb, e1, FALSE, FALSE, 2);
  /*
     w1=gtk_button_new_from_stock(GTK_STOCK_FIND);
     gtk_button_set_focus_on_click(w1,FALSE);
     gtk_box_pack_start(hb,w1,FALSE,FALSE,0);
   */
  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  w2 = gtk_image_new_from_stock (GTK_STOCK_FIND, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);

  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  w2 = gtk_image_new_from_stock (GTK_STOCK_INDEX, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);

  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  w2 = gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  fn = thwab_filename (th, "images/keys.png");
  w2 = gtk_image_new_from_file (fn);
  free (fn);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
  g_signal_connect (w1,"clicked",G_CALLBACK (lookupkey_cb),NULL);
  
  w3 = gtk_font_button_new_with_font ("Sans 16");
  gtk_box_pack_start (hb, w3, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (w3), "font-set", font_cb,NULL);
  
  //expander = gtk_expander_new (".::");
  xpander = gtk_toggle_button_new_with_label(".::");
  gtk_button_set_focus_on_click   (xpander,FALSE);
  hp = gtk_hpaned_new();
  gtk_box_pack_start (vb, hp, TRUE, TRUE, 0);
  
  v1 = gtk_vbox_new(FALSE,0);
  gtk_paned_pack1 (hp,v1,TRUE,TRUE);
  hb = gtk_hbox_new(FALSE,0);
  //h1 = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start (v1,hb, FALSE, FALSE, 0);
  gtk_box_pack_start (hb,xpander, FALSE, FALSE, 0);
  //gtk_box_pack_start (hb,h1,TRUE, TRUE, 0);
  
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_paned_pack2 (hp,w1,TRUE,TRUE);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  txt = gtk_text_view_new ();
  buff=gtk_text_view_get_buffer(txt);
  gtk_text_view_set_wrap_mode(txt,GTK_WRAP_WORD_CHAR);
  gtk_container_add (GTK_CONTAINER (w1), txt);
  
  font_desc = pango_font_description_from_string ("Sans 16");
  gtk_widget_modify_font (txt, font_desc);
  pango_font_description_free (font_desc);
  

  //gtk_expander_set_expanded (expander, TRUE);
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  toc_tree=w2=gen_toc_tree();
  gtk_container_add (GTK_CONTAINER (w1), w2);
  //gtk_container_add (GTK_CONTAINER (expander), w1);
  gtk_box_pack_start (gtk_widget_get_parent (gtk_widget_get_parent (xpander)), w1, TRUE, TRUE, 0);
  gtk_toggle_button_set_active    (xpander,TRUE);
  g_signal_connect (xpander,"toggled",G_CALLBACK (xpander_cb),w1);
  gtk_paned_set_position(hp,150);
  /*
  gen_sample_toc(w2);
  gtk_tree_view_expand_all(w2);
  */
  /* */
  g_signal_connect (G_OBJECT (toc_tree), "cursor-changed", open_text_cb,NULL);
  ix_dlg=index_dlg_new();
  //open_book(0);
  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}
// not needed any more, used for testing
/*
void
gen_sample_toc(GtkWidget *toc_tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);  
  GtkTreeIter iter1;		// Parent iter 
  GtkTreeIter iter2;		// Child iter 
  GtkTreeIter iter3;		// sub Child iter 
  
  gtk_tree_store_clear (store);
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "A. Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "B. Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "C. Chapter 3", -1);
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 3", -1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TOC_TREE_STR, "Section 3.1", -1);
  
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator 
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 3", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 3", -1);
}

void
gen_sample_index(GtkWidget *tree)
{
  GtkTreeIter iter1;		// Parent iter
  GtkTreeIter iter2;		// Child iter
  GtkTreeIter iter3;		// sub Child iter
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(tree);
  char str[]="إسلاميات:قرآنيات:تفاسير";
  gtk_tree_store_clear (store);
  index_add(store, "تجربة"
  ,"كلام للتجربة", NULL, "فلان",220,0,GTK_STOCK_NETWORK);
  
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator 
  gtk_tree_store_set (store, &iter1, TH_IX_TITLE,"لغويات",-1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TH_IX_TITLE,"معاجم",-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"لسان العرب",TH_IX_SUBTITLE,"",TH_IX_AUTHORS,"ابن منظور",TH_IX_YEAR,80, TH_IX_GPG,1,TH_IX_SOURCE, GTK_STOCK_HARDDISK,-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"المعجم الوسيط",TH_IX_SUBTITLE,"نسخة مشكولة",TH_IX_AUTHORS,"مجمع اللغة",TH_IX_YEAR,1980, TH_IX_SOURCE, GTK_STOCK_CDROM,-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"القاموس السخيف",TH_IX_SUBTITLE,"مجرد عنوان",TH_IX_AUTHORS,"مجهول",TH_IX_YEAR,2006, TH_IX_SOURCE, GTK_STOCK_NETWORK,-1);
  // gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(store),TH_IX_YEAR, GTK_SORT_DESCENDING // or GTK_SORT_ASCENDING );

  index_add(store, "نحو","كلام", NULL, "فلان",220,0,GTK_STOCK_NETWORK);
  index_add(store, str,"تفسير ابن كثير", NULL, "ابن كثير",220,0,GTK_STOCK_NETWORK);
}
*/
