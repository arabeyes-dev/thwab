/* libthwab-lib.h; header file for libthwab-lib
 *
 * Thwab-lib - Electronic Encyclopedia System
 * by Muayyad Alsadi<alsadi@gmail.com>
 * released under terms of GNU GPL
 *
 * "in which there are valuable Books." V98:3
 *
 */
/*
 * Those routines are supposed to be independent of where the resources are saved
 * this mean you don't say open this file or that as it could be extended later to
 * access remote thwab-libs or retrieve resources from removable media
 * also it should take care of cached search ..etc
 */
#ifndef _LIBTHWAB_LIB_
#define _LIBTHWAB_LIB_
/* 1st to get rid of non-GNU, 2nd to have GNU extentions */
#define _GNU_SOURCE
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<sys/stat.h>
#include <libgen.h>
#include "thwab-lib.h"

typedef struct _Thwab  Thwab;

struct _Thwab
{

};

typedef struct _ThwabTopicItem  ThwabTopicItem;
struct _ThwabTopicItem
{
  int id;
  char *ID,*file,*classify,*title,*subtitle,*authors,*computerized,*source;
  int b_year, a_year, d_year,gpg;
  char *lang,*charset,*l,*r,*key,*comment;
};
/* TODO: file should be private as it need not be a file, it could be remote object */
typedef struct _ThwabTopic  ThwabTopic;
struct _ThwabTopic
{

};
typedef void(*ThwabSearchCB)(char *id,float r, gpointer data);

/* TODO: replace thwab_new(arg0) with thwab_init(argc,argv) and thwab_init with thwab_new() like GTK*/
Thwab		*thwab_new(const char *argv0);
int		thwab_init(Thwab *thwab,void(*func)(gdouble, char *,gpointer), gpointer user);
char		*thwab_filename(Thwab *th, char *file);
//void		thwab_foreach_topic(Thwab* thwab, GTraverseFunc func, gpointer user);
ThwabTopicItem	*thwab_get_topic_item(Thwab* thwab, int i);

/*

Thwab		*thwab_new_with_progress(const char *argv0, const char *path,void(*f)(char *));
void		thwab_destroy(Thwab *th);

int		thwab_get_n_topics(Thwab *th); // returns number of topics
const ThwabTopicItem	*thwab_get_topic_info(Thwab *th, int topic); // returns number of topics
*/
/* topic */
ThwabTopic	*thwab_topic_new(Thwab* thwab, ThwabTopicItem *);
ThwabTopic	*thwab_topic_new_by_id(Thwab* thwab, int id);
ThwabTopic	*thwab_topic_new_by_title(Thwab* thwab, char *title);
//int		thwab_topic_foreach(ThwabTopic	*, void(*func)(int ,const char *,const char *,gpointer), gpointer user);
int		thwab_topic_foreach(ThwabTopic	*topic, void(*func)(int i,int ,const char *,const char *,gpointer), gpointer user);
const char	*thwab_topic_content_title_by_id(ThwabTopic	*topic, const char *id);
const char *thwab_topic_content_key_by_id(ThwabTopic *topic, const char *id);
char		*thwab_topic_content_by_id(ThwabTopic	*topic, const char *id, int *s);
char		*thwab_topic_content(ThwabTopic	*, int i, int *len);
const char *thwab_topic_lookup_key(ThwabTopic	*topic, const char *key);
void		thwab_topic_find(ThwabTopic *topic, const char **targets, int is_and, ThwabSearchCB func, gpointer data);
void		thwab_topic_destroy(ThwabTopic *);
const char *thwab_topic_get_filename(ThwabTopic *);
#endif
