/* libthwab-lib.c: library to access the Thwab-lib Encyclopedia System
 *
 * Thwab-lib - Electronic Encyclopedia System
 * by Muayyad Alsadi<alsadi@gmail.com>
 * released under terms of GNU GPL
 *
 * "in which there are valuable Books." V98:3
 *
 */
#include "libitar/libitar.h"
#include "libthwab-lib.h"
// next tree for wait fork and execl
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <time.h>
#include <zlib.h>

/////////////////////
/* move some to config header file */
/* to turn of gettext uncomment next block and comment the later block */
/*
#define _(String) (String)
#define N_(String) String
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)
*/
/*********************/
/* real gettext */
char *thwab_domain="libthwab";
#include <locale.h>
#include <libintl.h>
#define _(String) dgettext (thwab_domain,String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)
/*********************/
/////////////////////

char th_book_info_[]="+/0";
char th_book_toc_[]="+/1";
char th_book_keys_[]="+/3";
char th_book_search_[]="+/4";
char th_system_search_cache_[]="/var/cache/thwab-lib/4s/"; // perms should be 775 owned by root.admin
#define TH_BOOK_INFO th_book_info_
#define TH_BOOK_TOC th_book_toc_
#define TH_BOOK_KEYS th_book_keys_
#define TH_BOOK_SEARCH th_book_search_
char *html_generator_="Thwab v1.0";
#define TH_HTML_GENERATOR html_generator_
char *html_copyright_="<center> <span dir='ltr'><small dir='ltr'><a href='http://www.thwab.net'>Thwab.Net</a>, Copyright © 2007, Muayyad Saleh AlSadi&lt;alsadi&#64;gmail&#46;com&gt;</small></span> </center>";
#define TH_HTML_COPYRIGHT html_copyright_

typedef struct _Thwab_R_  Thwab_R_;

struct _Thwab_R_
{
  /* private */
  char *prefix;
  GTree *topics;
  int	topics_n;
  //GNode *tree;
};

typedef struct _ThwabTopicItem_R_  ThwabTopicItem_R_;
struct _ThwabTopicItem_R_
{
  int id;
  char *ID,*file,*classify,*title,*subtitle,*authors,*computerized,*source;
  int b_year, a_year, d_year,gpg;
  char *lang,*charset,*l,*r,*key,*comment;
  /* private */
  void *free_me; // free all but file
};

/* from GNU C Library */
char *
readlink_malloc (const char *filename)
{
  int size = 100;
  while (1)
    {
      char *buffer = (char *) malloc (size);
      if (!buffer)
	return NULL;
      int nchars = readlink (filename, buffer, size);
      if (nchars < size)
	return buffer;
      free (buffer);
      size *= 2;
    }
}
/* if argv0 is a link return the dir pointed by link elsewhere return dirname  */
char *
get_prefix (const char *argv0)
{
  struct stat st;
  char *s0, *s1, *prefix;
  s0 = dirname (strdup (argv0));
  /* TODO: should we use canonicalize_file_name (argv0) from GNU stdlib.h */
  if (stat (argv0, &st) == 0 && S_ISLNK (st.st_mode) &&
      (s1 = readlink_malloc (argv0)))
    {
      s1 = dirname (s1);
      if (s1[0] != '/')
	{
	  prefix = g_build_filename (s0, s1, NULL);
	  free (s1);
	  free (s0);
	  return prefix;
	}
      prefix = s1;
      free (s0);
      return prefix;
    }
  prefix = s0;
  return prefix;
}
/**
 * thwab_str_similarity: find out how similar two strings are
 * return 1.0 if equal, 0.0 if not
 * rewrite of g_strstr_len from GLib
 **/
float
thwab_str_similarity(const char *haystack, const char *needle,
	float prefix_suffix, float nonalnum, float minrank)
{
  int needle_len,haystack_len,aln1,aln2;
  const char *P,*p,*P0;
  const char *end;
  float r=1.0;
  gunichar C,c;
  
  if (haystack==NULL || needle==NULL) return 0.0;
  needle_len = strlen (needle);
  haystack_len = strlen (haystack);
  if (needle_len == 0 || haystack_len == 0 ) return 0.0;
  if (haystack_len < needle_len) return 0.0;
  P0= P = haystack;
  end = haystack + haystack_len - needle_len;
  while (*P0 && P <= end)
    {
      p=needle;
      P=P0;
      C=g_utf8_get_char(P);
      c=g_utf8_get_char(p);
      while (c && C)
        {
	if (C==c)
	 {
          P=g_utf8_next_char(P);
	  p=g_utf8_next_char(p);
	  C=g_utf8_get_char(P);
	  c=g_utf8_get_char(p);
	 }
	else
	 {
	  if ((aln1=g_unichar_isalnum(C))==(aln2=g_unichar_isalnum(c)))
	   goto next;
	  else // one is alnum and the other is not
	  {
	   if (aln2) {P=g_utf8_next_char(P); C=g_utf8_get_char(P);}
	   else {p=g_utf8_next_char(p); c=g_utf8_get_char(p);}
	   r*=nonalnum;
	   if (r<minrank) return 0.0;
	  }
	 }
	}
      if (c) // both C and c should be 0, if not one has suffix
        r=0.0;
      else if (C)
        r*=powf(prefix_suffix, ABS(g_utf8_strlen(P,-1) - g_utf8_strlen(p, -1)));
	if (r<minrank) return 0.0;
      return r;
    next:
      P0=g_utf8_next_char(P0);
      r*=prefix_suffix;
      if (r<minrank) return 0.0;
    }
  return 0.0;
}

char*
thwab_filename(Thwab *thwab, char *file)
{
  Thwab_R_ *th=(Thwab_R_ *)thwab;
  const char *p = TH_PATH;
  char *s, *s1 = NULL, *s2 = NULL;
  struct stat st;
  while (1)
    {
      s = strchr (p, G_SEARCHPATH_SEPARATOR);
      if (!s)
	break;
      s1 = strndup (p, s - p);
      if (s1[0] != '/' && s1[0] != '~')
	s2 = g_build_filename (th->prefix, s1, file, NULL);
      else
	s2 = g_build_filename (s1, file, NULL);
      printf ("Testing [%s]: ", s2);
      if (stat (s2, &st) == 0)
	break;
      printf ("skipping\n");
      p = s + 1;
      free (s2);
      s2 = NULL;
      free (s1);
      s1 = NULL;
    }
  if (s1)
    free (s1);
  printf ("\n");
  return s2;
}
////////////////////////
int strnchr2chr(char *str, int c, int to, int l); /* from libitar */
char *str_skipblank(const char *str,int l); /* from libitar */
///////////////////////
char *
str_skip_nonblank(const char *str,int l)
{
  int ch;
  while((l==-1 || l-- > 0) && (ch=*str)!=0 && !isblank(ch)) ++str;
  return (char *)str;
}
int
str_rskipblank(char *str,int l)
{
  char *s;
  int r=0;
  int ch=*str;
  while(l-->0 && ch)
  {
    if (!isblank(ch)) s=str;
    ch=*(++str);
  }
  *(s+1)='\0';
  return r;
}
int
str_blank_to_ch(char *str,int to, int l)
{
  int r=0;
  int ch=*str;
  while(l-->0 && ch)
  {
    if (isblank(ch)) {++r; *str=to;}
    ch=*(++str);
  }
  return r;
}
int
str_count_chr(char *str,int c, int l)
{
  int r=0;
  int ch=*str;
  while((l==-1 || l-- > 0) && ch)
  {
    if (ch==c) ++r;
    ch=*(++str);
  }
  return r;
}

int
parse_thwab_lib_info(ThwabTopicItem_R_ *item, char *buff, int j)
{
  itar *t;
  char *s,*s1,*s2;
  int i,J,l,n,m=0;
  n=strnchr2chr(buff, '\n', 0, j);
  s=buff;
  J=j;
  item->id=-1;
  for (i=0;i<n;++i)
  {
    if (m==1) /* get comment mode */
    { 
      *(s1+strlen(s1))='\n';
      continue;
    }
    s1=str_skipblank(s,j-1);
    s2=strchr(s1, '=');
    if (!s2)
    {
      if (*s1=='_') {item->comment=s1+strlen(s1)+1; m=1;}
      else fprintf(stderr,"bad line %d: [%s]\n",i,s1);
      continue;
    }
    *s2='\0'; l=s2-s1;
    str_rskipblank(s1,l);
    /* str_blank_to_ch(s1,'\0', l); */
    s=s2+1+strlen(s2+1)+1;
    /* printf("/next:%s/",s); */
    j=J-(buff-s);
    /* printf("next:%s\n",s); */
    /* printf("Parsed:[%s]\n",str_skipblank(s2+1,j-1)); */
    if (strncmp(s1,"version",l)==0)
    {
    } else if (strncmp(s1,"thwab",l)==0)
    {
      item->ID=str_skipblank(s2+1,j-1);
      item->id=g_str_hash(item->ID);
    } else if (strncmp(s1,"charset",l)==0)
    {
      item->charset=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"digits",l)==0)
    {
    } else if (strncmp(s1,"format",l)==0)
    {
    } else if (strncmp(s1,"lang",l)==0)
    {
      item->lang=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"key",l)==0)
    {
      item->key=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"classification",l)==0)
    {
      item->classify=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"title",l)==0)
    {
      item->title=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"subtitle",l)==0)
    {
      item->subtitle=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"author",l)==0)
    {
      item->authors=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"computerized",l)==0)
    {
      item->computerized=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"related",l)==0)
    {
    } else if (strncmp(s1,"relation",l)==0)
    {
    } else if (strncmp(s1,"L",l)==0)
    {
      item->l=str_skipblank(s2+1,j-1);
    } else if (strncmp(s1,"R",l)==0)
    {
      item->r=str_skipblank(s2+1,j-1);
    } else { printf("!!\n"); printf("Unknown option [%s]\n",s1);}
    
  }
  /* if (buff) {free(buff); buff=NULL;} */ /* not freed because item->something are just pointers use strdup to free*/
  //printf("# comment [%s]\n",item->comment);
  //printf("#\n");
  return 0;
}

////////////////////////
ThwabTopicItem *
thwab_item_new_from_cache(Thwab_R_ *th, const char *fn,FILE *cache)
{
  guint32 j,l;
  char *buff;
  ThwabTopicItem_R_ *item;
  item=(ThwabTopicItem_R_ *)malloc(sizeof(ThwabTopicItem_R_));
  if (!item) return NULL;
  memset(item,0,sizeof(ThwabTopicItem));
  item->file=fn;
  fread(&j, sizeof(j), 1, cache);
  j=GUINT32_FROM_LE(j);
  buff=malloc(j);
  fread(buff, j, 1, cache);
  item->free_me=buff;
  fprintf(stderr, "info from cache for [%s]\n",buff);
  if (strcmp(fn,buff)) {free(buff); return NULL;}
  l=strlen(buff)+1; j-=l; buff+=l;
  if (parse_thwab_lib_info(item,buff,j)<0) { free(item->free_me); free (item); item=NULL; fprintf(stderr,"error: [%s] is not a Thwab iTar file\n",fn); return NULL;}
  if (item->id == -1) item->id=rand();
  return (ThwabTopicItem *)item;
}
ThwabTopicItem *
thwab_item_new_from_file(Thwab_R_ *th, char *fn, FILE *cache)
{
  ThwabTopicItem_R_ *item;
  item=(ThwabTopicItem_R_ *)malloc(sizeof(ThwabTopicItem_R_));
  if (!item) return NULL;
  memset(item,0,sizeof(ThwabTopicItem));
  item->file=fn;
  
  itar *t;
  char *buff,*s1,*s2;
  int i,j,l,n,m=0;
  guint32 S,s;
  t=itar_open_full(fn,"rb",NULL);
  if (!t) { free (item); item=NULL; fprintf(stderr,"error: [%s] is not a Thwab iTar file\n",fn); return NULL;}
  itar_seek_by_file(t, TH_BOOK_INFO);
  buff=NULL;
  j=itar_pop_contents_by_file0(t, TH_BOOK_INFO, &buff);
  if (j<0) {itar_close(t); free (item); item=NULL; fprintf(stderr,"error: [%s] is not a Thwab iTar file\n",fn); return NULL;}
  item->free_me=buff;
  s=j+strlen(fn)+1;
  S=GUINT32_TO_LE(s);
  fwrite(&S, sizeof(S), 1, cache);
  fprintf(cache,"%s%c",fn,'\0');
  fwrite(buff, j, 1, cache);
  item->free_me=buff;
  if (parse_thwab_lib_info(item,buff,j)<0) { free(buff); free (item); item=NULL; fprintf(stderr,"error: [%s] is not a Thwab iTar file\n",fn); return NULL;}
  /* free(fn); // should not be freed here as it's saved by item->file */
  itar_close(t);
  if (item->id == -1) item->id=rand();
  return (ThwabTopicItem *)item;
}
/*
ThwabTopicItem *
thwab_item_new(Thwab_R_ *th, char *d, char *s)
{
  char *fn;
  ThwabTopicItem_R_ *item;
  item=(ThwabTopicItem_R_ *)malloc(sizeof(ThwabTopicItem_R_));
  if (!item) return NULL;
  memset(item,0,sizeof(ThwabTopicItem));
  fn=g_build_filename (d, s, NULL);
  item->file=fn;
  if (parse_thwab_lib_info(item->file,item)<0) {free (item); item=NULL; printf(" not a thwab-lib file\n");}
  // free(fn); // should not be freed here as it's saved by item->file
  return (ThwabTopicItem *)item;
}
*/
void thwab_foreach_topic(Thwab* thwab, GTraverseFunc func, gpointer user);
gboolean myfunc(gpointer key, gpointer value, gpointer data)
{
return FALSE;
}
/*
int
thwab_scandir(Thwab_R_ *th, char *d, int ls_n,void(*func)(gdouble, char *,gpointer), gpointer user)
{
  char *s;
  GDir* dir;
  GError *err=NULL;
  int i=0;
  ThwabTopicItem_R_ *item;
  d=g_build_filename (d, TH_BOOKS, NULL);
  dir=g_dir_open(d, 0, &err);
  if (!dir) return -1;
  while((s=g_dir_read_name(dir))!=0)
  {
    if (! g_str_has_suffix(s,TH_SUFFIX) || !(item=thwab_item_new(th, d, s))) continue;
    // TODO: skip is id is already loaded 
    if (!g_tree_lookup(th->topics, GINT_TO_POINTER(item->id))) g_tree_insert(th->topics,GINT_TO_POINTER(item->id),(gpointer)item);
    if (func) func(i/(gdouble)ls_n , item->title,user);
    ++i;
  }
  g_dir_close(dir);
  free(d);
  return 0;
}
*/
gint
thwab_item_cmp(gconstpointer a, gconstpointer b)
{
  register gint i=GPOINTER_TO_INT(a),j=GPOINTER_TO_INT(b);
  return (i==j)?0:(i>j)?1:-1;
}
GList *
thwab_files_ls_(GSList *ls, char *d, int *ls_n)
{
  char *s;
  GDir* dir;
  GError *err=NULL;
  ThwabTopicItem_R_ *item;
  int i=0;
  d=g_build_filename (d, TH_BOOKS, NULL);
  fprintf (stderr, "Scanning resources directory [%s]:  ", d);
  dir=g_dir_open(d, 0, &err);
  if (!dir) {fprintf(stderr,"NOT FOUND\n");return ls;}
  while((s=g_dir_read_name(dir))!=0)
  {
    if (! g_str_has_suffix(s,TH_SUFFIX)) continue;
    ls=g_list_insert_sorted(ls,g_build_filename (d,s,NULL),strcmp);
    ++i;
  }
  g_dir_close(dir);
  free(d);
  fprintf(stderr,"%d book(s) found\n",i);
  (*ls_n)+=i;
  return ls;
}

GList *
thwab_files_ls(Thwab_R_ *th,int *ls_n)
{
  const char *p = TH_PATH;
  char *s, *s1 = NULL, *s2 = NULL,dir[ITARNMAX];
  GList *ls=NULL;
  struct stat st;
  *ls_n=0;
  /* scan dirs */
  fprintf (stderr, "Directories [%s]\n", p);
  while (1)
    {
      s = strchr (p, G_SEARCHPATH_SEPARATOR);
      if (!s) break;
      s1 = strndup (p, s - p);
      if (s1[0]=='/') s2=strdup(s1);
      else if (s1[0]=='~' && s1[1]=='/') s2=g_build_filename (g_get_home_dir(),s1+2, NULL);
      else s2=g_build_filename (th->prefix, s1, NULL);
      if (stat (s2, &st) == 0)
	{
	  realpath(s2, dir);
	  ls=thwab_files_ls_(ls, dir, ls_n);
	} else {
	  fprintf (stderr, "Scanning directory [%s]:  NOT FOUND\n", s2);
	}
      free (s2); s2 = NULL;
      free (s1); s1 = NULL;
      p = s + 1;
    }
  if (s1) free (s1);
  /* done */
  ls=g_list_first(ls);
  fprintf (stderr, "Total books: %d\n", *ls_n);
  return ls;
}
int
thwab_save_ls_cache(char *fn,GList *ls)
{
  printf("Saving files list cache\n");
  GList *nd=ls;
  FILE *f=fopen(fn,"wb");
  if (!f) return -1;
  while(nd)
  {
    fprintf(f,"%s\n", nd->data);
    nd=g_list_next(nd);
  }
  fclose(f);
  return 0;
}

int
thwab_real_init(Thwab_R_ *th, char *cache_fn,GList *ls, int ls_n, void(*func)(gdouble, char *,gpointer), gpointer user)
{
  ThwabTopicItem_R_ *item;
  GList *nd=ls;
  int i=0;
  FILE *cache=fopen(cache_fn,"wb");
  if (!cache) return -1;
  while(nd)
  {
    if (!(item=thwab_item_new_from_file(th, nd->data,cache))) continue;
    if (!g_tree_lookup(th->topics, GINT_TO_POINTER(item->id))) g_tree_insert(th->topics,GINT_TO_POINTER(item->id),(gpointer)item);
    if (func) func(i/(gdouble)ls_n , item->title,user);
    ++i;
    nd=g_list_next(nd);
  }
  fclose(cache);
  return 0;
}
int
thwab_cached_init(Thwab_R_ *th, char *cache_fn,GList *ls, int ls_n, void(*func)(gdouble, char *,gpointer), gpointer user)
{
  ThwabTopicItem_R_ *item;
  GList *nd=ls;
  int i=0;
  FILE *cache=fopen(cache_fn,"rb");
  if (!cache) return -1;
  while(nd)
  {
    if (!(item=thwab_item_new_from_cache(th, nd->data,cache))) continue;
    if (!g_tree_lookup(th->topics, GINT_TO_POINTER(item->id))) g_tree_insert(th->topics,GINT_TO_POINTER(item->id),(gpointer)item);
    if (func) func(i/(gdouble)ls_n , item->title,user);
    ++i;
    nd=g_list_next(nd);
  }
  fclose(cache);
  /*  it's safe to distroy ls after here as fn is also saved in cache */
  /*  it's NOT safe to distroy ls after real_init as fn is taken from ls */
  /* FIXME: to free() ls or NOT*/
  return 0;
}
int
thwab_is_ls_cache_old(char *fn,GList *ls, int ls_n)
{
  char **ls_c;
  char *str,*s_end,*s,*s2;
  gsize l;
  int i,n;
  GList *nd=ls;
  if (!g_file_get_contents(fn,&str,&l,NULL)) return -1;
  s=s2=str;
  s_end=str+l;
  n=str_count_chr(str,'\n',l);
  if (n!=ls_n) {free(str); return -2;}
  while(s<s_end)
  {
    if (!nd) {free(str); return -5;}
    if (!(s2=strchr(s,'\n'))) {free(str); return -3;}
    *s2='\0';
    if (strcmp(s,nd->data)) {free(str); return -4;}
    s=s2+1;
    nd=g_list_next(nd);
  }
  free(str);
  return 0;
}

int
thwab_init(Thwab *thwab,void(*func)(gdouble, char *,gpointer), gpointer user)
{
  Thwab_R_ *th=thwab;
  const char *p = TH_PATH;
  char *s, *s1 = NULL, *s2 = NULL,*fn,*fn2;
  struct stat st;
  GTree*tree;
  GList *ls=NULL,*ls_new=NULL;
  ThwabTopicItem *item;
  int ls_n,r;
  /* Loadding topics */
  th->topics=g_tree_new(thwab_item_cmp);
  fprintf(stderr,"=====================\n");
  fn=g_build_filename (g_get_home_dir(),TH_AFTER_HOME, NULL);
  mkdir(fn,0755);
  free(fn);
  fn= g_build_filename (g_get_home_dir(),TH_FILES_CACHE, NULL);
  fn2= g_build_filename (g_get_home_dir(),TH_INFO_CACHE, NULL);
  printf("chache file [%s]\n",fn);
  ls_new=thwab_files_ls(th, &ls_n);
  if (r=thwab_is_ls_cache_old(fn, ls_new, ls_n)) {
    printf("r=%d\n",r);
    thwab_save_ls_cache(fn, ls_new);
    thwab_real_init(th, fn2, ls_new, ls_n, func, user);
  }
  else {
    printf("quick start\n");
    thwab_cached_init(th, fn2, ls_new, ls_n, func, user);
  }
  free(fn);
  free(fn2);
  fprintf(stderr,"=====================\n");
  return 0;
}

Thwab*
thwab_new(const char *argv0)
{
  Thwab_R_ *th=malloc(sizeof(Thwab_R_));
  if (!th) return NULL;
  memset(th,0,sizeof(Thwab_R_));
  th->prefix = get_prefix (argv0);
  printf("Ok, using %s\n", th->prefix);
  return (Thwab*)th;
}
void
thwab_foreach_topic(Thwab* thwab, GTraverseFunc func, gpointer user)
{
  Thwab_R_ *th=thwab;
  g_tree_foreach(th->topics, func, user);
}

ThwabTopicItem*
thwab_get_topic_item(Thwab* thwab, int i)
{
  Thwab_R_ *th=thwab;
  ThwabTopicItem* ptr=(ThwabTopicItem*)g_tree_lookup(th->topics, GINT_TO_POINTER(i));
  printf("topics=%p id=%d (%p) p=%p\n",th->topics, i, GINT_TO_POINTER(i), ptr);
  return ptr;
}
/***********************************************************************/
typedef struct
{
	char *id,*title,typ;
} ThwabTOC;
typedef struct
{
	char *key,*id;
} ThwabKeys;

typedef struct
{
	char *root,*keyword;
	int n;
	char *ids; // array of n adjacent strings, ie id[i+1]=id[i]+strlen(id[i]);
} ThwabKeywords;

typedef struct _ThwabTopic_R_  ThwabTopic_R_;
struct _ThwabTopic_R_
{
	/* private */
	ThwabTopicItem *item;
	itar *t;
	int toc_n,keys_n,keywords_n;
	ThwabTOC *toc;
	ThwabKeys *keys;
	ThwabKeywords *keywords;
	char *free_toc,*free_keys,*free_keys_end,*free_keywords,*free_keywords_end;
	GHashTable* keys_ht;
};
int
thwab_topic_parse_toc(ThwabTopic_R_ *r, char *buff,int L)
{
  char typ,*s,*s1,*s2,*s3,*next_l;
  int i,j,k,l,n;
  r->toc_n=0;
  /* not needed any more
  if (buff[L-1]!='\n')
  {
    fprintf(stderr,"Warning: No newline at end of file\n");
    buff[L-1]='\n';
  }
  */
  //printf("TOC: [%s]\n",buff);
  n=strnchr2chr(buff, '\n', 0, L);
  //printf("Info: [%s]\n",buff);
  s=buff;
  j=L;
  if (n<1) 
  {
    fprintf(stderr,"Warning: Empty content file\n");
    return n;
  }
  r->toc_n=n;
  r->toc=calloc(n,sizeof(ThwabTOC));
  k=0; next_l=s;
  for (i=0;i<n;++i)
  {
    next_l=s+strlen(s)+1;
    s=str_skipblank(s,j-1);
    j=L-(buff-s);
    s1=str_skip_nonblank(s,j-1);
    j=L-(buff-s1);
    if (s1==s) {continue;}
    /* FIXME: double check again for bad input */
    s2=str_skipblank(s1,j-1);
    typ=*s2;
    j=L-(buff-s2);
    s3=str_skip_nonblank(s2,j-1);
    j=L-(buff-s3);
    s2=str_skipblank(s3,j-1);
    j=L-(buff-s2);
    *s1='\0';
    r->toc[k].id=s;
    r->toc[k].title=s2;
    r->toc[k].typ=typ;
    s=s2+1+strlen(s2+1)+1;
    j=L-(buff-s);
    ++k;
    s=next_l;
  }
  printf ("%d %d\n",i,k);
  return k;
}
int
cmp_key_by_id(const ThwabKeys *p1, const ThwabKeys *p2) {
  return strcmp(p1->id, p2->id);
}

int
thwab_topic_parse_keys(ThwabTopic_R_ *r, char *buff,int L)
{
  char typ,*s,*s1,*s2,*t1,*t2;
  int i,j,k,l,n;
  guint h;
  r->keys_n=0;
  printf("Parsing Keys\n");
  /* not needed any more
  if (buff[L-1]!='\n')
  {
    fprintf(stderr,"Warning: No newline at end of file\n");
    buff[L-1]='\n';
  }
  */
  n=strnchr2chr(buff, '\n', 0, L);
  if (n<1)
  {
    fprintf(stderr,"Warning: Empty keys file\n");
    return n;
  }
  r->keys=calloc(n,sizeof(ThwabKeys));
  s=buff;
  j=L;
  k=0;
  r->keys_ht=g_hash_table_new(g_str_hash, g_str_equal);
  /* TODO: Glib hash for int with g_hash_table_new(g_int_hash, g_int_equal)
  to save memory as we don't need to save keys, but GLib want me to alloc mem for each int! */
  fprintf(stderr,"Parsing ...\n");
  for (i=0;i<n;++i)
  {
    s2=s+strlen(s)+1;
    s1=strchr(s,'\t');
    if (!s1) {s=s2; continue;}
    *s1='\0';
    r->keys[k].key=s;
    r->keys[k].id=s1+1;
    t1=g_utf8_casefold(s,s1-s);
    t2=g_utf8_normalize(t1,-1,G_NORMALIZE_ALL_COMPOSE); // COMPOSE is used to minimize size
    if (strlen(t2)<=s1-s) {*s='\0'; strncat(s,t2,s1-s); free(t2);}
    else r->keys[k].key=t2; // we won't free it here */
    g_free(t1);
    ++k;
    s=s2;
  }
  r->keys_n=k; n=k;
  fprintf(stderr,"Sorting ...\n");
  qsort(r->keys, r->keys_n, sizeof(ThwabKeys), cmp_key_by_id);
  fprintf(stderr,"Hashing ...\n");
  for (i=0;i<n;++i)
  {
      g_hash_table_insert(r->keys_ht, r->keys[i].key, &(r->keys[i]));
  }
  return n;
}
void thwab_topic_keywords_load(ThwabTopic *topic);
ThwabTopic*
thwab_topic_new(Thwab* thwab, ThwabTopicItem *item)
{
  Thwab_R_ *th=(Thwab_R_ *)thwab;
  ThwabTopicItem_R_ *it=(ThwabTopicItem_R_ *)item;
  ThwabTopic_R_ *r=malloc(sizeof(ThwabTopic_R_));
  //memset(r,sizeof(ThwabTopic_R_),0);
  char *buff=NULL;
  int i;
  if (!r) return NULL;
  r->item=it;
  r->toc=NULL;
  printf("@1\n");
  r->t=itar_open_full(it->file,"rb",NULL);
  printf("@2\n");
  if (!r->t) {printf("failed to open itar file"); free(r); return NULL;}
  itar_seek_by_file(r->t, TH_BOOK_TOC);
  i=itar_pop_contents_by_file0(r->t, TH_BOOK_TOC, &buff);
  if (i>0)
  {
    r->free_toc=buff;
    r->toc_n=thwab_topic_parse_toc(r,buff,i);
  } else
  {
    r->toc_n=0;
    r->free_toc=NULL;
    r->toc=NULL;
  }
  printf("@3\n");
  itar_seek_by_file(r->t, TH_BOOK_KEYS);
  i=itar_pop_contents_by_file0(r->t, TH_BOOK_KEYS, &buff);
  printf("[%d]\n",i);
  if (i>0)
  {
    r->free_keys=buff;
    r->free_keys_end=buff+i;
    thwab_topic_parse_keys(r,buff,i);
  } else
  {
    printf("no keys\n");
    r->keys_ht=NULL;
    r->keys_n=0;
    r->free_keys=NULL;
    r->keys=NULL;
  }
  r->keywords_n=0;
  r->keywords=NULL;
  printf("@4\n");
  thwab_topic_keywords_load((ThwabTopic*)r);
  printf("@5\n");
  /*
  char *targets[3]={"عذاب",  "القبر",NULL  };
  thwab_topic_find((ThwabTopic*)r, targets, 1, NULL, NULL);
  */
  return (ThwabTopic*)r;
}
int
thwab_topic_foreach(ThwabTopic	*topic, void(*func)(int i,int ,const char *,const char *,gpointer), gpointer user)
{
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  int i;
  for (i=0;i< r->toc_n;++i) func(i, r->toc[i].typ>'3', r->toc[i].id, r->toc[i].title, user);
  return r->toc_n;
}
char *
thwab_topic_content_by_id(ThwabTopic *topic, const char *id, int *s)
{
  char *buff;
  int j;
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  char *Id=strdupa(id);
  if (!r || !r->t) return NULL;
  itar_seek_by_file(r->t, Id);
  j=itar_pop_contents_by_file0(r->t, Id, &buff);
  if (j<0) return NULL;
  if (s) *s=j;
  return buff;
}

char *
thwab_topic_content(ThwabTopic	*topic, int i,int *s)
{
  char *buff;
  int j;
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  if (!r || !r->t) return NULL;
  if (i >= r->toc_n) return NULL;
  if (r->toc[i].typ > '3') return NULL;
  itar_seek_by_file(r->t, r->toc[i].id);
  j=itar_pop_contents_by_file(r->t, r->toc[i].id, &buff);
  if (j<0) return NULL;
  if (s) *s=j;
  return buff;
}
/* inline replace "from" with "to" both having same length and return number of replacements
int
thwab_inline_replace(char *txt, char *from, char *to)
{
  char *s=txt;
  int r=0,l=strlen(from);
  if (l != strlen(to)) return -1;
  while(s=strstr(s,from)) { memcpy(s, to, l); ++r; s+=l;}
  return r;
}
 */
/* inline replace "from" with "to" both having "from" >= "to" in length */
int
thwab_inline_replace(char *txt, char *from, char *to)
{
  char *p_in=txt,*p_out=txt,*p_end,c;
  int r=0,l=strlen(from),l2=strlen(to);
  if (l < l2) return -1;
  //p_end=p_in+strlen(txt);
  while(c=*p_in)
  {
    if (! strncmp(p_in,from,l)) // startswith(from)
    {
      memcpy(p_out, to, l2);
      p_out+=l2; p_in+=l;
      ++r;
    } else {
      *p_out=c;
      ++p_in; ++p_out;
    }
  }
  *p_out='\0';
  return r;
}

void
thwab_fix_bidi(char *html, int is_rtl)
{
  //
  char *left="left",*right="right";
  char *ltr="ltr",*rtl="rtl";
  thwab_inline_replace(html,"RTL", is_rtl?ltr:rtl);
  thwab_inline_replace(html,"LTR", is_rtl?rtl:ltr);
  thwab_inline_replace(html,"RIGHT", is_rtl?left:right);
  thwab_inline_replace(html,"LLEFT", is_rtl?right:left);
}
inline void
thwab_mf_html_file_(const char *mf, char *fn)
{
    *fn='\0';
    strncat(fn, mf, ITARNMAX);
    strnchr2chr(fn, '/', '_', ITARNMAX);
    strcat(fn, ".html");
}
inline int
thwab_node_html(ThwabTopic_R_ *r, int i, char *html_header, 
  char *html_footer, char *dir, char *id,char *title_p, char *id_p,
  char *title_up, char *id_up,char *title_n, char *id_n)
{
      char *fn,*s1,*s2,*str;
      FILE *f;
      int j,l,n;
      fn=g_build_filename (dir, id, NULL);
      f=fopen(fn, "wt");
      if (!f) {fprintf(stderr, "can't open [%s]\n", fn); return -1;}
      fprintf(f,html_header, TH_HTML_GENERATOR, r->item->title, r->item->title, title_p,
      id_p, title_up, id_up, r->item->title,
        r->toc[i].title, title_n, id_n,"<!-- UPs -->",r->toc[i].title);
      str=thwab_topic_content_by_id(r, r->toc[i].id, &l);
      /* not needed
      if (str[l-1]!='\n') {printf("No newline at end of file!!\n");} */
      s2=s1=g_markup_escape_text(str,l);
      g_free(str);
      n=strnchr2chr(s1, '\n', '\0', -1);
      for (j=0;j<n;++j) {fprintf(f,"<p>%s</p>\n",s2); s2+=strlen(s2)+1;}
      g_free(s1);
      g_free(fn);
      fprintf(f,html_footer,title_p,id_p,title_up,id_up,
        r->item->title, r->toc[i].title,title_n, id_n, TH_HTML_COPYRIGHT);
      fclose(f);
      return 0;
}
inline int
thwab_subtoc_html(ThwabTopic_R_ *r, int i, char *html_header, 
  char *html_footer, char *dir, char *id,char *title_p, char *id_p,
  char *title_up, char *id_up,char *title_n, char *id_n)
{
      char *fn,*s1,*s2;
      char id_tmp[ITARNMAX];
      FILE *f;
      int j,n,l,sub_d,id_len;
      fn=g_build_filename (dir, id, NULL);
      f=fopen(fn, "wt");
      if (!f) {fprintf(stderr, "can't open [%s]\n", fn); return -1;}
      fprintf(f,html_header, TH_HTML_GENERATOR, r->item->title, r->item->title, title_p, id_p, title_up, id_up, r->item->title, r->toc[i].title, title_n, id_n,"<!-- UPs -->",r->toc[i].title);
      fprintf(f, "<ul>\n");
      sub_d=str_count_chr(r->toc[i].id,'/', ITARNMAX);
      id_len=strlen(r->toc[i].id);
      for (j=i+1; j<r->toc_n; ++j)
      {
        if (str_count_chr(r->toc[j].id,'/', ITARNMAX)==sub_d+1 && id_len<strlen(r->toc[j].id) && strncmp(r->toc[j].id,r->toc[i].id,id_len)==0)
          {
          thwab_mf_html_file_(r->toc[j].id, id_tmp);
          fprintf(f, "<li><a href='%s'>%s</a> %s</li>\n", id_tmp, r->toc[j].title, (r->toc[j].typ<'4')? "":"&gt;");}
      }
      fprintf(f, "</ul>\n");
      g_free(fn);
      fprintf(f,html_footer,title_p,id_p,title_up,id_up,
        r->item->title, r->toc[i].title, title_n, id_n,TH_HTML_COPYRIGHT);
      fclose(f);
      return 0;
}
inline int
thwab_load_html_template(Thwab *th, char *lang, char **h,char **h_header,char **h_footer)
{
  // Load and process HTML template
  char *temp_fn, *s1, *s2;
  char *html,*html_header,*html_footer;
  FILE *f;
  long l;
  int i,rtl=0;
  temp_fn=thwab_filename (th,"html_templates/file.html");
  f=fopen(temp_fn,"rt");
  if (!f) { free(temp_fn); return -3;}
  free(temp_fn);
  fseek(f,0,SEEK_END);
  l=ftell(f); rewind(f);
  html=(char *)malloc(l+1);
  if (!html) {fclose(f); return -4;}
  if (fread(html,l,1,f)!=1) { free(html); fclose(f); return -5;}
  fclose(f);
  html[l]='\0';
  /* TODO: autodetect RTL or LTR in HTML */
  if (lang && strncmp(lang,"ar",2)==0) rtl=1; // TODO: better RTL checking
  else rtl=0;
  thwab_fix_bidi(html, rtl); // 1 for arabic 0 for English
  // find the 9th %s to be chop the head and footer
  s2=NULL; s1=html; i=0;
  while(s1=strstr(s1,"%s"))
  {
    if (i==13) s2=s1;
    ++i;
    s1+=2;
  }
  if (!s2 || i!=23) {free(html); return -6;}
  *s2='\0'; html_header=html; html_footer=s2+2;
  *h=html; *h_header=html_header; *h_footer=html_footer;
  return 0;
}
int
thwab_topic_convert_to_html(ThwabTopic	*topic, Thwab *th, char *prefix)
{
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  char *temp_fn, *html=NULL,*html_header,*html_footer;
  int i,j,n,d=0,sub_d,depth=-1,ul_n=0,id_len;
  int l;
  char *dir,*fn, id[ITARNMAX],*nxt;
  char *title_p, *title_n, *title_up;
  char id_p[ITARNMAX], id_n[ITARNMAX],id_up[ITARNMAX], id_tmp[ITARNMAX];
  char *str,*s1,*s2;
  FILE *toc_f,*f;
  int rtl=0;
  if (!topic || !th || r->toc_n <= 0 ) return -1;
  //if (mkdir(prefix,0777)<0) return -2;
  dir=g_build_filename (prefix, "files",NULL);
  printf("prefix=[%s] dir=[%s]\n",prefix, dir);
  if (mkdir(dir,0777)<0) return -2;
  temp_fn=thwab_filename (th,"html_templates/img");
  /* TODO: use abetter method than forking a system cp */
  //system("/bin/cp temp_fn dir");
  pid_t cpid, w;
  cpid = fork();
  if (cpid == -1) { perror("fork");  free(temp_fn); return -6;}
  if (cpid == 0) {  /* Code executed by child */
     execl("/bin/cp", "/bin/cp", "-af", temp_fn, dir ,NULL);
  }
  free(temp_fn);
  i=wait(NULL);
  if (i<0) {free(dir); return -3;}
  // Load and process HTML template
  if (thwab_load_html_template(th, r->item->lang, &html, &html_header, &html_footer)<0) {free(dir); return -4;}
  // generate index.html
  fn=g_build_filename (prefix,"index.html", NULL);
  symlink ("files/index.html", fn); // no error check as it's not critical
  g_free(fn);
  fn=g_build_filename (dir,"index.html", NULL);
  f=fopen(fn, "wt");
  g_free(fn);
  if (!f) {g_free(dir); g_free(html); return -5;}
  fprintf(f,html_header,TH_HTML_GENERATOR, r->item->title,r->item->title,"","index.html","",
   "index.html",r->item->title, r->item->title,
   "TOC","toc.html","",r->item->title);
  fprintf(f, "<H2>%s</H2>\n"
  "<H3>%s<H3><HR/>\n", (r->item->subtitle)? r->item->subtitle:  "", r->item->authors);
  s2=s1=g_markup_escape_text(r->item->comment,-l);
  n=strnchr2chr(s1, '\n', '\0', -1);
  for (j=0;j<n;++j) {fprintf(f,"<p>%s</p>\n",s2); s2+=strlen(s2)+1;}
  g_free(s1);
  fprintf(f,html_footer,"","index.html","","index.html",
    r->item->title,r->item->title, "TOC","toc.html",TH_HTML_COPYRIGHT);
  fclose(f);
  // gen top most toc.html file
  fn=g_build_filename (dir,"toc.html", NULL);
  toc_f=fopen(fn, "wt");
  if (!toc_f) {g_free(dir); g_free(html); return -5;}
  g_free(fn);
  title_p = r->item->title;
  *id_n='\0'; nxt="";
  if (r->toc_n>0)
  {
    thwab_mf_html_file_(r->toc[0].id, id_n);
    nxt=r->toc[0].title;
  }
  strcpy(id_p, "index.html");
  fprintf(toc_f,html_header,TH_HTML_GENERATOR, r->item->title,r->item->title,"Home","index.html",
    "Index","index.html",r->item->title, r->item->title, nxt, id_n, "",r->item->title);
  for (i=0;i< r->toc_n;++i)
  {
    /* converting member files */
    title_n=(i+1 < r->toc_n)?r->toc[i+1].title:r->item->title;
    strcpy(id,id_n);
    if (i+1< r->toc_n) thwab_mf_html_file_(r->toc[i+1].id, id_n);
    else strcpy(id_n, "index.html");
    if (strchr(r->toc[i].id,'/'))
    {
      str=g_path_get_dirname(r->toc[i].id);
      thwab_mf_html_file_(str, id_up);
      g_free(str);
      title_up="UP";
    }
    else {
      strcpy(id_up, "toc.html");
      title_up="UP";
    }
    d=str_count_chr(r->toc[i].id,'/', ITARNMAX);
    if (d>depth) {depth=d; fprintf(toc_f,"<ul compact='1'>\n"); ++ul_n;}
    else if (d<depth) {
      while( depth > d )
        {fprintf(toc_f,"</ul>\n"); --ul_n; --depth;}
    }
    //for (j=0;j<depth;++j) fputc(' ',toc_f);
    if (r->toc[i].typ<'4')
    { /* converting a content file */
       fprintf(toc_f, "<li><a href='%s'>%s</a></li>\n", id, r->toc[i].title);
       if (thwab_node_html(r, i, html_header, html_footer, dir, id, title_p, id_p, title_up, id_up, title_n, id_n)<0) {free(dir); free(html); return -5;}
    } else {
       /* converting a sub TOC file */
      fprintf(toc_f, "<li><a href='%s'><b>%s</b></a> &gt;</li>\n", id, r->toc[i].title);
      if (thwab_subtoc_html(r, i, html_header, html_footer, dir, id, title_p, id_p, title_up, id_up, title_n, id_n)<0) {free(dir); free(html); return -5;}
    }
    title_p=r->toc[i].title;
    strcpy(id_p, id);
  }
  while( ul_n-- > 0 ) {fprintf(toc_f,"</ul>\n");}
  if (r->toc_n>0) {thwab_mf_html_file_(r->toc[0].id, id_n); title_n=r->toc[0].title;}
  else {strcpy(id_n,"index.html"); title_n="Index";}
  fprintf(toc_f,html_footer,"Home","index.html","Home","index.html",
  r->item->title, r->item->title, title_n, id_n,TH_HTML_COPYRIGHT);
  fclose(toc_f);
  g_free(dir);
  g_free(html);
  return 0;
}
const char *
thwab_topic_lookup_key(ThwabTopic	*topic, const char *key)
{
      ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
      char *t1,*t2;
      int h;
      ThwabKeys *k;
      if (!r) return NULL;
      t1=g_utf8_casefold(key,-1);
      t2=g_utf8_normalize(t1,-1,G_NORMALIZE_ALL_COMPOSE);
      printf("looking for [%s]",t2);
      k=(ThwabKeys *)g_hash_table_lookup (r->keys_ht,t2);
      if (k) printf("found at [%s]\n",k->id);
      else printf("not found\n");
      g_free(t2);
      g_free(t1);
      if (!k) return NULL;
      return k->id;
}

int
thwab_topic_parse_keywords(ThwabTopic_R_ *r, char *buff,int L)
{
  char *s,*s1,*s2;
  char *root,*keyword;
  char *keyword_norm,*keyword_fold;
  int i,j,k,l,n,id_n,keyword_len;
  guint h;
  r->keywords_n=0;
  printf("Parsing Search Keywords\n");
  /*
  if (buff[L-1]!='\n')
  {
    fprintf(stderr,"Warning: No newline at end of file\n");
    buff[L-1]='\n';
  }
  */
  r->free_keywords=buff;
  r->free_keywords_end=buff+L;

  n=strnchr2chr(buff, '\n', 0, L);
  if (n<1)
  {
    fprintf(stderr,"Warning: Empty search keywords file\n");
    return n;
  }
  r->keywords=calloc(n,sizeof(ThwabKeywords));
  s=buff;
  j=L;
  k=0;
  for (i=0;i<n;++i)
  {
    root=NULL; keyword=NULL; id_n=0;
    s2=s+strlen(s)+1; // prepare next loop
    s1=str_skip_nonblank(s,-1); // strchr(s,'\t');
    if (s1!=s && s1 && *s1)
    {
      *s1='\0'; root=s;
      s=s1+1;
      s1=str_skip_nonblank(s,-1); // strchr(s,' ');
      if (s1!=s && s1 && *s1)
      {      
	*s1='\0'; keyword=s; keyword_len=s1-s;
	s=s1+1;
	id_n=strtol(s, &s1,10);
	if (s1!=s && s1 && *s1)
	{
	  s=s1; s1=str_skipblank(s,-1);
	  strnchr2chr(s1, ' ', 0, -1); // TODO: more serious solution
	  r->keywords[k].ids=s1;
	  r->keywords[k].n=id_n;
	  r->keywords[k].root=root;
	  // TODO: the following are very time consuming, to be saved in the cached file
	  keyword_norm=g_utf8_normalize(keyword,-1,G_NORMALIZE_ALL_COMPOSE);
	  keyword_fold=g_utf8_casefold(keyword_norm,-1);
	  if (strlen(keyword_fold)<=keyword_len) {*keyword='\0'; strncat(keyword, keyword_fold, keyword_len); free(keyword_fold);}
	  else keyword=keyword_fold;
	  r->keywords[k].keyword=keyword;
	  g_free(keyword_norm);
	  ++k;
	}
      }
    }
    s=s2;
  }
  r->keywords_n=k;
  return k;
}

void
thwab_topic_keywords_load(ThwabTopic *topic)
{
  char *buff,*fn,*fn2,*s;
  int i,k,l,t1,t2,L,fd;
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  FILE *F;
  gzFile gzf;
  r->keywords_n=-1;
  r->free_keywords=NULL;
  printf("\ntrying to load search keywords\n");
  fn=((ThwabTopicItem_R_ *)(r->item))->file;
  fn2=malloc((l=strlen(fn))+6);
  strcpy(fn2,fn);
  if ((s=strchr(basename(fn2),'.'))!=NULL) {l=(s-fn2);} /* NOTE: must be the gnu basename */
  fn2[l]='\0'; strncat(fn2+l,".4",2);
  printf("opening :[%s]\n",fn2);
  F=fopen(fn2, "rb");
  if (F) {
    free(fn2);
    printf("trying to load uncompressed search keywords\n");
    fseek(F, (off_t)(0), SEEK_END); L=ftell(F);
    rewind(F);
    L+=2;
    printf("size %d\n",L);
    buff=malloc(L);
    if (!buff) {fclose(F); return;}
    if (fread(buff, L-2, 1,F)<1) {free(buff); fclose(F); return;}
    buff[L-2]='\n'; buff[L-1]='\0';
    k=thwab_topic_parse_keywords(r,buff,L);
    if (k==0) r->keywords_n=-1;
    printf("%d key found\n",k);
    fclose(F);
  } else {
    fn2[l]='\0'; strncat(fn2+l,".4.gz",5);
    printf("opening :[%s]\n",fn2);
    F=fopen(fn2, "rb");
    free(fn2);
    if (F) {
      fd=fileno(F);
      // TODO: find a better safer way (libcall) to know gzip uncompressed size
      l = lseek(fd, (off_t)(-4), SEEK_END);
      if (l == -1L) {fclose(F); return;}
      if (read(fd, &L, sizeof(L)) != sizeof(L)) {fclose(F); return;}
      L = GINT_FROM_LE(L)+2; // a two extra bytes for "\n\0"
      buff=malloc(L);
      if (!buff) {fclose(F); return;}
      lseek(fd, 0, SEEK_SET);
      gzf=gzdopen(fd, "rb");
      if (!gzf) {fclose(F); free(buff); return;}
      i=gzread(gzf, buff, L-2);
      buff[L-2]='\n'; buff[L-1]='\0';
      k=thwab_topic_parse_keywords(r,buff,L);
      if (k==0) r->keywords_n=-1;
      printf("%d key found\n",k);
      gzclose(gzf);
      fclose(F);
    } else {
      itar_seek_by_file(r->t, TH_BOOK_SEARCH);
      i=itar_pop_contents_by_file0(r->t, TH_BOOK_SEARCH, &buff); // TODO: this is time consuming tobe cached in external file (optionally gzipped but not bzipped)
      if (i>0)
      {
        t1=time(NULL);
        printf("Parsing: ");
        k=thwab_topic_parse_keywords(r,buff,i);
        t2=time(NULL);
        printf("done in %d sec\n", t2-t1);
        printf("%d keyword(s) loaded\n",k);
        if (k==0) r->keywords_n=-1;
      } else
      {
      /* failed */
        r->keywords_n=-1;
        r->free_keywords=NULL;
      }
    }
  }
}
void
thwab_topic_keywords_destroy(ThwabTopic_R_ *r)
{
  // TODO: not ready yet
  char *k;
  int i;
  for (i=0;i < r->keywords_n;++i)
   {
     if ((k=r->keywords[i].keyword) && (k < r->free_keywords || k > r->free_keywords_end)) free(r->keywords[i].keyword);
   }
  free(r->free_keywords);
  free(r->keywords);
}
void
thwab_topic_destroy(ThwabTopic *topic)
{
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  if (!r) return;
  printf("Closing\n");
  if (r->keys_ht) g_hash_table_destroy (r->keys_ht);
  if (r->free_toc) free(r->free_toc);
  if (r->toc) free(r->toc);
  // loop over keys[i].key if not in free_keys then free it
  int i;
  if (r->free_keys)
  {
  for (i=0;i < r->keys_n;++i)
  {
    if (r->keys[i].key<r->free_keys || r->keys[i].key>r->free_keys_end) free(r->keys[i].key);
  }
  free(r->free_keys);
  }
  if (r->keys) free(r->keys);
  thwab_topic_keywords_destroy(r);
  if (r->t) itar_close(r->t);
  free(r);
}

typedef struct
{
  float r,R; // overall rank and current pass rank
} ThwabSearchResultItem;

typedef struct
{
  ThwabSearchCB func;
  gpointer data;
} ThwabFindCBData;

static inline void
thwab_topic_found(GTree *result, ThwabKeywords keyword, int pass, float r)
{
  char *id=keyword.ids,*next=NULL;
  int i,idn=keyword.n;
  ThwabSearchResultItem *p;
  for (i=0; i<idn; ++i)
  {
    next=id+strlen(id)+1;
    if (!(p=g_tree_lookup(result, id)))
    {
      //printf("new: ");
      p=malloc(sizeof(ThwabSearchResultItem));
      p->r=0; p->R=0;
      g_tree_insert(result, id, (gpointer)p);
    }
    if (pass) p->R+= r - p->R * r; // fuzzy logic OR
    else p->r+= r - p->r * r;
    //printf("found [%s] r=%g, R=%g\n", id, p->r, p->R);
    id=next;

  }
}
gboolean
thwab_apply_and_pass(char *id, ThwabSearchResultItem *p, gpointer data)
{
  p->r*=p->R; p->R=0;
  return FALSE;
}
gboolean
thwab_apply_or_pass(char *id, ThwabSearchResultItem *p, gpointer data)
{
  p->r+=p->R - p->R*p->r; p->R=0;
  return FALSE;
}
gboolean
thwab_topic_report_found(char *id, ThwabSearchResultItem *p, ThwabFindCBData *ptr)
{
  //if ( p->r > 0.0) printf("%f\t[%s]\n",p->r,id);
  if ( p->r > 0.0 && ptr->func) ptr->func(id,p->r,ptr->data);
  return FALSE;
}
static int
toc_id_cmp(const ThwabTOC *a, const ThwabTOC *b)
{
  return strcmp(a->id,b->id);
}

const char *
thwab_topic_content_title_by_id(ThwabTopic *topic, const char *id)
{
 char *str=NULL;
 ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
 ThwabTOC *bs,target;
 if (!topic || !r->toc_n) return NULL;
 target.id=id;
 bs = bsearch( &target, r->toc, r->toc_n, sizeof(ThwabTOC), toc_id_cmp);
  if (!bs) return NULL;
  str=(*bs).title;
  if (strcmp("_",str)==0 && bs > r->toc) {str=(*(--bs)).title; }
  return str;
}

static int
key_id_cmp(const void *A,  const void *B)
{
  ThwabKeys *a=(ThwabKeys *)A, *b=(ThwabKeys *)B;
  return strcmp(a->id, b->id);
}

const char *
thwab_topic_content_key_by_id(ThwabTopic *topic, const char *id)
{
 ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  ThwabKeys *bs,target;
  if (!topic || !r->keys_n) return NULL;
  target.id=id;
  bs = bsearch( &target, r->keys, r->keys_n, sizeof(ThwabKeys), key_id_cmp);
  if (!bs) return NULL;
  return (*bs).key;
}

void
thwab_topic_find(ThwabTopic *topic, const char **targets, int is_and, ThwabSearchCB func, gpointer data)
{
  ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
  ThwabFindCBData cbdata;
  int i,j,l,L,id_n,target_len;
  char *target,*target_norm,*target_fold;
  //*target_coll;
  char *keyword,*keyword_coll,*ids,*s;
  float rank;
  GTree *result;
  if (!r || r->keywords_n<=0) return;
  result=g_tree_new_full(strcmp, NULL, NULL, g_free);
  cbdata.func=func;
  cbdata.data=data;
  for (i=0;((target=targets[i])!=NULL);++i)
  {
    //printf("!\n");
    printf("!! [%s]\n",target);
    target_norm=g_utf8_normalize(target,-1,G_NORMALIZE_ALL_COMPOSE);
    //printf("!!!\n");
    target_fold=g_utf8_casefold(target_norm,-1);
    //target_coll=g_utf8_collate_key(target_fold,-1);
    target_len=strlen(target_fold);
    //printf("pass %d: [%s][%s]l=%d\n",i+1,target,strlen(target_coll));
    for(j=0; j< r->keywords_n; ++j)
    {
      // TODO: optionally devide by id_n (for "or") to dec importence of to frequent words
      keyword=r->keywords[j].keyword;
      //keyword_coll=r->keywords[j].keyword_coll;
      id_n=r->keywords[j].n;
      ids=r->keywords[j].ids;
      rank=thwab_str_similarity(keyword,target_fold,0.8, 0.9, 0.1);
      if (rank>1.0) printf("%s %s %g\n",keyword,target_fold,rank);
      if (rank>0.0) thwab_topic_found(result, r->keywords[j], i, 0.75*rank);
      else { // more checks
      
      }
/*      strcmp(keyword,target_fold)==0) 
      else { // prefix suffix test
        s=strstr(keyword,target);
	//printf("[%s][%s][%s]\n",keyword_coll,target_coll,s);
	if (s) {
	  L=strlen(keyword);
	  l=L-target_len; // excess letters
	  printf("substr:[%s][%f]\n",keyword,0.9*(1.0-(float)l/L));
	  thwab_topic_found(result, r->keywords[j], i, 0.9*(1.0-(float)l/L));
        } else { // other tests
	}
      }*/
      
    }
    // TODO: calc min and max rank for every pass and ignore words that are not found at all
    printf("ok\n");
    g_free(target_fold);
    g_free(target_norm);
    if (i)
    {
      if (is_and) g_tree_foreach(result,thwab_apply_and_pass,NULL);
      else g_tree_foreach(result,thwab_apply_or_pass,NULL);
    }
  }
  g_tree_foreach(result, thwab_topic_report_found, &cbdata);
  g_tree_destroy (result);
}
const char *
thwab_topic_get_filename(ThwabTopic *topic)
{
 ThwabTopic_R_ *r=(ThwabTopic_R_ *)topic;
 if (r) return r->item->file;
 return NULL;
}
/*
int thwab_cmp_len (char **p1, char **p2)
{
  return strlen(*p2)-strlen(*p1);
}
void thwab_sort_by_len (char **array, int nstrings)
{
  qsort (array, nstrings, sizeof (char *), compare_elements);
}
thwab_sort_by_len(targets)
// load keywords
// and gen
keyword_norm=g_utf8_normalize(keyword,-1,G_NORMALIZE_ALL_COMPOSE);
keyword_fold=g_utf8_casefold(keyword_norm,-1);
keyword_coll=g_utf8_collate_key(keyword_fold,-1);
// search

foreach target in targets
{
 target_norm=g_utf8_normalize(target,-1,G_NORMALIZE_ALL_COMPOSE);
 target_fold=g_utf8_casefold(target_norm,-1);
 target_coll=g_utf8_collate_key(target_fold,-1);
 
 foreach keyword in keywords
 {
   if (strcmp(keyword_coll,target_coll)==0) thwab_found(1.0);
   else {
     // prefix suffix test
     s=strstr(keyword_coll,target_coll);
     if (s) {
       L=g_utf8_strlen(keyword_coll,-1);
       l=L-g_utf8_strlen(target_coll,-1); // excess letters
       thwab_found(1.0-(float)l/L);
     } else {
       
     } // endelse strcmp_prefix_suffix
   } // endelse strcmp_coll
 } // end foreach keyword
 g_free(target_coll); g_free(target_fold); g_free(target_norm);
} // end foreach target

////////////
thwab_found(keyword,p,pass)
{
  for each topic in keyword
  {
    c=count_topic();
    P=p*(1.0-(1.0/(1.0+c))) // this way is better than real or on them as too frequent keyword could be negligable like "to" "the"
    // and at same pass becomes or
    // when and-search finished remove all having pass!=max_pass or p=0
    if (AND && topic.pass!=pass) {
      if (topic.pass==pass-1) {topic.p*=P;  topic.pass=pass;}
      else {remove_topic // by setting p to 0}
    } else { // OR
      if (topic exists) topic.p=topic.p+P-(topic.p*P);
      else topic.p=P;
      topic.pass=pass;
    }
  }
}
*/
/*
ThwabTopic	*thwab_topic_new_by_id(Thwab* thwab, int id);
ThwabTopic	*thwab_topic_new_by_title(Thwab* thwab, char *title);

char		*thwab_topic_content(ThwabTopic	*, char *id);
void		thwab_topic_distroy(ThwabTopic *);
*/
