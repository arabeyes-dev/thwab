#ifndef _TH_LIB_CONFIG_
#define _TH_LIB_CONFIG_

#include <math.h>
#include <glib.h>
#define TH_FILES \
	"share" G_DIR_SEPARATOR_S "thwab-lib" G_DIR_SEPARATOR_S
#define TH_PATH \
	"thwab-lib" G_DIR_SEPARATOR_S G_SEARCHPATH_SEPARATOR_S \
	".." G_DIR_SEPARATOR_S TH_FILES G_SEARCHPATH_SEPARATOR_S \
	"~" G_DIR_SEPARATOR_S ".thwab-lib" G_DIR_SEPARATOR_S G_SEARCHPATH_SEPARATOR_S \
	"/usr/local/" TH_FILES G_SEARCHPATH_SEPARATOR_S \
	"/usr/" TH_FILES

#define TH_AFTER_HOME ".thwab-lib"
#define TH_FILES_CACHE TH_AFTER_HOME G_DIR_SEPARATOR_S "ls.txt"
#define TH_INFO_CACHE TH_AFTER_HOME G_DIR_SEPARATOR_S "info.txt"

#define TH_BOOKS "resources" G_DIR_SEPARATOR_S
#define TH_SUFFIX "tar.bz2"
#define TH_BOOK_CLOSED "close.png"
#define TH_BOOK_OPENED "open.png"

#endif
