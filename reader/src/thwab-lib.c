/* thwab-lib.c: Electronic Encyclopedia System GUI
 *
 * Thwab-lib - Electronic Encyclopedia System
 * by Muayyad Alsadi<alsadi@gmail.com>
 * released under terms of GNU GPL
 *
 * "in which there are valuable Books." V98:3
 *
 */
#define _GNU_SOURCE		/* 1st to get rid of non-GNU, 2nd to have strndup and alloca */
#include<stdio.h>
#include<unistd.h>
#include<sys/stat.h>
#include<gtk/gtk.h>
#include "libthwab-lib.h"
#include "libitar/libitar.h"
/* move some to config header file */
/* to turn of gettext uncomment next block and comment the later block */
/*
#define _(String) (String)
#define N_(String) String
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)
*/
/*********************/
/* real gettext */
#include <locale.h>
#include <libintl.h>
#define _(String) gettext (String)
#define gettext_noop(String) String
#define N_(String) gettext_noop (String)
/*********************/
/* really needed to be global */
Thwab *th=NULL;
char *convert_to_html_err[7];
GtkWidget *win,*ix_dlg,*ix_tree,*e1;
GtkClipboard* clip_p;
GtkTooltips *tips;
GtkAccelGroup *accel_group;
GdkPixbuf *open_book_pix=NULL,*close_book_pix=NULL;
/* should be members of a struct that is allocated for each viewer */
ThwabTopic *topic=NULL;
ThwabTopicItem* topic_item=NULL;
GtkWidget *txt, *toc_tree;
GtkTextBuffer *buff;
GtkWidget *search_win,*search_ls;
/* some needed and some are rubish */
GtkWidget *tree_,*tree;
GtkWidget *v1, *h1, *h2, *tb1, *hb, *vb, *tb, *w_tmp, *w1, *w2, *w3;
GtkPaned *hp;
GtkWidget *tools, *tools_bx, *tools_nb;
GtkWidget *xpander,*toc_expander,*toc_collapser;
GtkExpander *expander;
GtkToolItem *ti;
GtkDialog *wait_msg(GtkWindow *parent, char *msg)  // needs gtk_widget_destroy(dlg);
{
  GtkDialog *dlg=gtk_message_dialog_new_with_markup(parent,
    GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_NONE,"<span size=\"xx-large\" color=\"darkblue\">%s</span>\n%s",  _("Please wait..."), msg);
  gtk_widget_show_all (dlg);
  while (gtk_events_pending ()) gtk_main_iteration ();  
  return dlg;
}
void bad(GtkWindow *parent, char *msg)
{
  GtkDialog *dlg=gtk_message_dialog_new_with_markup
  (parent,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
  _("<span size=\"xx-large\" color=\"darkred\">Error</span>\n%s"),msg);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  gtk_widget_show_all (dlg);
  gtk_dialog_run(dlg);
  gtk_widget_destroy(dlg);
}

/*************************************
 * Here after, Splash related code *
 *************************************/
// generate a bitmap corresponding Magic Magenta
GdkBitmap *
pixbuf_gen_mask (GdkPixbuf * pixbuf)
{
  GdkBitmap *b;
  guchar *xbm, *xbm_p, *row;
  int xbm_line;
  int width, height, rowstride, n_channels;
  guchar *pixels, *p;
  int x, y;
/* another trick
GdkPixbuf*  gdk_pixbuf_add_alpha(const GdkPixbuf *pixbuf,
  TRUE,255,0,255); *//* Magic magenta */

  n_channels = gdk_pixbuf_get_n_channels (pixbuf);
  g_assert (gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
  g_assert (gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);
  g_assert (gdk_pixbuf_get_has_alpha (pixbuf));
  g_assert (n_channels == 4);

  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  xbm_line = (width + 7) >> 3;
  xbm = (guchar *) malloc (xbm_line * height);
  memset (xbm, 0, xbm_line * height);
  //g_assert (x >= 0 && x < width);
  //g_assert (y >= 0 && y < height);

  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  row = pixels = gdk_pixbuf_get_pixels (pixbuf);
  for (y = 0, xbm_p = xbm; y < height;
       ++y, row += rowstride, xbm_p += xbm_line)
    for (x = 0, p = row; x < width; ++x, p += n_channels)
      if (p[3] > 127 || (p[0] == 255 && p[1] == 0 && p[2] == 255))
	*(xbm_p + (x >> 3)) |= (1 << (x & 7));
  b = gdk_bitmap_create_from_data (NULL, xbm, width, height);
  free (xbm);
  return b;
  //p = pixels + y * rowstride + x * n_channels;
  //p[0] = red;
  //p[1] = green;
  //p[2] = blue;
  //p[3] = alpha;
}
GtkWidget *splash_txt;
GtkTextBuffer *splash_buff;
void splash_progress(gdouble f, char *title, GtkWidget *p)
{
  GtkTextIter iter;
  //printf("book [%s] loaded\n",title);
  //gtk_progress_bar_pulse (p);
  gtk_progress_bar_set_fraction(p,f);
  gtk_text_buffer_insert_at_cursor(splash_buff,"[",-1);
  gtk_text_buffer_insert_at_cursor(splash_buff,title,-1);
  gtk_text_buffer_insert_at_cursor(splash_buff,"]\n",-1);
  gtk_text_buffer_get_end_iter(splash_buff,&iter);
  gtk_text_view_scroll_to_iter(splash_txt,&iter,0.0,FALSE,0.0,0.0);
  while (gtk_events_pending ()) gtk_main_iteration ();
  //sleep(1);
}
gboolean
splash_out(GtkWidget *splash)
{
  gtk_widget_hide (splash);
  return FALSE;
}
void
init_and_load_thwab ()
{
  GtkWidget *splash;
  GtkWidget *pixmap, *p, *txt, *fixed,*w;
  GdkPixbuf *pix;
  GdkBitmap *mask = NULL;
  GtkStyle *style;
  GError *err = NULL;
  GdkGC *gc;
  GdkCursor *cursor;
  GtkTextIter iter;
  char *splash_file;
  int width, height;
  gtk_window_set_auto_startup_notification (FALSE);
  /* splash = gtk_window_new (GTK_WINDOW_POPUP);	// GTK_WINDOW_TOPLEVEL); */
  /* gtk_window_set_position (splash, GTK_WIN_POS_CENTER); */
    /*
     gtk_window_set_keep_above(splash, TRUE);
     gtk_window_set_decorated(splash,FALSE);
    */
  splash = g_object_new(GTK_TYPE_WINDOW,
	"type",GTK_WINDOW_POPUP,"type-hint", GDK_WINDOW_TYPE_HINT_SPLASHSCREEN,
	"window-position",GTK_WIN_POS_CENTER,
	"modal",TRUE,
	/* "decorated",FALSE, */
	/*
	"default-width",640,
	"default-height",400, */
	NULL
  );

  

  /* To display the pixmap, we use a fixed widget to place the pixmap */
  fixed = gtk_fixed_new ();
  style = gtk_widget_get_default_style ();
  gc = style->black_gc;

  if ((splash_file = thwab_filename (th,"images/splash.png"))
      && (pix = gdk_pixbuf_new_from_file (splash_file, &err)))
    {
      pixmap = gtk_image_new_from_pixbuf (pix);
      width = gdk_pixbuf_get_width (pix);
      height = gdk_pixbuf_get_height (pix);
      gtk_widget_set_size_request (fixed, width, height);
      mask = pixbuf_gen_mask (pix);
      gtk_fixed_put (GTK_FIXED (fixed), pixmap, 0, 0);
    }
  if (splash_file)
    {
      free (splash_file);
      if (!pix)
	{
	  fprintf (stderr, "Error: %s\n", err->message);
	}
    }
  p = gtk_progress_bar_new ();
  splash_txt=txt = gtk_text_view_new ();
  w = gtk_button_new_from_stock(GTK_STOCK_OK);
  /* w = g_object_new(GTK_TYPE_BUTTON,"label",GTK_STOCK_OK,
	"use-stock", TRUE,"receives-default",TRUE,"is-focus",TRUE, NULL);*/
  gtk_widget_set_size_request (txt, 300, 100);
  gtk_widget_set_size_request (p, 150, 16);
  gtk_fixed_put (GTK_FIXED (fixed), txt, 50, 200);
  gtk_fixed_put (GTK_FIXED (fixed), p, 35, 360);
  gtk_fixed_put (GTK_FIXED (fixed), w, 35, 360);
  splash_buff = gtk_text_view_get_buffer (txt);
  gtk_text_view_set_editable (txt, FALSE);
  gtk_progress_bar_set_text (p, _("Loading ..."));
  gtk_progress_bar_set_fraction (p, 0.0);
  //gtk_progress_bar_set_pulse_step (p, 0.17);

  gtk_container_add (GTK_CONTAINER (splash), fixed);
  gtk_widget_show (fixed);
  /* This masks out everything except for the image itself */
  if (mask)
    gtk_widget_shape_combine_mask (splash, mask, 0, 0);
  /* show the window */
  gtk_widget_show_all (splash);
  gtk_widget_show (p);
  gtk_widget_hide (w);

  /*
     gdk_drawable_get_size(p->window,&w,&h);
     gdk_window_get_position(p->window, gint *x, gint *y); */
  cursor = gdk_cursor_new (GDK_WATCH);
  gdk_window_set_cursor (splash->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending ())
    gtk_main_iteration ();
  thwab_init(th,splash_progress, p);
  //splash_progress(1.0, _("Done!"), p);
  gtk_text_buffer_insert_at_cursor(splash_buff,_("Done!\n"),-1);
  gtk_text_buffer_get_end_iter(splash_buff,&iter);
  gtk_text_view_scroll_to_iter(splash_txt,&iter,0.0,FALSE,0.0,0.0);

  
  gtk_widget_hide (p);
  gtk_widget_show (w);
  
  g_signal_connect_swapped (w,"clicked",G_CALLBACK (gtk_widget_hide),splash);
  while (gtk_events_pending ()) gtk_main_iteration ();
  g_timeout_add(4000,splash_out,splash);
  cursor = gdk_cursor_new (GDK_LEFT_PTR);
  gdk_window_set_cursor (splash->window, cursor);
  gdk_cursor_destroy (cursor);
  gtk_window_set_auto_startup_notification (TRUE);
  //gtk_widget_hide (splash);
}

/*************************************
 * Here after, TOC tree related code *
 *************************************/
enum { TOC_TREE_I ,TOC_TREE_ID, TOC_TREE_STR, TOC_TREE_N };
GtkWidget *
gen_toc_tree()
{
  GtkWidget *toc_tree;
  GtkTreeStore *store;
  GError *err = NULL;
  GdkPixbuf *pix;
  GtkCellRenderer *renderer,*r;
  GtkTreeViewColumn* col;
  char *fn;
  store = gtk_tree_store_new (TOC_TREE_N, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING);
  toc_tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  /* add columns */
  // title special column
  col=gtk_tree_view_column_new();
  gtk_tree_view_column_set_title  (col,"TOC");

  r = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (col, r, 0);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (col,renderer,TRUE);
  g_object_set (renderer, "xalign", 0.0, NULL);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TOC_TREE_STR);
  gtk_tree_view_column_set_attributes(col,renderer,"text", TOC_TREE_STR, NULL);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (toc_tree), col,-1);
  fn = thwab_filename (th, "images/file.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  // if (!pix) {printf("Error: %s\n",err->message);}
  free (fn);
  if (pix) g_object_set (r, "pixbuf", pix, NULL);
  fn = thwab_filename (th, "images/book-close.png");
  close_book_pix=pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-closed", pix, NULL);
  fn = thwab_filename (th, "images/book-open.png");
  open_book_pix=pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-open", pix, NULL);
  g_object_set (r, "xalign", 0.0, "xpad", 0, NULL);
  /* set tree attributes */
  gtk_tree_view_set_headers_visible (toc_tree, FALSE);
  //gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (toc_tree), TRUE); // it look nicer 
  gtk_tree_view_set_enable_search (toc_tree, TRUE);
  gtk_tree_view_set_search_column (toc_tree, TOC_TREE_STR);
  return toc_tree;
}
/***********************
TODO: faster newer method based on the fact that toc is sorted
while(new is not a child of lastclass) go_up();
add_it_as child();
 ***********************/
static inline int
is_toc_child(char *id_parent,char *id_child)
{
char *p=NULL;
if (!id_parent || *id_parent=='\0' || (p=strstr(id_child,id_parent)) != NULL && (p-id_child)==0) {return 1;}
return 0;
}
GtkTreeIter toc_iter;
GtkTreeIter *last_parent_iter;
char last_parent_id[ITARNMAX];
static inline void
toc_go_up(char *last, GtkTreeStore *store)
{
  int b;
  char *p;
  GtkTreeIter i;
  GtkTreeIter *i_new=&i;
  if (!last) return;
  p=strrchr(last,'/');
  if (p) *p='\0';
  else *last='\0';
  b=gtk_tree_model_iter_parent(store, i_new, last_parent_iter);
  if (!b) last_parent_iter=NULL;
  else {toc_iter=i; last_parent_iter=&toc_iter;}
}

void
add_toc_item(GtkTreeStore *store, int i, int is_parent, char *id, char *title)
{
  GtkTreeIter iter;
  GtkTreeIter *new_iter=&iter;
  while(!is_toc_child(last_parent_id,id)) {toc_go_up(last_parent_id, store);}
  gtk_tree_store_append (store, new_iter, last_parent_iter);
  gtk_tree_store_set (store, new_iter, TOC_TREE_I, i, TOC_TREE_ID, id, TOC_TREE_STR, title, -1);
  if (is_parent) {strcpy(last_parent_id, id); toc_iter=iter; last_parent_iter=&toc_iter;}
}

/* TODO: if title is "_" and last add is dir then just replace the id for the last dir to be this file */
/*void
add_toc_item(GtkTreeStore *store, int i, char *id, char *title)
{
  int n,found=0;
  char *s,*s1,*s2,*s3;
  GtkTreeIter iter0,iter1;
  GtkTreeIter *iter=&iter0,*parent=NULL;
  if (id[0]=='/') ++id;
  s1=s=strdupa(id);
  if (gtk_tree_model_iter_children(store,iter, NULL))
  {
    while(*s1)
    {
      s2=strchr(s1,'/');
      if (!s2) break;
      *s2='\0';
      do
      {
        gtk_tree_model_get(store, iter, TOC_TREE_ID, &s3, -1);
        if (strcmp(s, s3)==0)
        {
          found=1;
	  free(s3);
	  iter1=*iter;
	  parent=&iter1;
          break;
        }
        free(s3);
      } while(gtk_tree_model_iter_next(store,iter));
      *s2='/';
      s1=s2+1;
      iter1=*iter;
      parent=&iter1;
      gtk_tree_model_iter_children(store,iter, parent);
    }
  }
  gtk_tree_store_append (store, iter, parent);
  gtk_tree_store_set (store, iter, TOC_TREE_I,i,TOC_TREE_ID,id,TOC_TREE_STR,title,-1);
}
void
gen_sample_toc(GtkWidget *toc_tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);  
  gtk_tree_store_clear (store);
  add_toc_item(store, 0, "01", "Chapter 1");
  add_toc_item(store, 1, "01/01", "Section 1.1");
  add_toc_item(store, 2, "01/02", "Section 1.2");
  add_toc_item(store, 3, "01/03", "Section 1.3");
  add_toc_item(store, 4, "02", "Chapter 2");
  add_toc_item(store, 5, "02/01", "Section 2.1");
  add_toc_item(store, 6, "02/02", "Section 2.2");
  add_toc_item(store, 7, "02/02/01", "Subsection 2.2.1");
  add_toc_item(store, 8, "02/02/02", "Subsection 2.2.2");
  add_toc_item(store, 9, "02/03", "Section 2.3");
}*/
/******************************************
 * Here after, Index dialog and tree code *
 ******************************************/
enum { TH_IX_ID, TH_IX_TITLE,TH_IX_SUBTITLE, TH_IX_AUTHORS, TH_IX_YEAR, TH_IX_GPG, TH_IX_SOURCE ,TH_IX_N };
enum { TH_IX_C_TITLE,TH_IX_C_SUBTITLE, TH_IX_C_AUTHORS, TH_IX_C_YEAR, TH_IX_C_GPG, TH_IX_C_SOURCE ,TH_IX_C_N };
GtkWidget *
gen_index_tree()
{
  GtkWidget *w, *tree;
  GtkTreeStore *store;
  GError *err = NULL;
  GdkPixbuf *pix;
  GtkCellRenderer *renderer,*r;
  GtkTreeViewColumn* col;
  int i;
  char *fn;
  
  store = gtk_tree_store_new (TH_IX_N, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN, G_TYPE_STRING);
  tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  // title special column
  col=gtk_tree_view_column_new();
  gtk_tree_view_column_set_title  (col,_("Title"));

  r = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (col,r,0);
  
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (col,renderer,TRUE);
  g_object_set (renderer, "xalign", 0.0, NULL);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_TITLE);
  gtk_tree_view_column_set_attributes(col,renderer,"text", TH_IX_TITLE, NULL);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (tree), col,-1);
  // the rest of columns
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, _("Subtitle"), renderer, "text", TH_IX_SUBTITLE, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, _("Authors"), renderer, "text", TH_IX_AUTHORS, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, _("Date"), renderer, "text", TH_IX_YEAR, NULL);
  renderer = gtk_cell_renderer_toggle_new();
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_GPG);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, _("Valid"), renderer, "active", TH_IX_GPG, NULL);
  renderer = gtk_cell_renderer_pixbuf_new();
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)TH_IX_SOURCE);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree), -1, "@", renderer, "stock-id", TH_IX_SOURCE, NULL);
  /* add icon to col #1 */
  fn = thwab_filename (th, "images/book-close.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  // if (!pix) {printf("Error: %s\n",err->message);}
  free (fn);
  if (pix) g_object_set (r, "pixbuf", pix, NULL);
  fn = thwab_filename (th, "images/drawer-close.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-closed", pix, NULL);
  fn = thwab_filename (th, "images/drawer-open.png");
  pix = gdk_pixbuf_new_from_file (fn, &err);
  free (fn);
  if (pix) g_object_set (r, "pixbuf-expander-open", pix, NULL);
  g_object_set (r, "xalign", 0.0, "xpad", 0, NULL);
  
  /* tree attributes */
  col=gtk_tree_view_get_column (tree,TH_IX_C_TITLE);
   gtk_tree_view_column_set_resizable(col,TRUE);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_TITLE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_SUBTITLE);
   gtk_tree_view_column_set_resizable(col,TRUE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_AUTHORS);
   gtk_tree_view_column_set_resizable(col,TRUE);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_AUTHORS);
  col=gtk_tree_view_get_column (tree,TH_IX_C_YEAR);
   gtk_tree_view_column_set_sort_column_id(col,TH_IX_YEAR);
   gtk_tree_view_set_headers_visible (tree, TRUE);
  col=gtk_tree_view_get_column (tree,TH_IX_C_YEAR);
  // hide extra columns
  for (i=TH_IX_C_SUBTITLE+1;i<TH_IX_C_N-1;++i)
  {
    col=gtk_tree_view_get_column (tree, i);
    gtk_tree_view_column_set_visible(col,FALSE);
  }
  //gtk_tree_view_set_headers_clickable(tree,TRUE);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (tree), TRUE);
  gtk_tree_view_set_enable_search (tree, TRUE);
  gtk_tree_view_set_search_column (tree, TH_IX_TITLE);
  
  return tree;
}
/* FIXME: there is a bug here */
void
get_or_mk_index_class(GtkTreeStore *store, GtkTreeIter *parent, GtkTreeIter *iter, char *classify)
{
  int n,found=0;
  char *s,*str;
  GtkTreeIter iter0=*iter,iter1=*iter;
  s=strchr(classify,':');
  if (s!=NULL) *s='\0';
  if (!gtk_tree_model_iter_children(store,&iter1, parent))
  {
    gtk_tree_store_append (store, &iter1, parent);
    gtk_tree_store_set (store, &iter1, TH_IX_TITLE,classify,-1);
    iter0=iter1;
    if (s) get_or_mk_index_class(store, &iter0, &iter1, s+1);
    *iter=iter1;
  } else // search children for class
  {
    do
    {
      gtk_tree_model_get(store, &iter1, TH_IX_TITLE, &str, -1);
      if (strcmp(classify, str)==0)
      {
        found=1;
        iter0=iter1;
        if (s) get_or_mk_index_class(store, &iter0, &iter1, s+1);
        //else printf("stop\n");
	free(str);
        break;
      }
      free(str);
    } while(gtk_tree_model_iter_next(store,&iter1));
    if (! found)
    {
      gtk_tree_store_append (store, &iter1, parent);
      gtk_tree_store_set (store, &iter1, TH_IX_TITLE,classify,-1);
      iter0=iter1;
      if (s) get_or_mk_index_class(store, &iter0, &iter1, s+1);
      *iter=iter1;
    }
  }
  *iter=iter1; // set iter to the iter we found or created
}
void
mk_new_index_class(GtkTreeStore *store, GtkTreeIter *parent, GtkTreeIter *iter, char *classify)
{
    char *s=classify;
    GtkTreeIter iter0=*iter,iter1=*iter;
    s=strchr(classify,':');
    //printf("ix_new Called [%s]\n",classify);
    if (s!=NULL) {*s='\0';}
    gtk_tree_store_append (store, &iter1, parent);
    gtk_tree_store_set (store, &iter1, TH_IX_TITLE,classify,-1);
    iter0=iter1;
    if (s) get_or_mk_index_class(store, &iter0, &iter1, s+1);
    *iter=iter1; // set iter to the iter we found or created
}

void 
index_add(GtkTreeStore *store, char *classify, int id,char *title, char *subtitle,
	char *authors,int year,int gpg,char *source)
{
  GtkTreeIter iter,iter0;
  if (gtk_tree_model_get_iter_first(store, &iter)==FALSE)
  {
//     gtk_tree_store_append (store, &iter, NULL);	/* Acquire a top-level iterator */
//     gtk_tree_store_set (store, &iter, TH_IX_TITLE,classify,-1);
    mk_new_index_class(store, NULL, &iter, classify);
  } else get_or_mk_index_class(store, NULL, &iter, classify);
  gtk_tree_store_append (store, &iter0, &iter);
  iter=iter0;
  gtk_tree_store_set(store, &iter, TH_IX_ID,id,TH_IX_TITLE, title, TH_IX_SUBTITLE, subtitle,TH_IX_AUTHORS,authors,TH_IX_YEAR,year, TH_IX_SOURCE, source,-1);
}
gboolean
gen_index_cb(gpointer key, ThwabTopicItem *item, GtkTreeStore *store)
{
  char *p=item->classify,*s,*s1=NULL;
  while (1)
  {
    s = strchr (p, ',');
    if (!s) break;
    s1 = strndup (p, s - p);
    //printf("@@%s(%d)\n",item->title,item->id);
    index_add(store, s1,item->id, item->title, item->subtitle,
      item->authors,0,0,GTK_STOCK_HARDDISK);
    p=s+1;
    free(s1);
  }
  //printf("@@%s(%d)\n",item->title,item->id);
  index_add(store, p, item->id,item->title, item->subtitle,
    item->authors,0,0,GTK_STOCK_HARDDISK);
  return FALSE;
}
void
gen_index(GtkWidget *tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(tree);
  gtk_tree_store_clear (store);
  thwab_foreach_topic(th, gen_index_cb,store);
}
void
ix_toggle_details(GtkToggleButton *b1, GtkWidget *w)
{
  GtkTreeViewColumn* col;
  int i,j=gtk_toggle_button_get_active (b1);
  // hide extra columns
  for (i=TH_IX_C_SUBTITLE+1;i<TH_IX_C_N-1;++i)
  {
    col=gtk_tree_view_get_column (w, i);
    gtk_tree_view_column_set_visible(col,j);
  }

}

GtkDialog*
index_dlg_new()
{
  int i,j;
  GtkWidget *hb,*b1,*w1,*w2;
  GtkTreeStore *store;
  GtkDialog *dlg=gtk_dialog_new_with_buttons(_("Index"),win, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
  GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,NULL);
  gtk_widget_set_size_request (dlg, 400, 400);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  hb = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start ((GTK_DIALOG(dlg)->vbox), hb, FALSE, FALSE, 0);
  b1=gtk_toggle_button_new_with_label(_("Details"));
  gtk_button_set_focus_on_click   (b1,FALSE);
  gtk_toggle_button_set_mode(b1,TRUE);
  gtk_box_pack_start (hb, b1, FALSE, FALSE, 0);
  // tree
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  w2 = gen_index_tree();
  gen_index(w2); //gen_sample_index(w2);
  gtk_tree_view_expand_all(w2);
  store=(GtkTreeStore *)gtk_tree_view_get_model(w2);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start ((GTK_DIALOG(dlg)->vbox), w1, TRUE, TRUE, 0);
  ix_tree=w2;
  g_signal_connect (b1,"toggled",ix_toggle_details,w2);
  return dlg;
}

void book_toc_cb(int i,int is_parent,const char *id, const char *title,GtkTreeStore *store)
{
  add_toc_item(store, i, is_parent, id, title);
}

void
open_book(int id)
{
  GtkDialog *dlg;
  //printf("id=%d\n",id);
  ThwabTopicItem* item=thwab_get_topic_item(th,id);
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);
  if (topic) thwab_topic_destroy(topic);
  dlg=wait_msg(win,_("Generating Table of contents")); // needs gtk_widget_destroy(dlg);
  gtk_tree_store_clear (store);
  topic=thwab_topic_new(th, item);
  last_parent_id[0]='\0';
  last_parent_iter=NULL;
  thwab_topic_foreach(topic, book_toc_cb, store);
  //gtk_tree_view_expand_all(toc_tree);
  gtk_widget_destroy(dlg);
  gtk_text_buffer_set_text(buff, item->comment,-1);
  gtk_window_set_title(win, item->title);
  topic_item=item;
}
void
info_cb(GtkWidget *w, gpointer user)
{
  if (topic) gtk_text_buffer_set_text(buff, topic_item->comment,-1);
  else bad(win,_("No topic is opened"));
}
void
index_cb()
{
  int i,j;
  char *s;
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(ix_tree);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeViewColumn *focus_column;
  do
  {
    j=1;
    gtk_widget_show_all (ix_dlg);
    i=gtk_dialog_run(ix_dlg);
    if (i==GTK_RESPONSE_ACCEPT)
    {
      gtk_tree_view_get_cursor(ix_tree, &path, &focus_column);
      if (!path) bad(ix_dlg, _("Nothing Selected"));
      else if (! gtk_tree_model_get_iter(store,&iter,path) ||
         gtk_tree_model_iter_has_child(store, &iter)) bad(ix_dlg, _("Select a book!"));
      else {
        gtk_tree_model_get (store, &iter, TH_IX_ID, &i, TH_IX_TITLE,&s,-1);
        /*printf("[%d][%s] OK\n",i,s);*/
        j=0;
	open_book(i);
	/* printf("[ok]\n"); */
	free(s);
	}
      gtk_tree_path_free(path);
    } else
    { // cancel
      j=0;
    }
  } while(j);
  //gtk_widget_destroy(dlg);
}

void
open_text_cb(GtkTreeView *toc_tree, gpointer user)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeViewColumn *focus_column;
  
  int i,l;
  char *s;
  gtk_tree_view_get_cursor(toc_tree, &path, &focus_column);
  if (!path || ! gtk_tree_model_get_iter(store,&iter,path) ) return;
  else
  {
    gtk_tree_model_get (store, &iter, TOC_TREE_I, &i,-1);
    s=thwab_topic_content(topic, i,&l);
    if (!s) {/* printf("what!!!\n");*/ return;}
    gtk_text_buffer_set_text(buff, _("Please wait..."),-1);
    while (gtk_events_pending ()) gtk_main_iteration ();
    gtk_text_buffer_set_text(buff, s, l);
    free(s);
  }
}
void
font_cb(GtkFontButton *fb, gpointer user)
{
  const char *fn=gtk_font_button_get_font_name  (fb);
  PangoFontDescription *font_desc = pango_font_description_from_string (fn);
  printf("new font [%s]\n",fn);
  gtk_widget_modify_font (txt, font_desc);
  pango_font_description_free (font_desc);
}
void xpander_cb(GtkToggleButton *tb, GtkWidget *w)
{
  int W1=0, H1=0,W=0;
  GtkRequisition r;
  gtk_widget_size_request(tb,&r);
  W1=r.width;
  W=(gtk_toggle_button_get_active(tb))?W1*8:W1;
  g_object_set(G_OBJECT(w),"visible", gtk_toggle_button_get_active(tb), NULL);
  GtkPaned *p=gtk_widget_get_parent (gtk_widget_get_parent (gtk_widget_get_parent (xpander)));
  gtk_paned_set_position(p,W);
}
void lookupkey_cb(GtkButton *b, gpointer   user)
{
    char *s,*id;
    int l;
    const char *k=gtk_entry_get_text(e1);
    id=thwab_topic_lookup_key(topic, k);
    if (!id) return;
    s=thwab_topic_content_by_id(topic, id,&l);
    if (!s) return;
    gtk_text_buffer_set_text(buff, _("Please wait..."),-1);
    while (gtk_events_pending ()) gtk_main_iteration ();
    gtk_text_buffer_set_text(buff, s, l);
    free(s);
}
void clear_find_e_cb(GtkWidget *b,GtkWidget *e)
{
  gtk_entry_set_text(e,"");
  gtk_widget_grab_focus (e);
}
void paste_find_e_cb(GtkWidget *b,GtkWidget *e)
{
  char *s;
  s=gtk_clipboard_wait_for_text(clip_p);
  gtk_entry_set_text(e,s);
  free(s);
}
void in_page_find_cb(GtkButton *b, gpointer   user);
enum { FIND_RESULT_ID, FIND_RESULT_TITLE, FIND_RESULT_RANK,FIND_RESULT_N };
void
search_open_text_cb(GtkTreeView *ls, gpointer user)
{
  GtkListStore *store=(GtkListStore *)gtk_tree_view_get_model(ls);
  GtkTreePath *path;
  GtkTreeIter iter;
  GtkTreeViewColumn *focus_column;
  
  int l;
  char *s,*id;
  gtk_tree_view_get_cursor(ls, &path, &focus_column);
  if (!path || ! gtk_tree_model_get_iter(store, &iter, path) ) return;
  else
  {
    gtk_tree_model_get (store, &iter, FIND_RESULT_ID, &id,-1);
    s=thwab_topic_content_by_id(topic, id,&l);
    if (!s) return;
    gtk_text_buffer_set_text(buff, _("Please wait..."),-1);
    while (gtk_events_pending ()) gtk_main_iteration ();
    gtk_text_buffer_set_text(buff, s, l);
    in_page_find_cb(NULL, NULL);
    while (gtk_events_pending ()) gtk_main_iteration ();
    free(s);
    
  }
}
/*gboolean
return_true(GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
  gtk_widget_hide(widget);
  return TRUE;
}*/
GtkDialog*
mk_search_win()
{
  int i,j;
  GtkWidget *vb,*b1,*w1,*w2,*ls;
  GtkExpander *ex;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *col;
  GtkListStore *store=gtk_list_store_new(FIND_RESULT_N, G_TYPE_STRING, G_TYPE_STRING,G_TYPE_FLOAT);
  GtkDialog *dlg=gtk_dialog_new_with_buttons(_("Search Result"),win, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_STOCK_CLOSE, GTK_RESPONSE_REJECT,NULL);
  gtk_widget_set_size_request (dlg, 200, 300);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  //g_signal_connect (dlg,"destroy-event",return_true,NULL);
  vb = GTK_DIALOG(dlg)->vbox;
  ex=gtk_expander_new(_("Details"));
  gtk_box_pack_start (vb, ex, FALSE, FALSE, 0);
  ls = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (ls),FIND_RESULT_TITLE);
  gtk_tree_view_set_enable_search (GTK_TREE_VIEW (ls), TRUE);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (ls), TRUE);

  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (w1), ls);
  gtk_box_pack_start (vb, w1, TRUE, TRUE, 0);
  // list columns
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ls), -1, _("ID"), renderer, "text", FIND_RESULT_ID, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ls), -1, _("Title"), renderer, "text", FIND_RESULT_TITLE, NULL);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (ls), -1, _("Rank"), renderer, "text", FIND_RESULT_RANK, NULL);
  //gtk_container_add (GTK_CONTAINER (sw), treeview);
  col=gtk_tree_view_get_column (ls, FIND_RESULT_ID);
  gtk_tree_view_column_set_resizable(col,TRUE); 
  gtk_tree_view_column_set_sort_column_id(col,FIND_RESULT_ID);
  col=gtk_tree_view_get_column (ls, FIND_RESULT_TITLE);
  gtk_tree_view_column_set_resizable(col,TRUE);
  gtk_tree_view_column_set_sort_column_id(col,FIND_RESULT_TITLE);
  col=gtk_tree_view_get_column (ls, FIND_RESULT_RANK);
  gtk_tree_view_column_set_resizable(col,TRUE);
  gtk_tree_view_column_set_sort_column_id(col,FIND_RESULT_RANK);
  search_ls=ls;

  g_signal_connect (G_OBJECT (ls), "cursor-changed", search_open_text_cb,NULL);
  
  return dlg;
}
void find_cb_cb(char *id,float r, GtkListStore *store)
{
  GtkTreeIter iter;
  //printf("%s %g\n",id,r);
  char *t=NULL;
  t=(char *)thwab_topic_content_title_by_id(topic, id);
  if (!t) {
    t=(char *)thwab_topic_content_key_by_id(topic, id);
    if (!t) t=_("No Title");
  }
  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter, FIND_RESULT_ID, id, FIND_RESULT_TITLE, t,  FIND_RESULT_RANK, r,-1);
}
void find_cb(GtkButton *b, gpointer   user)
{
  const char *s=gtk_entry_get_text(e1);
  char **targets=g_strsplit_set(s," \t\n",0);
  int i;
  GtkListStore *store=(GtkListStore *)gtk_tree_view_get_model(search_ls);  
  gtk_list_store_clear (store);
  thwab_topic_find(topic, targets, 1, find_cb_cb, store);
  g_strfreev(targets);
  gtk_tree_sortable_set_sort_column_id(store,FIND_RESULT_RANK,GTK_SORT_DESCENDING);
  gtk_widget_show_all(search_win);
  i=gtk_dialog_run(search_win);
  in_page_find_cb(NULL, NULL);
  
}
void in_page_find(char **targets)
{
  char *target;
  int i=0,j=0;
  GtkTextMark *m;
  GtkTextIter iter,i0,i1,i2,i_min;
  m=gtk_text_buffer_get_insert(buff);
  //gtk_text_buffer_get_iter_at_mark(buff,&iter,m);
  gtk_text_buffer_get_start_iter(buff,&i0);
  gtk_text_buffer_get_end_iter(buff,&i_min);
  gtk_text_buffer_remove_tag_by_name (buff, "SearchHighlight", &i0, &i_min);
  while(target=targets[i])
  {
  iter=i0;
  while(gtk_text_iter_forward_search(&iter,target,0, &i1, &i2,NULL))
  {
     j=1;
     gtk_text_buffer_apply_tag_by_name(buff,"SearchHighlight",&i1,&i2);
     if (gtk_text_iter_compare(&i_min,&i1)>0)  i_min=i1;
     iter=i2;
  }
  ++i;
  }
  if (!j)  i_min=i0;
  gtk_text_buffer_place_cursor(buff,&i_min);
  gtk_text_view_place_cursor_onscreen(txt);
  gtk_text_view_scroll_to_iter    (txt, &i_min, 0.0, FALSE, 0.0, 0.0);
  gtk_widget_grab_focus(txt);
}
void in_page_find_cb(GtkButton *b, gpointer   user)
{
  const char *s=gtk_entry_get_text(e1);
  char **targets=g_strsplit_set(s," \t\n",0);
  int i;
  in_page_find(targets);
  g_strfreev(targets);
}
void
convert_cb(GtkButton *b, gpointer   user)
{
  char *dir,*fn,*p;
  GtkDialog *dlg;
  int r=0;
  //time_t tm;
  double tm_d;
  GTimer *tm;
  if (! topic) { bad(win,_("No topic is opened")); return;}
  fn=g_path_get_basename(thwab_topic_get_filename(topic));
  printf("[%s]\n",fn);
  if ((p=strchr(fn,'.'))!=NULL) {*p='\0';}
  printf("[%s]\n",fn);
  dir=g_build_filename(g_get_home_dir(), "Thwab-Converted", fn,NULL);
  if (g_mkdir_with_parents(dir,0755)) { bad(win,_("Could not create directory")); free(dir); free(fn); return;}
  dlg=wait_msg(win,_("Converting to HTML...")); // needs gtk_widget_destroy(dlg);
  //tm=time(NULL);
  tm=g_timer_new();
  g_timer_start(tm);
  printf("DIR=[%s]\n",dir);
  r=thwab_topic_convert_to_html(topic, th, dir);
  printf("r=%d\n",r);
  gtk_widget_destroy(dlg);
  g_timer_stop(tm);
  tm_d=g_timer_elapsed(tm,NULL);
  g_timer_destroy(tm);
  //tm_d=difftime (time(NULL), tm);
  if (r<0) {
    bad(win, convert_to_html_err[(r>-6)?-r:0]);
    free(dir); free(fn); return;
  }
  dlg=gtk_message_dialog_new_with_markup
  (win,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
  _("<span size=\"xx-large\" color=\"darkblue\">Converted</span>\nThe book is converted to HTML in %.3f sec, it could be found in [%s]."), tm_d, dir);
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  gtk_widget_show_all (dlg);
  gtk_dialog_run(dlg);
  gtk_widget_destroy(dlg);
  free(dir);
  free(fn);
}
void
refresh_cb(GtkWidget *b, gpointer   user)
{
  GtkDialog *dlg;
  char *fn;
  fn= g_build_filename (g_get_home_dir(),TH_FILES_CACHE, NULL);
  unlink(fn);
  free(fn);
  fn= g_build_filename (g_get_home_dir(),TH_INFO_CACHE, NULL);
  unlink(fn);
  free(fn);
  dlg=gtk_message_dialog_new_with_markup  (win,
   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
    "\t<span size=\"xx-large\" color=\"darkblue\">%s</span>\n%s",
   _("Warning:"),_("Next startup time will be longer."));
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  gtk_widget_show_all (dlg);
  gtk_dialog_run(dlg);
  gtk_widget_destroy(dlg);

}
void
not_imp_cb(GtkWidget *b, gpointer   user)
{
  bad(win,_("This feature is not yet implemented"));
}
void
help_cb(GtkWidget *b, gpointer   user)
{
  GtkDialog *dlg=gtk_message_dialog_new_with_markup  (win,
   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
    "\t<span size=\"xx-large\" color=\"darkred\">%s</span>\n%s\n<span color=\"darkblue\"><u>www.Thwab.net</u></span>\n\n"
   "\t<span size=\"xx-large\" color=\"brown\">%s</span>\nCopyright © 2007, Muayyad Saleh Alsadi &lt;alsadi[AT]gmail[DOT]com&gt;\nReleased under terms of GNU GPL v2.0\n\n"
   "\t<span size=\"xx-large\" color=\"darkgreen\">%s</span>\n%s\n\n"
   "\t<span size=\"xx-large\" color=\"darkblue\">%s</span>\n%s",
   _("About"),_("Thwab is an Electronic Encyclopedia System\n {in which there are valuable Books.} V98:3"),
   _("Copyrights"),
   _("Translators"),_("This is the native C Interface, here translators should add their names like this:\n\tMuayyad Saleh AlSadi <alsadi[AT]gmail[DOT]com>"),
   _("Help"),_("Thwab Help is available in Thwab format, click on [Index] button then choose [Thwab Reference].\nHelp is also available in HTML format found in [doc] directory")   );
  g_signal_connect_swapped (dlg,"response",G_CALLBACK (gtk_widget_hide),dlg);
  gtk_widget_show_all (dlg);
  gtk_dialog_run(dlg);
  gtk_widget_destroy(dlg);
}


int
main (int argc, char *argv[])
{
  char *fn;
  PangoFontDescription *font_desc;
  char default_font[]="Sans 24";
  char *bin_dir=g_path_get_dirname(argv[0]);
  char *loc_dir=g_build_filename(bin_dir,"../share/locale/",NULL);
  int i;
  //printf("%s\n",loc_dir);
  //exit(-1);
  setlocale(LC_ALL, "");
  //setlocale(LC_MESSAGES, "ar_JO.UTF-8");
  textdomain("thwab");
  bindtextdomain("thwab", loc_dir);
  g_free(bin_dir);
  g_free(loc_dir);
  i=0;
  convert_to_html_err[i++]=_("Could not Convert, Internal error.");
  convert_to_html_err[i++]=_("Not topic is opened, or topic has no contents.");
  convert_to_html_err[i++]=_("Already converted! or at least directory exists.");
  convert_to_html_err[i++]=_("Could not copy HTML template files.");
  convert_to_html_err[i++]=_("Error loading HTML template file");
  convert_to_html_err[i++]=_("Could not create files, check free space.");

  gtk_init (&argc, &argv);
  th=thwab_new(argv[0]);
  //prefix = get_prefix (argv[0]);
  init_and_load_thwab ();
  /* allocate the GUI members */
  /* win= gtk_window_new (GTK_WINDOW_TOPLEVEL); */
  /* gtk_widget_set_size_request (win, 640, 400); */
  GError *err=NULL;
  fn = thwab_filename (th, "images/thwab-lib.png");
  gtk_window_set_default_icon_from_file(fn,&err);
  free (fn);
  win = g_object_new(GTK_TYPE_WINDOW,
	"type",GTK_WINDOW_TOPLEVEL,
	"title",_("Thwab Library"),
	/*"icon",icon, */
	"default-width",640,
	"default-height",400,
	"urgency-hint",TRUE,
	NULL
  );
  g_signal_connect (G_OBJECT (win), "destroy", G_CALLBACK (gtk_main_quit),
		    NULL);
  
  tips = gtk_tooltips_new ();
  clip_p=gtk_clipboard_get (gdk_atom_intern("PRIMARY",0));
  accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW (win), accel_group);
  search_win=mk_search_win();

  vb = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (win), vb);
  tools = gtk_toolbar_new ();
  gtk_box_pack_start (vb, tools, FALSE, FALSE, 0);
  gtk_toolbar_set_show_arrow (tools, TRUE);
  gtk_toolbar_set_tooltips (tools, TRUE);

  ti = gtk_tool_button_new_from_stock (GTK_STOCK_INDEX);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (index_cb),NULL);
  fn = thwab_filename (th, "images/related.png");
  w1 = gtk_image_new_from_file (fn);
  free (fn);
  ti=gtk_tool_button_new(w1,_("Related"));
  gtk_tool_item_set_tooltip (ti, tips, _("Open book related to the current one"), NULL);
  gtk_toolbar_insert(tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (not_imp_cb),NULL);
/*  ti = gtk_tool_button_new_from_stock (GTK_STOCK_PROPERTIES);
  gtk_toolbar_insert (tools, ti, -1); */
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_DIALOG_INFO);
  gtk_tool_button_set_label (ti, _("Info"));
  gtk_tool_item_set_tooltip (ti, tips, _("Display book information"), NULL);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (info_cb),NULL);
  
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_DIALOG_AUTHENTICATION);
  gtk_tool_button_set_label (ti, _("GPG"));
  gtk_tool_item_set_tooltip (ti, tips, _("validate book e-sign"), NULL);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (not_imp_cb),NULL);
  gtk_toolbar_insert (tools, ti, -1);
  
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_CONVERT);
  gtk_tool_item_set_tooltip (ti, tips, _("Convert book to HTML"), NULL);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (convert_cb),NULL);
  
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_REFRESH);
  gtk_tool_item_set_tooltip (ti, tips, _("Remove fast start cache, new one will be generated."), NULL);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (refresh_cb),NULL);
  
  
  /*ti = gtk_tool_button_new_from_stock (GTK_STOCK_CLOSE);
  gtk_toolbar_insert (tools, ti, -1);
  */
  /*
  gtk_toolbar_insert (tools, gtk_separator_tool_item_new (), -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GOTO_FIRST);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_BACK);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_UP);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GO_FORWARD);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_GOTO_LAST);
  gtk_toolbar_insert (tools, ti, -1);
  */
  gtk_toolbar_insert (tools, gtk_separator_tool_item_new (), -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_PREFERENCES);
  gtk_toolbar_insert (tools, ti, -1);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (not_imp_cb),NULL);
  
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_HELP);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (help_cb),NULL);
  gtk_toolbar_insert (tools, ti, -1);
  ti = gtk_tool_button_new_from_stock (GTK_STOCK_QUIT);
  g_signal_connect (G_OBJECT (ti), "clicked", G_CALLBACK (gtk_main_quit),NULL);
  gtk_toolbar_insert (tools, ti, -1);

/*
  w_tmp=gtk_entry_new();
  ti=gtk_tool_button_new(icon_wid,"Label");
  gtk_toolbar_insert(tools,ti,-1);
  */
/*
  ti=gtk_tool_item_new();
  gtk_tool_item_set_homogeneous(ti,FALSE);
  gtk_tool_item_set_expand(ti,FALSE);
  gtk_tool_item_set_tooltip(ti, tips,"Some tip",NULL);
  w_tmp=gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(ti),w_tmp);
  gtk_toolbar_insert(tools,ti,-1);
*/
  hb = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (vb, hb, FALSE, FALSE, 2);
  e1 = gtk_entry_new ();
  
  w1 = gtk_button_new();
  w2=gtk_image_new_from_stock (GTK_STOCK_PASTE,GTK_ICON_SIZE_MENU);
  gtk_container_add(w1,w2);
  gtk_button_set_focus_on_click (w1, FALSE);
  g_signal_connect (w1,"clicked",G_CALLBACK (paste_find_e_cb),e1);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 2);

  w1 = gtk_button_new();
  w2=gtk_image_new_from_stock (GTK_STOCK_CLEAR,GTK_ICON_SIZE_MENU);
  gtk_container_add(w1,w2);
  gtk_button_set_focus_on_click (w1, FALSE);
  g_signal_connect (w1,"clicked",G_CALLBACK (clear_find_e_cb),e1);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 2);
  
  w1 = gtk_label_new (_("Find"));
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 2);
  gtk_box_pack_start (hb, e1, FALSE, FALSE, 2);
  /*
     w1=gtk_button_new_from_stock(GTK_STOCK_FIND);
     gtk_button_set_focus_on_click(w1,FALSE);
     gtk_box_pack_start(hb,w1,FALSE,FALSE,0);
   */
  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  gtk_tooltips_set_tip(tips,w1,_("Fuzzy Search"),NULL);
  w2 = gtk_image_new_from_stock (GTK_STOCK_FIND, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
  g_signal_connect (w1,"clicked",G_CALLBACK (find_cb),NULL);

  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  w2 = gtk_image_new_from_stock (GTK_STOCK_INDEX, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
  gtk_tooltips_set_tip(tips,w1,_("Search in the current page"),NULL);
  g_signal_connect (w1,"clicked",G_CALLBACK (in_page_find_cb),NULL);
/*
  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  w2 = gtk_image_new_from_stock (GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
*/
  w1 = gtk_button_new ();
  gtk_button_set_focus_on_click (w1, FALSE);
  fn = thwab_filename (th, "images/keys.png");
  w2 = gtk_image_new_from_file (fn);
  free (fn);
  gtk_container_add (GTK_CONTAINER (w1), w2);
  gtk_box_pack_start (hb, w1, FALSE, FALSE, 0);
  gtk_tooltips_set_tip(tips,w1,_("Search by key"), NULL);
  g_signal_connect (w1,"clicked",G_CALLBACK (lookupkey_cb),NULL);
  
  w3 = gtk_font_button_new_with_font (default_font);
  gtk_box_pack_start (hb, w3, FALSE, FALSE, 0);
  g_signal_connect (G_OBJECT (w3), "font-set", font_cb,NULL);
  
  toc_tree=w2=gen_toc_tree();
    
  //expander = gtk_expander_new (".::");
  xpander = gtk_toggle_button_new_with_label(".::");
  gtk_button_set_focus_on_click   (xpander,FALSE);
  toc_expander=gtk_button_new();
  if (open_book_pix) gtk_container_add(toc_expander,gtk_image_new_from_pixbuf (open_book_pix));
  gtk_button_set_focus_on_click   (toc_expander,FALSE);
  toc_collapser=gtk_button_new();
  if (close_book_pix) gtk_container_add(toc_collapser,gtk_image_new_from_pixbuf (close_book_pix));
  gtk_button_set_focus_on_click   (toc_collapser,FALSE);
  hp = gtk_hpaned_new();
  gtk_box_pack_start (vb, hp, TRUE, TRUE, 0);
  
  v1 = gtk_vbox_new(FALSE,0);
  gtk_paned_pack1 (hp,v1,TRUE,TRUE);
  hb = gtk_hbox_new(FALSE,0);
  //h1 = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start (v1,hb, FALSE, FALSE, 0);
  gtk_box_pack_start (hb,xpander, FALSE, FALSE, 0);
  gtk_box_pack_start (hb,toc_expander, FALSE, FALSE, 0);
  gtk_box_pack_start (hb,toc_collapser, FALSE, FALSE, 0);
  g_signal_connect_swapped (toc_expander,"clicked",G_CALLBACK (gtk_tree_view_expand_all),toc_tree);
  g_signal_connect_swapped (toc_collapser,"clicked",G_CALLBACK (gtk_tree_view_collapse_all),toc_tree);
  //gtk_box_pack_start (hb,h1,TRUE, TRUE, 0);
  
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_paned_pack2 (hp,w1,TRUE,TRUE);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  txt = gtk_text_view_new ();
  buff=gtk_text_view_get_buffer(txt);
  gtk_text_view_set_wrap_mode(txt,GTK_WRAP_WORD_CHAR);
  gtk_text_view_set_border_window_size (txt, GTK_TEXT_WINDOW_TOP, 1);
  gtk_text_view_set_editable(txt, FALSE);
  gtk_container_add (GTK_CONTAINER (w1), txt);
  gtk_text_buffer_create_tag (buff, "SearchHighlight", "background", "#0000C0",
  "background-full-height",TRUE, "foreground", "#FFFF70",NULL);
  
  font_desc = pango_font_description_from_string (default_font);
  gtk_widget_modify_font (txt, font_desc);
  pango_font_description_free (font_desc);
  

  //gtk_expander_set_expanded (expander, TRUE);
  w1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (w1, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  w2=toc_tree;
  gtk_container_add (GTK_CONTAINER (w1), w2);
  //gtk_container_add (GTK_CONTAINER (expander), w1);
  gtk_box_pack_start (gtk_widget_get_parent (gtk_widget_get_parent (xpander)), w1, TRUE, TRUE, 0);
  gtk_toggle_button_set_active    (xpander,TRUE);
  g_signal_connect (xpander,"toggled",G_CALLBACK (xpander_cb),w1);
  gtk_paned_set_position(hp,150);
  /*
  gen_sample_toc(w2);
  gtk_tree_view_expand_all(w2);
  */
  /* */
  g_signal_connect (G_OBJECT (toc_tree), "cursor-changed", open_text_cb,NULL);
  ix_dlg=index_dlg_new();
  //open_book(0);
  gtk_widget_show_all (win);
  gtk_main ();
  return 0;
}
// not needed any more, used for testing
/*
void
gen_sample_toc(GtkWidget *toc_tree)
{
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(toc_tree);  
  GtkTreeIter iter1;		// Parent iter 
  GtkTreeIter iter2;		// Child iter 
  GtkTreeIter iter3;		// sub Child iter 
  
  gtk_tree_store_clear (store);
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "A. Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "B. Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "C. Chapter 3", -1);
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 3", -1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TOC_TREE_STR, "Section 3.1", -1);
  
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator 
  gtk_tree_store_set (store, &iter1, TOC_TREE_STR, "Book 3", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 1", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 2", -1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator
  gtk_tree_store_set (store, &iter2, TOC_TREE_STR, "Chapter 3", -1);
}

void
gen_sample_index(GtkWidget *tree)
{
  GtkTreeIter iter1;		// Parent iter
  GtkTreeIter iter2;		// Child iter
  GtkTreeIter iter3;		// sub Child iter
  GtkTreeStore *store=(GtkTreeStore *)gtk_tree_view_get_model(tree);
  char str[]="إسلاميات:قرآنيات:تفاسير";
  gtk_tree_store_clear (store);
  index_add(store, "تجربة"
  ,"كلام للتجربة", NULL, "فلان",220,0,GTK_STOCK_NETWORK);
  
  gtk_tree_store_append (store, &iter1, NULL);	// Acquire a top-level iterator 
  gtk_tree_store_set (store, &iter1, TH_IX_TITLE,"لغويات",-1);
  gtk_tree_store_append (store, &iter2, &iter1);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter2, TH_IX_TITLE,"معاجم",-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"لسان العرب",TH_IX_SUBTITLE,"",TH_IX_AUTHORS,"ابن منظور",TH_IX_YEAR,80, TH_IX_GPG,1,TH_IX_SOURCE, GTK_STOCK_HARDDISK,-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"المعجم الوسيط",TH_IX_SUBTITLE,"نسخة مشكولة",TH_IX_AUTHORS,"مجمع اللغة",TH_IX_YEAR,1980, TH_IX_SOURCE, GTK_STOCK_CDROM,-1);
  gtk_tree_store_append (store, &iter3, &iter2);	// Acquire a child iterator 
  gtk_tree_store_set (store, &iter3, TH_IX_TITLE,"القاموس السخيف",TH_IX_SUBTITLE,"مجرد عنوان",TH_IX_AUTHORS,"مجهول",TH_IX_YEAR,2006, TH_IX_SOURCE, GTK_STOCK_NETWORK,-1);
  // gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(store),TH_IX_YEAR, GTK_SORT_DESCENDING // or GTK_SORT_ASCENDING );

  index_add(store, "نحو","كلام", NULL, "فلان",220,0,GTK_STOCK_NETWORK);
  index_add(store, str,"تفسير ابن كثير", NULL, "ابن كثير",220,0,GTK_STOCK_NETWORK);
}
*/
