/**
 * LibITAR - routines to access compressed indexed TAR archives
 * 
 * Random access compressed TAR files
 * 	by Muayyad Saleh Alsadi<alsadi@gmail.com>
 *	Released under terms of GNU GPL
 *
 * The archive is compatible with GZ/BZiped TAR format
 * and can be extracted (or listed...etc) with unaware usual tools
 * but it generate an index file which makes it very fast to extract
 * a random file at a random position
 **/

#ifndef _LIBITAR_BZ_
#define _LIBITAR_BZ_
#include <bzlib.h>

typedef struct
{
	BZFILE	*b;
	bz_stream B; /* used for low-level writing */
	int	is_closed; /* TODO: move to itar_private */
	int	bzerror, nUnused;
	void	*unused;
} private_bz;
#define BZ(p)	((private_bz*)(ITAR(p)->c))
#define bz_best_size(s) ((s)+((s)+99)/100+600)
#define bz_min_size bz_best_size(BLOCKSIZE)
int		itar_bz_init(itar *p);
int		itar_bz_close(itar *p);

int		itar_bz_read_init(itar *p);
size_t		itar_bz_read(itar *p, void *buffer, size_t size);
int		itar_bz_read_close(itar *p);
int		itar_bz_write_init(itar *p);
int		itar_bz_write(itar *p, const void *buffer, size_t size);
int		itar_bz_write_close(itar *p); /* used internally by itar_bz_close(..) and write_init */
#endif
