/**
 * LibITAR - routines to access compressed indexed TAR archives
 * 
 * Random access compressed TAR files
 * 	by Muayyad Saleh Alsadi<alsadi@gmail.com>
 *	Released under terms of GNU GPL
 *
 * The archive is compatible with GZ/BZiped TAR format
 * and can be extracted (or listed...etc) with unaware usual tools
 * but it generate an index file which makes it very fast to extract
 * a random file at a random position
 **/

#include "libitar.h"
#include "libitar_private.h"
#include "libitar_bz.h"
/***************************************
 * private BZ functions implementation *
 ***************************************/
int itar_bz_init(itar *p)
{
  ITAR(p)->c=malloc(sizeof(private_bz));
  if (!p) return -1; /* TODO: better error handling */
  BZ(p)->b = NULL;
  if (ITAR(p)->mode) { itar_bz_write_init(p); return itar_w_open(p,bz_min_size);}
  return itar_bz_read_init(p);
}
int itar_bz_close(itar *p)
{
  if (BZ(p))
  {
    if (ITAR(p)->mode) {itar_bz_write_close(p); itar_w_close(p);}
    else  itar_bz_read_close(p);
    free(BZ(p));
   (ITAR(p))->c=NULL;
  }
  return 0;
}
/*********************
 * reading functions *
 *********************/
int itar_bz_read_init(itar *p)
{
  if (BZ(p)->b) itar_bz_close(p);
  BZ(p)->b = NULL;  
  BZ(p)->bzerror = BZ_OK;
  BZ(p)->nUnused = 0;
  BZ(p)->unused = NULL;
  return 0;
}
int itar_bz_read_close(itar *p)
{
  if (BZ(p)->b)
  {
    BZ2_bzReadClose ( &(BZ(p)->bzerror), BZ(p)->b );
    if (BZ(p)->bzerror != BZ_OK) return ITAR_ERR_CLOSE;
    BZ(p)->b = NULL;
    BZ(p)->is_closed = 1;
    return 0;
  }
  BZ(p)->b = NULL;
  return 0;
}


size_t itar_bz_read(itar *p, void *buffer, size_t size)
{
  size_t n=0;
  BZ(p)->bzerror = BZ_OK;
  if (!(BZ(p)->b))
  {
    BZ(p)->b = BZ2_bzReadOpen ( &(BZ(p)->bzerror), p->f, 0, 0, BZ(p)->unused, BZ(p)->nUnused);
    if (BZ(p)->bzerror != BZ_OK)
    {
      BZ2_bzReadClose ( &(BZ(p)->bzerror), BZ(p)->b );
      return ITAR_ERR_OPEN;
    }
    BZ(p)->unused=NULL;
    BZ(p)->nUnused=0;
  }
  n=BZ2_bzRead ( &(BZ(p)->bzerror), BZ(p)->b, buffer, size);
  if (BZ(p)->bzerror == BZ_STREAM_END)
  {
    BZ2_bzReadGetUnused (&(BZ(p)->bzerror), BZ(p)->b, &(BZ(p)->unused), &(BZ(p)->nUnused) );
    BZ2_bzReadClose ( &(BZ(p)->bzerror), BZ(p)->b );
    BZ(p)->b = NULL;
    BZ(p)->bzerror = BZ_STREAM_END;
  }
  return n;
}
/**********************
 * writing functions  *
 **********************/

int itar_bz_write_init(itar *p)
{
  /* if (BZ(p)->B.total_out_hi32!=0 || BZ(p)->B.total_out_lo32!=0) BZ2_bzCompressEnd(BZ(p)->B); */
  itar_bz_write_close(p);
  BZ2_bzCompressInit (&BZ(p)->B,9,0,0); /* imply total_out_{hi32,lo32}=0 */
  return 0;
}

int itar_bz_write(itar *p, const void *buffer, size_t size)
{
  int r,rr;
  BZ(p)->B.next_in=(char *)buffer;
  BZ(p)->B.avail_in=size;
  ITAR(p)->d_size+=size;
  do
  {
    if ((r=itar_w_init(p,bz_best_size(BZ(p)->B.avail_in)))) return r;
    BZ(p)->B.next_out=ITAR(p)->W.ptr;
    BZ(p)->B.avail_out=ITAR(p)->W.size;
    r=BZ2_bzCompress (&BZ(p)->B, BZ_RUN);
    rr=itar_w_save(p, ITAR(p)->W.size - BZ(p)->B.avail_out);
  } while (r==BZ_RUN_OK && BZ(p)->B.avail_in!=0);
  if (r!=BZ_RUN_OK || BZ(p)->B.avail_in!=0) return ITAR_ERR_BAD;
  //printf("compressed=%d (total in=%d out=%d pos=%d) s=%d,r=%d,in=%d %c\n",ITAR(p)->W.size-BZ(p)->B.avail_out, BZ(p)->B.total_in_lo32, BZ(p)->B.total_out_lo32,ITAR(p)->W.done, ITAR(p)->W.size, r,BZ(p)->B.avail_in,(ITAR(p)->W.ptr==ITAR(p)->W.once)?'o':'n');
  return rr;
}

int itar_bz_write_close(itar *p)
{
  int r=BZ_FINISH_OK;
  if (BZ(p)->B.total_in_hi32!=0 || BZ(p)->B.total_in_lo32!=0)
    {
      BZ(p)->B.next_in=NULL;
      BZ(p)->B.avail_in=0;
      do {
      itar_w_init(p, bz_min_size);
      BZ(p)->B.next_out=ITAR(p)->W.ptr;
      BZ(p)->B.avail_out=ITAR(p)->W.size;
      r=BZ2_bzCompress (&BZ(p)->B, BZ_FINISH);
      if (itar_w_save(p, ITAR(p)->W.size - BZ(p)->B.avail_out)) return ITAR_ERR_BAD;
      //printf("compressed=%d (total in=%d out=%d pos=%d)\n",ITAR(p)->W.size-BZ(p)->B.avail_out, BZ(p)->B.total_in_lo32, BZ(p)->B.total_out_lo32,ITAR(p)->W.done);
      } while (r==BZ_FINISH_OK);
      if ((r!=BZ_STREAM_END && r!=BZ_STREAM_END ) || BZ(p)->B.avail_in!=0) return ITAR_ERR_BAD;      
      //printf("saved\n");
      BZ2_bzCompressEnd(&BZ(p)->B);
      //printf("saved_\n");
    }
  return 0;
}
