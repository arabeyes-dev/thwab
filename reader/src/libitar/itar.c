#include "libitar.h"
void error(char *);
void help(int i)
{
   printf(
   "itar - indexed TAR creator/extractor by Muayyad Alsadi <alsadi@gmail.com>\n"
   "Usage:\titar -t [-i INDEXFILE] TARFILE\n"
   "\tlist member files of a .tar.bz2 file\n"
   "Usage:\titar -c [-i INDEXFILE] TARFILE FILES...\n"
   "\tto create TARFILE (a .tar.bz2 file) adding FILES to it.\n"
   "\tif INDEXFILE is not specified then a .tar.bz2.i file is generated\n"
   "Usage:\titar [-i INDEXFILE] [-k KEY | -o OFFSET] TARFILE FILES...\n"
   "\tto extract FILES from TARFILE (a .tar.bz2 file)\n"
   "\tindex file could be specified with -i option\n"
   "\tif INDEXFILE is not specified then a .tar.bz2.i file is expected\n"
   "\tyou can seek by a KEY or by an OFFSET,\n"
   "\telsewhere each file itself is seeked.\n"
   "manual seeking (with -k or -o) is just for debugging\n"
   );
   exit(i);
}
int main(int argc, char **argv)
{
  itar *t;
  int i,j;
  char *buff;
  char *fn=NULL, *ndx_fn = NULL, *key=NULL, *fm=NULL,*s;
  int is_seeked=0;
  int c;
  int no_ndx=0, of=-1,is_create=0,is_list=0;
  opterr = 0;
/*
static struct option long_options[] =
        {
          // These options set a flag.
          {"verbose", no_argument,       &verbose_flag, 1},
          {"brief",   no_argument,       &verbose_flag, 0},
          // These options don't set a flag We distinguish them by their indices.
          {"add",     required_argument, 0, 'a'},
          {"append",  no_argument,       0, 'b'},
          {"delete",  required_argument, 0, 'd'},
          {"create",  no_argument,       0, 'c'},
          {"file",    required_argument, 0, 'f'},
          {0, 0, 0, 0}
        };
int option_index = 0;
while ((c = getopt_long (argc, argv, "abc:d:f:",
                       long_options, &option_index))!=-1)
    {
     switch (c)
        {
        case 0:
          // If this option set a flag, do nothing else now.
          if (long_options[option_index].flag != 0)
            break;
          printf ("option %s", long_options[option_index].name);
          if (optarg)
            printf (" with arg %s", optarg);
          printf ("\n");
          break;

        case 'a':
          puts ("option -a\n");
          break;

        case 'b':
          puts ("option -b\n");
          break;

        case 'c':
          printf ("option -c with value `%s'\n", optarg);
          break;

        case 'd':
          printf ("option -d with value `%s'\n", optarg);
          break;

        case 'f':
          printf ("option -f with value `%s'\n", optarg);
          break;

        case '?':
          // getopt_long already printed an error message.
          break;

        default:
          abort ();
        }
    }

  // Instead of reporting `--verbose'  and `--brief' as they are encountered,
  // we report the final status resulting from them.
  if (verbose_flag)
    puts ("verbose flag is set");

*/
  while ((c = getopt (argc, argv, "htci:k:o:")) != -1)
    {
    //printf("<%c:%s>",c,optarg);
    switch (c)
      {
      /* case 'n': no_ndx = 1;		break; */
      case 'k': key = optarg;		break;
      case 'o': of = atol(optarg);	break;
      case 'i': ndx_fn = optarg;	break;
      case 'c': is_create = 1;		break;
      case 't': is_list = 1;		break;
      case '?':
        if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
      case 'h':
	help(1);
      default:
        abort ();
      }}
  i = optind;
  //printf("%d\n",of);
  if ((!is_list || argc-i!=1) && argc-i<2) help(0) ;
  fn=argv[i++];
  j=0;
  if (is_create && is_list) {error("You may not specify create and list at the same time");}
  if (is_create)
  {
    if (key || of!=-1 || no_ndx ) {error("You may not specify extraction options when creating");}
    t=itar_open_full(fn,"wb+",ndx_fn);
    if (!t) error("could not open file");
    for (; i < argc; ++i)
    {
      printf("adding [%s]\n",argv[i]);
      j=itar_push_fs_tree(t, argv[i],0);
      if (j<0) {printf("%d\n",j); error("could not add file");}
    }
  } else if (is_list)
  {
    if (key || of!=-1 || no_ndx ) {error("You may not specify extraction options when listing");}
    t=itar_open_full(fn,"rb",ndx_fn);
    if (!t) error("could not open file");
    j=itar_get_n_files(t);
    for (i=0;i<j;++i)
    {
      if ((s=itar_get_filename_by_index(t, i))!=NULL)
        printf("%d\t%s\n",i,s);
    }
    printf("iTAR: %d file(s)\n",j);
  } else
  {
    if (key && of!=-1) {error("You may not specify both offset and key");} 
    t=itar_open_full(fn,"rb",ndx_fn);
    if (!t) error("could not open file");
    if (key && itar_seek_by_key(t, key)<0) error("could not seek");
    if (of!=-1 && itar_seek_by_offset(t, of)<0) error("could not seek");
    if (key || of!=-1) is_seeked=1;
    for (; i < argc; ++i)
    {
        if (!is_seeked && itar_seek_by_file(t, argv[i])<0) error("could not seek");
	/* is_seeked=1; */
	j=itar_pop_contents_by_file(t, argv[i], &buff); 
	if (j==ITAR_ERR_NFOUND) error("file not found in the the archive");
	if (j<0) error("could not read file");
	fwrite(buff,j,1,stdout);    
    }
  }
  itar_close(t);
  return j;
}
