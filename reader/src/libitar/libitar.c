/**
 * LibITAR - routines to access compressed indexed TAR archives
 * 
 * Random access compressed TAR files
 * 	by Muayyad Saleh Alsadi<alsadi@gmail.com>
 *	Released under terms of GNU GPL
 *
 * The archive is compatible with GZ/BZiped TAR format
 * and can be extracted (or listed...etc) with unaware usual tools
 * but it generate an index file which makes it very fast to extract
 * a random file at a random position
 **/

/**
 * It's possible to generate the index if missing but this takes as long time
 * as extracting the whole file (eg. by detecting BZ_STREAM_END)
 **/

/**
 * extracting TAR file idea was taken from untgz.c (zlib/contrib/untgz/)
 * which is not meant to be as powerfull as GNU TAR
 * untgz.c was written by "Pedro A. Aranda Guti\irrez" <paag@tid.es>
 * adaptation to Unix by Jean-loup Gailly <jloup@gzip.org>
 * various fixes by Cosmin Truta <cosmint@cs.ubbcluj.ro>
 * 
 * some other ideas taken from GNU TAR info page.
 **/
/* TODO: separate utility functions in another file */
/* TODO: a function to convert itar errno to error string */
/* TODO: load index keys in a B-Tree for faster searching (not important as there are few keys) */
/* FIXME: file names like ./mydir///dir2/myfile should be normalized to mydir/dir2/myfile */
/* FIXME: read/write functions should check mode */
/* FIXME: strncpy cased problems [as it fill nulls] doublecheck all calls and use strncat(..,n-1) */

#define _GNU_SOURCE	/* 1 st to get rid of non-GNU, 2nd to have strndup and alloca */
#include "libitar.h"
#include "libitar_private.h"

#ifndef  _POSIX_MAPPED_FILES
#define NO_MMAP_W
#define NO_MMAP_R
#define NO_MREMAP
#endif

#ifndef  NO_ALLOCA
#define xxalloc(s) malloc(s)
#define xxfree(p) free(p)
#else
#define xxalloc(s) alloca(s)
#define xxfree(p) 0
#endif

#define min(x,y) (((x)<(y))?(x):(y))
#define max(a,b) (((a)>(b))?(a):(b))
/* TODO: better error handling */
void
error (const char *msg)
{
  fprintf (stderr, "%s\n", msg);
  exit (1);
}
/* return pointer to 1st non-blank char */
char *
str_skipblank(const char *str,int l)
{
  int ch;
  while((l==-1 || l-->0) && (ch=*str)!=0 && isblank(ch)) ++str;
  return (char *)str;
}
/* replace 'c' with 'to' and return number of maches */
int
strnchr2chr(char *str, int c, int to, int l)
{
  int r=0;
  int ch=*str;
  while((l==-1 || l-->0) && ch)
  {
    if (ch==c) {++r; *str=to;}
    ch=*(++str);
  }
  return r;
}
/* convert string octal digits to int , on error return -1 */
int
getoct (char *p, int width)
{
  int r = 0;
  char c;

  while (width--)
    {
      c = *p++;
      if (c == 0)
	break;
      if (c == ' ')
	continue;
      if (c < '0' || c > '7')
	return -1;
      r<<=3; r+=c - '0';
    }
  return r;
}
/* convert int to string of octal digits, on error return -1 */
int
setoct (char *p, int width, int v)
{
  char *ptr=p+width-1;
  *ptr--=0;
  while(v && --width) { *ptr--='0'+(v&7); v>>=3;}
  if (v) return -1;
  while(ptr>=p) { *ptr--='0';}
  return 0;
}

itar *
itar_open(const char *file, const char *mode)
{
  itar *t=(itar *)malloc(sizeof(itar));
  if (!t) return NULL;
  memset(&(t->header),0,sizeof(itar_header));
  /* t->header.name[0]='\0'; */ /* TODO: not needed as the entire header is set to 0 */
  //printf("opening\n");
  t->opaque=malloc(sizeof(itar_opaque));
  if (!ITAR(t)) { free(t); return NULL; }
  if ( !(t->f=fopen(file,mode)) ) {free(ITAR(t)); free(t); return NULL;}
  ITAR(t)->ls=NULL;
  ITAR(t)->ls_n=0;
  ITAR(t)->counter=0;
  ITAR(t)->i_size=1<<15; /* 32KB */
  ITAR(t)->c_size=0;
  ITAR(t)->d_size=0;
  ITAR(t)->mode=(strchr(mode, 'w')!=NULL);
  ITAR(t)->type=ITAR_BZ; /* FIXME: because GZ is not implemented yet */
  if (itar_c_init(t)<0) {fclose(t->f); free(ITAR(t)); free(t);  return NULL;}
  //ITAR(t)->offset=0;
  /*ITAR(t)->last_file[0]=0;*/ /* TODO: no more needed use header->name instead */

  ITAR(t)->r=0;
  
  if (ITAR(t)->mode) {
    ITAR(t)->index=stderr;
    ITAR(t)->files_ls=tmpfile();
    if (!ITAR(t)->files_ls) {
      fclose(t->f); free(ITAR(t)); free(t);  return NULL;
    }
    fprintf(ITAR(t)->files_ls,"\n");
  } else ITAR(t)->index=0;
  t->itb.n=0;
  t->itb.offsets=NULL;
  t->itb.keys=NULL;
  ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  return t;
}
int
itar_close(itar *t)
{
  int i,j,fd;
  char *buff;
  if (ITAR(t)->mode)
  {
    j=((10240-(ITAR(t)->d_size % 10240))% 10240)/512;
    for (i=0;i<j;++i) itar_push_eof_blocks(t);
    fflush(ITAR(t)->index); fflush(ITAR(t)->files_ls);
    fseek(ITAR(t)->files_ls,0,SEEK_END);
    j=ftell(ITAR(t)->files_ls);
    rewind(ITAR(t)->files_ls);
    /* copy from ls to index*/
    #ifndef NO_MMAP_R
    fd=fileno(ITAR(t)->files_ls);
    buff=mmap (0, j,PROT_READ,MAP_SHARED , fd, 0);
    if (buff!=MAP_FAILED)
    {
      if (fwrite(buff,j,1,ITAR(t)->index)<1) {printf("Error\n"); exit(-1);} // FIXME: handle error
      fflush(ITAR(t)->index);
      munmap(buff, t->header.size);
    } else {
    #endif
    // non MMAP version
    buff=malloc(j);
    if (!buff) exit(-1); // FIXME: handle error
    if (fread(buff,j,1,ITAR(t)->files_ls)<1) exit(-1); // FIXME: handle error
    if (fwrite(buff,j,1,ITAR(t)->index)<1) exit(-1); // FIXME: handle error
    fflush(ITAR(t)->index);
    free(buff);
    #ifndef NO_MMAP_R
    }
    #endif
    if (ITAR(t)->index!=stderr) fclose(ITAR(t)->index);
    if (ITAR(t)->files_ls) fclose(ITAR(t)->files_ls);
  } else if (t->itb.n)
  {
    if (ITAR(t)->ls) free(ITAR(t)->ls);
    if (t->itb.offsets) free(t->itb.offsets);
    if (t->itb.keys) free(t->itb.keys);
    t->itb.n=0;
    t->itb.offsets=NULL;
    t->itb.keys=NULL;
  }
  itar_c_close(t);
  fclose(t->f);
  free(ITAR(t));
  free(t);
  return 0;
}
int
itar_load_indices(itar *t, FILE *F)
{
  size_t l,L,ll;
  int i,j,n,m=0;
  char *a,*ptr,*s1,*s2,*tmp,*next_ln,*ls;
  int of,c;
  fseek(F, 0, SEEK_END); l=ftell(F); fseek(F, 0, SEEK_SET);
  if (l==0) return 0;
  if (!(a=(char *)malloc(l+1))) { fclose(F); return ITAR_ERR_MEM;}
  L=fread(a,1,l,F);
  if (L==0) return ITAR_ERR_READ;
  a[L]=0; /* becomes a nul terminated string */
  ptr=a;
  j=strnchr2chr(ptr, '\n', '\0', L);
  if (a[L-1]!='\0') ++j; /* if no new line at end of the file */
  next_ln=ptr;
  for (i=0;i<j;++i) {
    s1=next_ln;
    if (*s1=='\0') break;
    next_ln=s1+strlen(s1)+1;
  }
  n=i; ls=next_ln+1;
  t->itb.offsets=(size_t *)malloc(sizeof(size_t)*n);
  if (!t->itb.offsets) { fclose(F); free(a); return ITAR_ERR_MEM;}
  t->itb.indices=(size_t *)malloc(sizeof(int)*n);
  if (!t->itb.indices) { fclose(F); free(a); free(t->itb.offsets); return ITAR_ERR_MEM;}
  t->itb.keys=(char **)malloc(sizeof(char **)*n);
  if (!t->itb.keys) { fclose(F);  free(a);  free(t->itb.offsets); free(t->itb.indices); return ITAR_ERR_MEM;}
  next_ln=ptr; m=0;
  for (i=0;i<n;++i)
  {
    ll=strlen(ptr); // line length
    next_ln+=ll+1;
    // do something here
    s1=strchr(ptr,'\t');
    if (!s1 || ll==0) {ptr=next_ln; continue;}
    *s1='\0';
    of=strtol(ptr, &tmp, 0);
    if (of<0 || of==LONG_MAX) {ptr=next_ln; continue;}
    t->itb.offsets[m]=of;
    ptr=s1+1;
    s1=strchr(ptr,'\t');
    if (!s1) {ptr=next_ln; continue;}
    *s1='\0';
    c=strtol(ptr, &tmp, 0);
    if (c<0 || c==LONG_MAX) {ptr=next_ln; continue;}
    t->itb.indices[m]=c;
    if (*(s1+1)=='\0') {ptr=next_ln; continue;}
    t->itb.keys[m]=s1+1; // with strdup if to orig buffer is to be freed
    ++m;
    ptr=next_ln;
  }
  t->itb.n=m;
  n=j-n-1;
  ITAR(t)->ls=(char **)malloc(sizeof(char *)*n);
  s1=ls;
  for (i=0,j=0;i<n;++i)
  {
    next_ln=s1+strlen(s1)+1;
    if (*s1=='\0') {s1=next_ln; continue;} /* skip empty lines */
    ITAR(t)->ls[j]=s1;
    s1=next_ln;
    ++j;
  }
  ITAR(t)->ls_n=j;
  /* free 'a' here if strndup is used */
  return 0;
}

int
itar_set_index_file(itar *t, const char *index_file)
{
  FILE *F;
  int r;
  if (ITAR(t)->mode)
  {
    ITAR(t)->index=stderr;
    if (! (F=fopen(index_file,"w"))) return ITAR_ERR_OPEN;
    ITAR(t)->index=F;
    return 0;
  } else
  {
    if (!(F=fopen(index_file,"r"))) return ITAR_ERR_OPEN;
    r=itar_load_indices(t, F);
    fclose(F);
    return r;
  }
  return 0;
}
/*int
itar_set_list_file(itar *t, const char *list_file)
{
  FILE *F;
  int r;
  ITAR(t)->files_ls=NULL;
  if (ITAR(t)->mode)
  {
    if (! (F=fopen(list_file,"w"))) return ITAR_ERR_OPEN;
    ITAR(t)->files_ls=F;
    return 0;
  }
  return -1; // not used in read mode
}*/
itar *
itar_open_full(const char *file,const char *mode, const char *index) /* index or ls could be set to NULL */
{
  itar *t;
  int l=strlen(file),L=l+3;
  char *fn, *ls_fn;
  if (!(t=itar_open(file,mode))) { return NULL;}
  /* TODO: error handling: close then return NULL but how to know it's bad index not bad itar */
  if (! index || itar_set_index_file(t, index)<0) /* index argument failed try .i suffix */
  {
    fn=xxalloc(L);
    if (!fn) {itar_close(t); return NULL;}
    strncpy(fn,file,l);
    strncpy(fn+l,".i",3); /* faster than strncat since I know strlen */
    if (itar_set_index_file(t, fn)<0 && ! ITAR(t)->mode)
    {
      itar_close(t);
      xxfree(fn);
      fprintf(stderr,"libitar: bad index\n");
      t=NULL;
    }
    /* if write mode and all failed use stderr for index */
    xxfree(fn);
  }
  ///* list is only for write mode */
  //if (ITAR(t)->mode && (! ls ||  itar_set_list_file(t, ls)<0)) /* ls argument failed try .ls suffix */
  //{
//    ls_fn=xxalloc(L+1);
//    if (!ls_fn) {itar_close(t); return NULL;}
//    strncpy(ls_fn,file,l);
//    strncpy(ls_fn+l,".ls",4); /* faster than strncat since I know strlen */
//    if (itar_set_list_file(t, ls_fn)<0) { fprintf(stderr,"libitar: bad file list\n"); }
//    xxfree(ls_fn);
//  }
  return t;
}
/* information functions */
int
itar_get_n_files(itar *t)
{
  return (ITAR(t)->mode)?-1:ITAR(t)->ls_n;
}
const char *
itar_get_filename_by_index(itar *t, int i)
{
  return (ITAR(t)->mode==0 && i<ITAR(t)->ls_n)?ITAR(t)->ls[i]:NULL;
}
/* TODO: replace strcmp with a version that skip last slash ..etc */
int
itar_get_index_by_filename(itar *t, char *filename)
{
  char **r;
  if (ITAR(t)->mode) return -1;
  r = bsearch(&filename, ITAR(t)->ls, ITAR(t)->ls_n, sizeof(char *), strcmp);
  if (!r) return -1;
  return r-(ITAR(t)->ls);
}

/* internal functions
int itar_get_offset_by_key(itar *t, const char *key);
int itar_get_offset_by_file(itar *t, const char *file);
*/
int
itar_get_offset_by_key(itar *t, const char *key)
{
  int i,j;
  for (i=0,j=t->itb.n;i<j;++i)
  {
  	if (!strncmp(key,t->itb.keys[i],ITARNMAX)) { return t->itb.offsets[i];}
  }
  return -1;
}
/*
int
itar_get_offset_by_file(itar *t, const char *file)
{
  int i,j,c,r=-1;
  for (i=0,j=t->itb.n;i<j;++i)
  {
    // eg. key:"d1/d2/" <= file:"d1/d2/f" 
    if ((c=strncmp(t->itb.keys[i], file,100))>0) return r;
    else if (c==0) return t->itb.offsets[i];
    r=t->itb.offsets[i];
  }
  if (c<0) return r;
  return -1;
}
*/
/* compare file and keys, eg. file:"d1/d2/f" >= key:"d1/d2/" to get offset */
int
itar_get_offset_by_file_(itar *t, const char *file, char **nxt)
{
  int i,j,c,rr,r=0;
  if (t->itb.n==0) return -1; /* FIXME: better error handling*/
  rr=t->itb.offsets[0];
  *nxt=t->itb.keys[0];
  for (i=0,j=t->itb.n;i<j;++i)
  {
    if ((c=strncmp(t->itb.keys[i], file,ITARNMAX))>0) return r; /* the old r */
    r=rr; rr=t->itb.offsets[i+1];
    *nxt=(i+1<j)?t->itb.keys[i+1]:NULL;
    if (c==0) return r; /* the new r */
  }
  if (c<0) return r; /* the last r */
  return -1;
}

int
itar_get_offset_by_file(itar *t, const char *file)
{
  char *nxt;
  return itar_get_offset_by_file_(t, file, &nxt);
}
int
itar_seek_by_chunk(itar *t, int c)
{
  if (c<0 || c > t->itb.n) return -1;
  itar_c_read_close(t);
  fseek(t->f, t->itb.offsets[c], SEEK_SET);
  itar_c_read_init(t);
  //printf("real seek done [0x%X]\n",t->itb.offsets[c]);
  ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  ITAR(t)->counter=t->itb.indices[c];
  return 0;
}
static int
cmp_int(const void *m1, const void *m2) {
	size_t a = *((size_t *)m1);
	size_t b = *((size_t *)m2);
	return (a>b)?+1:(a<b)?-1:0;
}
int
itar_seek_by_offset(itar *t, size_t offset)
{
  size_t *res;
  itar_c_read_close(t);
  fseek(t->f, offset, SEEK_SET);
  itar_c_read_init(t);
  //printf("real seek done [0x%X]\n",offset);
  ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  //ITAR(t)->offset=offset;
  res = bsearch(&offset, t->itb.offsets, t->itb.n, sizeof(int), cmp_int);
  if (res) ITAR(t)->counter= t->itb.indices[res - (t->itb.offsets)];
  //else ITAR(t)->counter=-1; /* FIXME: consider this an error and seek to offset 0 */
  return 0; /* TODO: better error handling */
}

int
itar_seek_by_key(itar *t, const char *key)
{
  int r;
  if ((r=itar_get_offset_by_key(t,key))>=0) return itar_seek_by_offset(t, r);
  return -1;
}
/* for a faster file poping,
   this should check if file > last_file and file < next_file then just return
   last_file should be set by each seek/header_pop function
   maybe we need to make this fool proof by saving last_offset and if it's != ftell
   then we have to cary on a failsafe procedure 

old method
int
itar_seek_by_file(itar *t, const char *file)
{
  int r;
  if ((r=itar_get_offset_by_file(t,file))>=0) return itar_seek_by_offset(t, r);
  return -1;
}

*/
int
itar_seek_by_file(itar *t, const char *file)
{
  char *nxt;
  int r=itar_get_offset_by_file_(t,file,&nxt);
  // printf("seek for [%s]\n",file);
  itar_get_offset_by_file_(t, t->header.name, &nxt); // set nxt to the next of header
  /* check if no need to seek */
  /* printf("** %d [%s->%s->%s]\n",r,ITAR(t)->last_file,file,nxt); */
  //ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  if (t->header.name[0]!='\0' && strncmp(file,t->header.name,ITARNMAX)>0 && nxt && strncmp(nxt,file,ITARNMAX)>0) return 0;
  /* seek */
  if (r>=0) return itar_seek_by_offset(t, r);
  return -1;
}
int
itar_seek_by_index(itar *t, int index)
{
  int i,j,k,n=t->itb.n;
  if (index<0) return -1;
  if (n>0) k=t->itb.indices[0]; else k=0;
  j=k;
  for (i=0;i<n;++i)
  {
    if ((k=t->itb.indices[i]) > index) break;
    j=k;
  }
  // if (ITAR(t)->counter <= index && ( ITAR(t)->counter < k || j==k)); // no need to seek
  if (ITAR(t)->counter > index || ( ITAR(t)->counter >= k && j!=k)) return itar_seek_by_chunk(t, (i>0)?i-1:0);
  return 0;
}
int
itar_pop_header_(itar *t, tar_buffer *buff)
{
  /* TODO: better error handling (double check return code) */
  int i,chk=0,l=itar_c_read(t, buff, BLOCKSIZE);
  unsigned char *ptr=buff->buffer;
  if (l<BLOCKSIZE) return l;
  for (i=0;i<512;++i,++ptr) chk+=*ptr;
  for (i=0,ptr=buff->buffer+148;i<7;++i,++ptr) chk-=*ptr;
  chk+=32*7;
  if (chk!=getoct(buff->header.chksum,8)) return ITAR_ERR_CHKSUM;
  return l;
}
/* pop header, return 0 if success or ITAR_ERR_BAD if fail */
/* currently never return ITAR_ERR_CHKSUM, just print a warning */
int
itar_pop_header(itar *t)
{
  tar_buffer buff;
  int l;
  if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED) return 0;
  l=itar_pop_header_(t, &buff);
  ++(ITAR(t)->counter); /* FIXME: set index by name */
  if (! buff.header.name[0]) return ITAR_ERR_NFOUND; /* TODO: how to handle empty blocks (error or warning)*/
  else if (l==ITAR_ERR_CHKSUM) fprintf(stderr,"libitar: Warning! bad checksum.\n");
  else if (l<BLOCKSIZE) return ITAR_ERR_BAD; /* TODO: better error handling (may need close..etc)*/
  else l=0; /* return success */
  /* parse header */
  memset(&(t->header),0,sizeof(itar_header));
  t->header.mode = getoct(buff.header.mode,8);
  t->header.mtime = (time_t)getoct(buff.header.mtime,12);  
  if (t->header.mode == -1 || t->header.mtime == (time_t)-1) return ITAR_ERR_BAD;
  t->header.chksum = getoct(buff.header.chksum,8);
  t->header.uid=(uid_t)getoct(buff.header.uid,8);
  t->header.gid=(gid_t)getoct(buff.header.gid,8);
  t->header.size=(size_t)getoct(buff.header.size,12);
  t->header.devmajor=getoct(buff.header.devmajor,8);
  t->header.devminor=getoct(buff.header.devminor,8);
  t->header.typeflag=buff.header.typeflag;
  /* TODO: handle GNU extention for long file names */
  strncpy(t->header.name, buff.header.name, TARNMAX); /* t->header.name[ITARNMAX_]=0; not needed because memset */
  /* strncpy(ITAR(t)->last_file, buff.header.name, ITARNMAX); */ /* ITAR(t)->last_file[99]=0; */ /* TODO: not needed any more */
  /* TODO: handle GNU extention for long link names */
  strncpy(t->header.linkname, buff.header.linkname, TARNMAX); /* t->header.linkname[99]=0; */
  strncpy(t->header.uname, buff.header.uname, 32); t->header.uname[31]=0;
  strncpy(t->header.gname, buff.header.gname, 32); t->header.gname[31]=0;
  /* printf("[%d][%s]",t->header.typeflag,buff.header.magic); */
  // detect the end block header
  if (t->header.name[0]==0) ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  else switch(t->header.typeflag)
  {
    case DIRTYPE:
    case LNKTYPE:
      t->header.size=0;
      ITAR(t)->r = t->header.size;
      ITAR(t)->pad = 0;      
      ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
      break;
    case REGTYPE:
    case AREGTYPE:
      ITAR(t)->r = t->header.size;   /* TODO: do we need to move alike from header() to header_() */
      if (ITAR(t)->r) ITAR(t)->pad = (512- (ITAR(t)->r & 511 ))& 511; /* (BLOCKSIZE - (ITAR(t)->r % BLOCKSIZE) ) % BLOCKSIZE; */
      else ITAR(t)->pad = 0;
      ITAR(t)->state=ITAR_ST_BLOCK_EXPECTED;
      break;
    default:
      ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
      return ITAR_ERR_NIMP;
  }
  return 0;
}

int
itar_pop_contents_block(itar *t, char *buff, size_t s)
{
  int l;
  char a[BLOCKSIZE]; /* padding */
  /* TODO: better error handling (double check return code)*/
  l=itar_c_read(t, buff, min(s,ITAR(t)->r)); /* min is used if one read 13 but file is 12*/
  /* TODO: need to check here for errors */
  ITAR(t)->r -= l;
  if (ITAR(t)->r < 0) ITAR(t)->r=0; /* in case r=12 and we read a 512 block */ /* FIXME: I think this is useless, imposible condition */
  /* skip block padding if this is last block */
  if (! (ITAR(t)->r))
  {
    ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
    if (ITAR(t)->pad)
    {
      /* printf("/padding:%d/",ITAR(t)->pad); */
      ITAR(t)->pad-=itar_c_read(t, a, ITAR(t)->pad);
    }
  }
  return l;
}

int
itar_pop_contents(itar *t, char **buff)
{
  if (ITAR(t)->r==0) {*buff=NULL; return 0;}
  *buff=malloc(ITAR(t)->r);
  if (! *buff ) return 0;
  /*
  if (ITAR(t)->r > BLOCKSIZE) // two stages: 1028 = 1024 (ie. 2*512) then 4
     itar_pop_contents_block(t, *buff, ITAR(t)->r - (ITAR(t)->r % BLOCKSIZE) );
  */
  return itar_pop_contents_block(t, *buff, ITAR(t)->r);
}
/* allocate size+2 buffer and load the content plus "\n\0" for easier parsing and assure being a valid null terminated C string */
int
itar_pop_contents0(itar *t, char **buff)
{
  int i;
  if (ITAR(t)->r==0) {*buff=NULL; return 0;}
  *buff=malloc(2+ITAR(t)->r);
  if (! *buff ) return 0; // TODO: better error handling
  i=itar_pop_contents_block(t, *buff, ITAR(t)->r);
  if (i>=0) {(*buff)[i]='\n'; (*buff)[i+1]='\0'; i+=1;}
  return i;
}

int
itar_skip_contents(itar *t)
{
  char buff[BLOCKSIZE];
  int r=0,l;
  /* TODO: I think this also work, since the padding is automatically skipped */
  while(ITAR(t)->r &&
    (l=itar_pop_contents_block(t, buff, min(ITAR(t)->r,BLOCKSIZE) ))) r+=l;
  /*
  while(ITAR(t)->r &&
    (l=min(ITAR(t)->r, itar_pop_contents_block(t, buff, BLOCKSIZE)) )) {r+=l; printf(".");}
  */
  return r;
}

/* put file/directory name in canonical form, return new length*/
int to_canonical_filename(char *f)
{
  /*
  remove repeated "/"
  remove inline "./" (eg. "foo/bar/./this/that" -> "foo/bar/this/that" )
  remove inline "../" and it's previous token (eg. "foo/bar/../this/that" -> "foo/this/that" )
  remove last "/" 
  
  "///a/b///c/../d///..//.//../foo.html" gives "a/foo.html"
  "test/../b/" gives "b"
  "../test/" gives "../test"
  */
  register char *o=f,*s=f,c,m=0; /* mode 0 default, 1 after first ., 2 after 2nd dot, 3 after first slash , 4 slash sequence to be removed, 5 tailing slash */
  char *p,l=1; /* l is_leading */
  /* TODO: make it simpler, there are common things in all modes */
  while(c=*(o++))
  {
    switch(m)
    {
    case 0: /* default, select next mode */
      if (c=='.') m=1;
      else if (c=='/') m=(l)?0:3;
      else {*(s++)=c; l=0;}
      break;
    case 1: /* . sequence, select next mode */
      if (c=='.') m=2;
      else if (c=='/') m=(l)?4:3;
      else { *(s++)='.'; *(s++)=c; m=0; l=0;}
      break;
    case 2: /* .. sequence, remove parent dir and select next mode */
      if (c=='/')
      {
	c=*s;
	*s='\0';
	if ((p=strrchr(f,'/'))) {s=p; m=3; *s='\0';}
	else if (s-f!=0) {s=f; m=0; *s='\0';}
	else {*s=c; s=o-1; m=3; } 
      }
      else {*(s++)=c; m=0; l=0;}
      break;
    case 3: /* slash sequence, back to default mode */
      if (c=='.') m=1;
      else if (c=='\0') m=5; // remove last slash
      else if (c!='/') {*(s++)='/'; *(s++)=c; m=0; l=0;}
      break;
    case 4: /* slash sequence, back to default mode */
      if (c=='.') m=1;
      else if (c!='/') {*(s++)=c; m=0; l=0;}
      break;
    }
  }
  if (m==5) --s;
  *s='\0';
  return s-f; 
}

int
itar_pop_contents_block_by_file(itar *t, char *file, char *buff, size_t s)
{
  int nfound=0;
  if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED)
    return 0;
  to_canonical_filename(file);
  itar_pop_header(t);
  while((nfound=strncmp(t->header.name,file,ITARNMAX))<0)
  {
    itar_skip_contents(t); /* TODO: I think if we put it here it would be much faster */
    if (itar_pop_header(t)<0) break;
  }
  if (nfound) return ITAR_ERR_NFOUND;
  return itar_pop_contents_block(t, buff, s);
}
/* TODO: FIXME: very urgent function itar_recover_from_nfound(itar *t) */
/*
static inline int
itar_recover_from_nfound(itar *t)
{
  printf("old state=%d header=%d!\n",ITAR(t)->state, ITAR_ST_HEADER_EXPECTED);
  return ITAR_ERR_NFOUND;
}
*/
int
itar_pop_contents_by_file(itar *t, char *file, char **buff)
{
  int nfound=0,e=0;
  *buff=NULL;
  /* if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED)
    return 0; */ // should not be checked as if the header is poped previously 
  to_canonical_filename(file);
  itar_pop_header(t);
  while((nfound=strncmp(t->header.name,file,ITARNMAX))<0)
  {
    itar_skip_contents(t); /* TODO: I think if we put it inline here it would be much faster */
    if ((e=itar_pop_header(t))<0) break;
  }
  if (nfound) return ITAR_ERR_NFOUND; // recover
  return itar_pop_contents(t, buff);
}

int
itar_pop_contents_by_file0(itar *t, char *file, char **buff)
{
  int nfound=0,e=0;
  *buff=NULL;
  /* if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED)
    return 0; */  // should not be checked as if the header is poped previously 
  to_canonical_filename(file);
  itar_pop_header(t);
  while((nfound=strncmp(t->header.name,file,ITARNMAX))<0)
  {
    itar_skip_contents(t); /* TODO: I think if we put it here it would be much faster */
    if ((e=itar_pop_header(t))<0) break;
  }
  if (nfound) return ITAR_ERR_NFOUND; // recover
  return itar_pop_contents0(t, buff);
}
/**********************************************/
int
itar_pop_contents_by_index(itar *t, int index, char **buff)
{
  int e=0;
  *buff=NULL;
  if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED)
    return 0;
  itar_pop_header(t);
  while( ITAR(t)->counter < index )
  {
    itar_skip_contents(t); /* TODO: I think if we put it inline here it would be much faster */
    if ((e=itar_pop_header(t))<0) break;
  }
  if (ITAR(t)->counter != index) return ITAR_ERR_NFOUND; // recover
  return itar_pop_contents(t, buff);
}
int
itar_pop_contents_by_index0(itar *t, int index, char **buff)
{
  int e=0;
  *buff=NULL;
  if (ITAR(t)->state != ITAR_ST_HEADER_EXPECTED)
    return 0;
  itar_pop_header(t);
  while( ITAR(t)->counter < index )
  {
    itar_skip_contents(t); /* TODO: I think if we put it inline here it would be much faster */
    if ((e=itar_pop_header(t))<0) break;
  }
  if (ITAR(t)->counter != index) return ITAR_ERR_NFOUND; // recover
  return itar_pop_contents0(t, buff);
}
/**********************************************/

int
itar_add_index_after_bytes(itar *t, int bytes)
{
  if (!ITAR(t)->mode) return -1;
  ITAR(t)->i_size=bytes;
  return 0;
}
int
itar_push_header_(itar *t, tar_buffer *buff)
{
  int i,chk=0;
  unsigned char *ptr=buff->buffer;
  if (ITAR(t)->d_size==0)  {/* add it at iTar file begining */
  fprintf(ITAR(t)->index,"0x%X\t0x%X\t%s\n",ITAR(t)->W.done,ITAR(t)->counter,t->header.name);
  }
  if (ITAR(t)->W.done-ITAR(t)->c_size >= ITAR(t)->i_size)
  {
    ITAR(t)->c_size=ITAR(t)->W.done;
    itar_c_write_init(t);
    fprintf(ITAR(t)->index,"0x%X\t0x%X\t%s\n",ITAR(t)->W.done,ITAR(t)->counter,t->header.name);
  }
  /* memset(ptr=buff->buffer+148, 0x20, 7); // replaced by two lines */
  *((int *)(buff->buffer+148))=0x20202020;
  *((int *)(buff->buffer+148+4))=0x20202020;
  for (i=0;i<512;++i,++ptr) chk+=*ptr;
  setoct(buff->header.chksum,7,chk);
  if (ITAR(t)->files_ls) fprintf(ITAR(t)->files_ls,"%s\n",t->header.name);
  ++(ITAR(t)->counter);
  return itar_c_write(t, buff->buffer, BLOCKSIZE);
}
int
itar_push_header(itar *t)
{
  tar_buffer buff;
  int l;
  memset(buff.buffer, 0, BLOCKSIZE);
  setoct(buff.header.mode,8, t->header.mode);
  setoct(buff.header.mtime,12,t->header.mtime);
  setoct(buff.header.uid,8,t->header.uid);
  setoct(buff.header.gid,8,t->header.gid);
  setoct(buff.header.devmajor,8,t->header.devmajor);
  setoct(buff.header.devminor,8,t->header.devminor);
  buff.header.typeflag=t->header.typeflag;
  strncpy(buff.header.magic,TMAGIC, TMAGLEN);
  //strncpy(buff.header.version, TVERSION, TVERSLEN);
  strncpy(buff.header.name,t->header.name, TARNMAX); // TODO: handle long filenames
  //strncpy(ITAR(t)->last_file, buff.header.name, ITARNMAX); 
  //ITAR(t)->last_file[ITARNMAX_]=0; /* TODO: no more needed, use t->header->name instead */
  strncpy(buff.header.linkname, t->header.linkname, TARNMAX);
  strncpy(buff.header.uname, t->header.uname, 32);
  strncpy(buff.header.gname, t->header.gname, 32);

  if (t->header.name[0]==0) ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  else switch(t->header.typeflag)
  {
    case DIRTYPE:
    case LNKTYPE:    
      t->header.size=0;
      setoct(buff.header.size,12,t->header.size);
      ITAR(t)->r=t->header.size;
      ITAR(t)->pad = 0;      
      ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
      break;
    case REGTYPE:
    case AREGTYPE:
      setoct(buff.header.size,12,t->header.size);
      ITAR(t)->r=t->header.size;
      if (ITAR(t)->r) ITAR(t)->pad = (512- (ITAR(t)->r & 511 ))& 511;
      else ITAR(t)->pad = 0;
      ITAR(t)->state=ITAR_ST_BLOCK_EXPECTED;
      break;
    default:
      ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
      return ITAR_ERR_NIMP;
  }
  return l=itar_push_header_(t, &buff);
}
int
itar_push_eof_blocks(itar *t)
{
  tar_buffer buff;
  if (ITAR(t)->state!=ITAR_ST_HEADER_EXPECTED) return -1;
  memset(buff.buffer, 0, BLOCKSIZE);
  ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
  return itar_c_write(t, &buff.buffer, BLOCKSIZE);
}
int
itar_push_contents_block(itar *t, const char *buffer, size_t s)
{
  int l=min(s,ITAR(t)->r); /* min is used if one read 13 but file is 12*/
  char a[BLOCKSIZE]; /* for padding */
  /* TODO: better error handling (double check return code)*/
  itar_c_write(t, buffer, l); 
  /* TODO: need to check here for errors */
  ITAR(t)->r -= l;
  /* write block padding if last block */
  if (! (ITAR(t)->r))
  {
    ITAR(t)->state=ITAR_ST_HEADER_EXPECTED;
    if (ITAR(t)->pad)
    {
      memset(a,0,BLOCKSIZE);
      ITAR(t)->pad-=itar_c_write(t, a, ITAR(t)->pad);
    }
  }
  return l;
}
int
itar_push_file(itar *t, const char *member_name,const char *file)
{
  /* prepare header */
  struct stat st;
  struct passwd pw_buf,*pwbufp=NULL;
  struct group gbuf,*gbufp=NULL;
  FILE *f;
  char *buff;
  int l,fn;
  char a[BLOCKSIZE],b[BLOCKSIZE];
  memset(&(t->header), 0, sizeof(itar_header));
  l=strlen(member_name); /* FIXME: check if len >=ITARNMAX_ then err */
  strncpy(t->header.name,member_name,ITARNMAX); t->header.name[ITARNMAX_]=0;
  if (stat(file, &st)<0) return ITAR_ERR_NFOUND;
  t->header.mode=(st.st_mode) & 07777; /* FIXME: something need to be done instead of hardcoding */
  t->header.uid=st.st_uid;
  t->header.gid=st.st_gid;
  t->header.size=st.st_size;
  t->header.mtime=st.st_mtime;
  if (S_ISREG(st.st_mode)) t->header.typeflag=REGTYPE;
  else if (S_ISDIR(st.st_mode)) {t->header.typeflag=DIRTYPE; t->header.name[l]='/'; t->header.name[++l]='\0';}
  /* else if (S_ISLNK(st.st_mode)) t->header.typeflag=LNKTYPE; */
  else return ITAR_ERR_NIMP;
  if (getpwuid_r (t->header.uid, &pw_buf, a, BLOCKSIZE, &pwbufp)!=0) ; /* FIXME: handle error */
  if (getgrgid_r (t->header.gid, &gbuf, b, BLOCKSIZE, &gbufp)!=0) ;
  strncpy(t->header.uname,pw_buf.pw_name,31); /* the 32 null terminator is set by memset*/
  strncpy(t->header.gname,gbuf.gr_name,31);
  /* push the header */
  itar_push_header(t); /* FIXME: handle error */
  if (S_ISDIR(st.st_mode)) return 0;
  if (!(f=fopen(file,"rb"))) return ITAR_ERR_OPEN;
#ifndef NO_MMAP_R
  fn=fileno(f);
  buff=mmap (0, t->header.size,PROT_READ,MAP_SHARED , fn, 0);
  if (buff!=MAP_FAILED)
  {
    l=itar_push_contents_block(t, buff, t->header.size);
    //itar_push_eof_blocks(t);
    munmap(buff, t->header.size);
    fclose(f);
    return l;
  } else {
#endif
   if (!(buff=xxalloc(t->header.size))) {fprintf(stderr,"libitar: failed to allocate memory\n"); return -1;}
   if (fread(buff,t->header.size,1,f)!=1) {xxfree(buff); fprintf(stderr,"libitar: could not read\n"); return -1;}
   l=itar_push_contents_block(t, buff, t->header.size);
   fclose(f);
   xxfree(buff);
   return l;
#ifndef NO_MMAP_R
  } /* end else */
#endif
  fclose(f);
  return -1;
}

/* NOTE: I liked to use ftw family of functions but I did not find a thread safe way
to pass (itar *t) 
  // need #include<ftw.h>
  // FIXME: how to make it thread safe as we should pass itar *t but 
  // FTW_PHYS do not followed slinks, FTW_MOUNT same mounted device, FTW_CHDIR (ch predix before and cd - after) FTW_DEPTH depth_first (MUST NOT BE USED) 
  int i=nftw (prefix, fs_tree_cb, descriptors, flag);
 */

/* Skip "", ".", and "..".  "" is returned by at least one buggy implementation: Solaris 2.4 readdir on NFS filesystems. */
int itar_scandir_select_all(const struct dirent *dp)
{
  return (dp->d_name[dp->d_name[0] != '.' ? 0 : dp->d_name[1] != '.' ? 1 : 2 ] != '\0')?1:0;
}
/* cmp is used instead of strcol as the last one depends on local and this is not what we want
   we want fast unified way, compate byte by byte not char by char
*/
int itar_scandir_cmp(const struct dirent **a, const struct dirent **b)
{
  return strcmp((*a)->d_name, (*b)->d_name);
}
int
itar_push_fs_tree_(itar *t, const char *memberdir, const char *dir)
{
  struct dirent **eps;
  struct stat st;
  char *fn,*mn;
  int r,i,l,L,n=scandir (dir, &eps, itar_scandir_select_all, itar_scandir_cmp);
  if (!(mn=xxalloc(ITARNMAX))) return ITAR_ERR_MEM;
  mn[0]=0; strncat(mn, dir, ITARNMAX_); mn[ITARNMAX_]='\0'; /* as strncat is faster than strncpy*/
  if (!(fn=xxalloc(ITARNMAX))) {xxfree(mn); return ITAR_ERR_MEM;}
  fn[0]=0; strncat(fn, dir, ITARNMAX_); fn[ITARNMAX_]='\0'; /* as strncat is faster than strncpy*/
  L=strlen(fn);
  /* FIXME: does both fn="foo/bar/." and fn="foo/bar/./" work ? */
  l=to_canonical_filename(mn); /* this is done once */
  if (n<0) return ITAR_ERR_OPEN;
  for (i=0;i<n;++i)
  {
    mn[l]='/'; mn[l+(l!=0)]='\0'; /* mn[l]=(l)?'/':'\0'; */
    strncat(mn+l+(l!=0), eps[i]->d_name, ITARNMAX_); mn[ITARNMAX_]='\0'; /* as strncat is faster than strncpy*/
    fn[L]='/'; fn[L+(L!=0)]='\0';
    strncat(fn+L+(L!=0), eps[i]->d_name, ITARNMAX_); fn[ITARNMAX_]='\0'; /* as strncat is faster than strncpy*/
    printf("libitar: %d adding [%s] as [%s]\n",i, fn, mn);
    if ( (r=itar_push_file(t, mn, fn)) <0) { xxfree(fn); xxfree(mn); return r;}
    /* FIXME: I don't know why this don't just work: if (eps[i]->d_type==DT_DIR ...etc */
    if (stat(fn, &st)<0) { xxfree(fn); xxfree(mn); return ITAR_ERR_NFOUND;}
    if (S_ISDIR(st.st_mode) && (r=itar_push_fs_tree_(t, mn, fn)) <0) { xxfree(fn); xxfree(mn); return r;}
    free (eps[i]);
    mn[l]='\0';
    fn[L]='\0';
  }
  free (eps);
  xxfree(fn);
  xxfree(mn);
  return 0;
}
int
itar_push_fs_tree(itar *t, const char *dir, int skip_offset)
{
  char *mn;
  int l,r;
  if (!(mn=xxalloc(ITARNMAX))) return ITAR_ERR_MEM;
  strncpy(mn, dir+skip_offset, ITARNMAX); mn[ITARNMAX_]='\0';
  l=to_canonical_filename(mn); /* this is done once */
  printf("libitar: adding [%s] as [%s]\n",dir, mn);
  r=itar_push_fs_tree_(t, mn, dir);
  xxfree(mn);
  return r;
}

/*
int
fs_tree_cb(const char *name, const struct stat *st, int flag, struct FTW *fwt)
{

}
int
itar_push_fs_tree(itar *t, const char *prefix, int slash_skip)
{
  // need #include<ftw.h>
  // FIXME: how to make it thread safe as we should pass itar *t but 
  // FTW_PHYS do not followed slinks, FTW_MOUNT same mounted device, FTW_CHDIR (ch predix before and cd - after) FTW_DEPTH depth_first (MUST NOT BE USED) 
  int i=nftw (prefix, fs_tree_cb, descriptors, flag);
  return -1;
}
*/
/**********************************************/
/**********************************************/
#ifndef NO_MMAP_W
void *my_mremap (void *p1, size_t s1, size_t s2,int fd)
{
  if (s1==s2 && p1) return p1;
#ifdef NO_MREMAP
  if (p1) munmap(p1, s1);
  return mmap (p1, s2, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
#else
  return mremap (p1, s1, s2, MREMAP_MAYMOVE);
#endif
}
#endif
/* benchmarking: mmap vs fread/fwrite/...etc
time for i in `seq 15`; do ./itar_w ../../delme.tar.bz2 *; done
no mmap at all (reading and writing) when compiled with -U _POSIX_MAPPED_FILES or -D NO_MMAP_W -D NO_MMAP_R -D NO_MREMAP
real    0m27.418s
user    0m25.819s
sys     0m0.685s

with mmap file reader and fwrite compressor (compiled with -D NO_MMAP_W)
result:
real    0m27.269s
user    0m25.810s
sys     0m0.536s

with both mmap for reader and compressor
usual compile in a mmap capable system
real    0m27.128s
user    0m25.710s
sys     0m0.540s

using real GNU tar -cvjf 
real    0m27.348s
user    0m25.726s
sys     0m0.537s

conclusion difference is marginal
*/
/* FIXME: do we need a hardcoded balue instead like 2MB*/
#define mmap_window_size(t) ((min(ITAR(t)->i_size,1<<15))<<6)
/* TODO: better error checking (eg. for ftruncate)*/
int
itar_w_open(itar *t,int min_size)
{
  int fd=fileno(t->f),m;
#ifndef NO_MMAP_W
  /* m is used to test if mmap is available, as it will be remapped later */
  ITAR(t)->W.done=0;
  m=mmap_window_size(t);
  if ((ITAR(t)->W.once=mmap (0, m, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0))!=(void *)-1)
  {
    min_size=m;
    ftruncate(fd, min_size);
    ITAR(t)->W.typ=ITAR_W_MMAP;
    ITAR(t)->W.min_size=min_size;
  } else {
#endif
  ITAR(t)->W.min_size=min_size;
  ITAR(t)->W.typ=ITAR_W_MEM;
  if (!(ITAR(t)->W.once=malloc(min_size))) return ITAR_ERR_MEM;
#ifndef NO_MMAP_W
  }
#endif
  ITAR(t)->W.ptr=ITAR(t)->W.once;
  ITAR(t)->W.size=ITAR(t)->W.min_size;
  return 0;
}
int
itar_w_close(itar *t)
{
#ifndef NO_MMAP_W
  if (ITAR(t)->W.typ==ITAR_W_MEM)
  {
#endif
    if (ITAR(t)->W.once) free(ITAR(t)->W.once);
#ifndef NO_MMAP_W
  } else {
    if (ITAR(t)->W.once)
    {
      ftruncate(fileno(t->f), ITAR(t)->W.done);
      munmap(ITAR(t)->W.once, ITAR(t)->W.min_size);
    }
  }
#endif
  ITAR(t)->W.once=0;
  return 0;
}
int
itar_w_init(itar *t,int size)
{
#ifndef NO_MMAP_W
  int fd=fileno(t->f);
  char *p;
  if (ITAR(t)->W.typ==ITAR_W_MEM)
  {
#endif
    if (size<=ITAR(t)->W.min_size) ITAR(t)->W.ptr=ITAR(t)->W.once;
    else if (!(ITAR(t)->W.ptr=malloc(size))) return ITAR_ERR_MEM;
#ifndef NO_MMAP_W
  } else if (ITAR(t)->W.min_size < ITAR(t)->W.done+size) { /* need a check if this is actually needed */
    size=max(mmap_window_size(t),size);
    //printf("expanding from %d to %d\n",ITAR(t)->W.min_size,ITAR(t)->W.done+size);
    p=my_mremap (ITAR(t)->W.once, ITAR(t)->W.min_size, ITAR(t)->W.done+size, fd);
    if (p==(void *)-1) return ITAR_ERR_MEM;
    ITAR(t)->W.once=p;
    ftruncate(fd, ITAR(t)->W.done+size);
    ITAR(t)->W.min_size=ITAR(t)->W.done+size;
    ITAR(t)->W.ptr=ITAR(t)->W.once+ITAR(t)->W.done;
  } else ITAR(t)->W.ptr=ITAR(t)->W.once+ITAR(t)->W.done;
#endif
  ITAR(t)->W.size=size;
  return 0;
}
int
itar_w_save(itar *t,int size)
{
#ifndef NO_MMAP_W
  if (ITAR(t)->W.typ==ITAR_W_MEM)
  {
#endif
    if (size && fwrite(ITAR(t)->W.ptr,size,1,t->f)!=1) return ITAR_ERR_BAD;
    if (ITAR(t)->W.ptr!=ITAR(t)->W.once) free(ITAR(t)->W.ptr);
    ITAR(t)->W.done+=size;
#ifndef NO_MMAP_W
  } else {
    ITAR(t)->W.done+=size;
    // fseek(t->f,ITAR(t)->W.done,SEEK_SET); /* TODO: rewrite functions not to use fseek/ftell and relay on ->done*/
  }
#endif
  return 0;
}

/**********************************************
 * private compressor wrappers implementation *
 **********************************************/
int itar_c_init(itar *p)
{
  switch(ITAR(p)->type)
  {
  case ITAR_BZ:
    ITAR(p)->itar_c_read_init=itar_bz_read_init;
    ITAR(p)->itar_c_read=itar_bz_read;
    ITAR(p)->itar_c_read_close=itar_bz_read_close;
    ITAR(p)->itar_c_write_init=itar_bz_write_init;
    ITAR(p)->itar_c_write=itar_bz_write;
    return itar_bz_init(p);
    break;
  default:
    return ITAR_ERR_NIMP;
  }
  return ITAR_ERR_NIMP;
}


int itar_c_close(itar *p)
{
  if (ITAR(p)->type==ITAR_BZ) return itar_bz_close(p);
  return ITAR_ERR_NIMP;
}
