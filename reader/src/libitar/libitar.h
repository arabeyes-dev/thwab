/**
 * LibITAR - routines to access compressed indexed TAR archives
 * 
 * Random access compressed TAR files
 * 	by Muayyad Saleh Alsadi<alsadi@gmail.com>
 *	Released under terms of GNU GPL
 *
 * The archive is compatible with GZ/BZiped TAR format
 * and can be extracted (or listed...etc) with unaware usual tools
 * but it generate an index file which makes it very fast to extract
 * a random file at a random position
 **/

#ifndef _LIBITAR_
#define _LIBITAR_
#define _GNU_SOURCE	/* 1st to get rid of non-GNU, 2nd to have strndup and alloca */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <errno.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <utime.h>
/* from untgz.c (in zlib/contrib) */
/* refere to GNU TAR Info page (Node: Standard)*/
#define REGTYPE  '0'		/* regular file */
#define AREGTYPE '\0'		/* regular file */
#define LNKTYPE  '1'		/* link */
#define SYMTYPE  '2'		/* reserved */
#define CHRTYPE  '3'		/* character special */
#define BLKTYPE  '4'		/* block special */
#define DIRTYPE  '5'		/* directory */
#define FIFOTYPE '6'		/* FIFO special */
#define CONTTYPE '7'		/* reserved */
#define BLOCKSIZE 512
#define TARNMAX	100
#define ITARNMAX_	MAXNAMLEN
#define ITARNMAX	(MAXNAMLEN+1)
#define  NO_ALLOCA
/* from Linux Programmer’s Manual on realplath(3)
#ifdef PATH_MAX
path_max = PATH_MAX;
#else
path_max = pathconf (path, _PC_PATH_MAX);
if (path_max <= 0) path_max = 4096;
#endif
*/
typedef struct 
{				/* byte offset */
  char name[TARNMAX];		/*   0 */
  char mode[8];			/* 100 */
  char uid[8];			/* 108 */
  char gid[8];			/* 116 */
  char size[12];		/* 124 */
  char mtime[12];		/* 136 */
  char chksum[8];		/* 148 */
  char typeflag;		/* 156 */
  char linkname[TARNMAX];	/* 157 */
  char magic[8];		/* 257 */
  char uname[32];		/* 265 */
  char gname[32];		/* 297 */
  char devmajor[8];		/* 329 */
  char devminor[8];		/* 337 */
  char prefix[155];		/* 345 */
  /* 500 */
} tar_header;
#define TMAGIC   "ustar  "        /* "ustar" space space and a null */
#define TMAGLEN  8

typedef union
{
  char buffer[BLOCKSIZE];
  tar_header header;
} tar_buffer;
/*
 * name and linkname should be of size NAME_MAX as defined by <dirent.h> or <sys/types.h>
 * but POSIX TAR define TAR header so that file name, TODO: How GNU solve it ?!
 */
typedef struct 
{
  char		name[ITARNMAX];
  mode_t	mode;
  uid_t		uid;
  gid_t		gid;
  size_t	size;
  time_t	mtime;
  int		chksum;
  char		typeflag;
  char		linkname[ITARNMAX];
  char		uname[32];
  char		gname[32];
  short		devmajor;
  short		devminor;	/*this should be dev_t but how to convert to it */
} itar_header;
enum itar_type {ITAR_GZ, ITAR_BZ};
enum itar_err
{
  ITAR_ERR_NIMP=-11, ITAR_ERR_NFOUND, ITAR_ERR_MEM, ITAR_ERR_OPEN,
  ITAR_ERR_READ, ITAR_ERR_BAD, ITAR_ERR_CHKSUM,ITAR_ERR_CLOSE
};

typedef struct
{
  int		n;
  size_t	*offsets;
  int		*indices;
  char	**keys;
} itar_itable;

typedef struct
{
  FILE		*f;
  itar_itable	itb;
  itar_header	header;
  void		*opaque; /* private opaque structure */
} itar;


itar		*itar_open(const char *file, const char *mode);
itar		*itar_open_full(const char *file,const char *mode, const char *index);  /* index or ls could be set to NULL */
int		itar_load_indices(itar *t, FILE *F); /* called only once */
int		itar_set_index_file(itar *t, const char *index_file); /* called only once */
int		itar_close(itar *t);
/* information functions */
int		itar_get_n_files(itar *t); /* 0 or -ve for error */
const char	*itar_get_filename_by_index(itar *t, int file_index);
int		itar_get_index_by_filename(itar *t, char *filename); /* 0 or -ve for error */

/*
 * itar_pop_contents_by_file0 and alike functions allocate size+2 buffer
 * and load the content plus "\n\0" for easier parsing
 * and to assure being a valid null terminated C string
 */
int		itar_pop_header(itar *t); /* set t->header */
int		itar_pop_contents_by_file(itar *t, char *file, char **buff);
int		itar_pop_contents_by_file0(itar *t, char *file, char **buff);  // add "\n\0" after the end of file
int		itar_pop_contents_by_index(itar *t, int file_index, char **buff);
int		itar_pop_contents_by_index0(itar *t, int file_index, char **buff); // add "\n\0" after the end of file

int		itar_pop_contents(itar *t, char **buffer);
int		itar_pop_contents0(itar *t, char **buffer); // add "\n\0" after the end of file
int		itar_pop_contents_block(itar *t, char *buffer, size_t s);
int		itar_pop_contents_block_by_file(itar *t, char *file, char *buff, size_t s);

int		itar_seek_by_offset(itar *t, size_t offset);
int		itar_seek_by_key(itar *t, const char *key);
int		itar_seek_by_file(itar *t, const char *file);
int		itar_seek_by_index(itar *t, int file_index);

int		itar_push_fs_tree(itar *t, const char *prefix, int skip_offset); // very high level
int		itar_push_file(itar *t, const char *member_name, const char *file); // get file name,time...etc from fs

int		itar_save_indices(itar *t, const char *index_file);
int		itar_add_index_after_bytes(itar *t, int bytes);

int		itar_push_header(itar *t);
int		itar_push_contents_block(itar *t, const char *buffer, size_t s);
int		itar_push_eof_blocks(itar *t); /* needed after last block */
#endif
