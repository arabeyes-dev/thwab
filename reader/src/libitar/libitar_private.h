/**
 * LibITAR - routines to access compressed indexed TAR archives
 * 
 * Random access compressed TAR files
 * 	by Muayyad Saleh Alsadi<alsadi@gmail.com>
 *	Released under terms of GNU GPL
 *
 * The archive is compatible with GZ/BZiped TAR format
 * and can be extracted (or listed...etc) with unaware usual tools
 * but it generate an index file which makes it very fast to extract
 * a random file at a random position
 **/

#ifndef _LIBITAR_PRI_
#define _LIBITAR_PRI_

/* itar_w: allocate and write to the file using mmap if posible */
typedef struct
{
  char typ,*ptr;
  int size;
  /* private */
  int min_size,done; /* with mmap min_size = size if no yet done+=size after that it's 0  */
  char *once; /* with mmap used as mmaped ptr, while ptr=once+offset*/
} itar_w;
enum itar_w_typ {ITAR_W_MMAP,ITAR_W_MEM};

typedef struct
{
  FILE		*index,*files_ls;	/* index file stream (used only in writing mode)*/
  char		**ls;		/* filelist array used in read mode*/
  int		ls_n;		/* number of files in filelist array */
  char		mode;		/* 0 read, 1 write*/
  char		type;		/* compression type */
  char		state;		/* header or block is coming next */
  //char		last_file[ITARNMAX]; /* last file read, saved for faster seeking, and used by c_write4header */ /* TODO: replaced by header->name*/
  int		counter;		/* index of member file  */
  int		i_size;		/* size after which an index is saved and new chunk is created*/
  int		c_size;		/* used by Writing funcs*/
  int		d_size;		/* decompressed size, used by Writing funcs*/
  int		r;		/* size remainder */
  int		pad;		/* size of BLOCK padding*/
  void		*c;		/* private opaque compressor structure */
  itar_w	W;
  int		(*itar_c_read_init)(itar *p);
  size_t	(*itar_c_read)(itar *p, void *buffer, size_t size);
  int		(*itar_c_read_close)(itar *p);
  int		(*itar_c_write_init)(itar *p);
  int		(*itar_c_write)(itar *p, const void *buffer, size_t size);
} itar_opaque;

#define ITAR(p)	((itar_opaque*)((p)->opaque))
#include "libitar_bz.h"

enum itar_st
{
  ITAR_ST_HEADER_EXPECTED, ITAR_ST_BLOCK_EXPECTED
};

int itar_w_open(itar *t,int min_size);
int itar_w_close(itar *t);
int itar_w_init(itar *t,int size);
int itar_w_save(itar *t,int size);

/* compressor wrappers */
int		itar_c_init(itar *p);
#define itar_c_read_init(p)		(ITAR(p)->itar_c_read_init(p))
#define itar_c_read(p, buffer, size)	(ITAR(p)->itar_c_read(p, buffer, size))
#define itar_c_read_close(p)		(ITAR(p)->itar_c_read_close(p))
#define itar_c_write_init(p)	(ITAR(p)->itar_c_write_init(p))
#define itar_c_write(p, buffer, size)	(ITAR(p)->itar_c_write(p, buffer, size))
int		itar_c_write4header(itar *p, const void *buffer, size_t size);
int		itar_c_close(itar *p);

/*
 reading compressor wrappers:
 itar_c_init(..)	is called once itar_open to allocate...etc
 itar_c_read_init(..)	is called after seek
 itar_c_read(..)	is called repeteadily for reading
 itar_c_read_close(..)	is called after read sequence ends and ready to seek (just before seek)
 itar_c_close(..)	is called once itar_close to free allocated...etc
 
 */
/*
 writing compressor wrappers:
 itar_c_init(..)	 is called once itar_open to allocate...etc
 itar_c_write_init(..)	 is called before creating new chunk (by c_write4header)
 itar_c_write(..)	 is called to add data to be compressed and update offset.
 itar_c_write4header(..) is called to add main header (ie. check if new chunk is needed)
 itar_c_close(..)	 is called once by itar_close to free allocated...etc
 
 c_write should set/increment c_size to be the size of compressed data so far
 */

#endif
