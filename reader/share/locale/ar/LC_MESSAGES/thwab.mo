��    4      �  G   \      x  }   y  5   �     -  0   3     d     l     �  
   �  "   �  #   �     �  )        .     3     ;     T      [     |     �     �     �     �     �     �     �     �  !   �     �     �  .        ;  $   L     q     �     �  3   �  
   �  
   �     �     �       #     �   4  �   �  
   V	  V   d	     �	     �	     �	     �	     �	  ;  �	  �   -  6   �       N        m  #   ~  &   �     �  :   �  .   
  *   G
  [   r
     �
     �
  '   �
       $        @     G     a  -   j     �     �     �     �     �  }   �     f     x  M   �     �  6     
   9  
   G     R  M   a     �     �  +   �     
     #  7   ?  �   w  �   �     �  �   �     n     }     �  
   �  5   �              2                        3                             )   #                            0          "   ,      
          $             *   /      +         (         4       '       -      &                    	   1   %       .      
   !       <span size="xx-large" color="darkblue">Converted</span>
The book is converted to HTML in %.3f sec, it could be found in [%s]. <span size="xx-large" color="darkred">Error</span>
%s About Already converted! or at least directory exists. Authors Convert book to HTML Converting to HTML... Copyrights Could not Convert, Internal error. Could not copy HTML template files. Could not create directory Could not create files, check free space. Date Details Display book information Done!
 Error loading HTML template file Find Fuzzy Search GPG Generating Table of contents Help ID Index Info Loading ... Next startup time will be longer. No Title No topic is opened Not topic is opened, or topic has no contents. Nothing Selected Open book related to the current one Please wait... Rank Related Remove fast start cache, new one will be generated. Search Result Search by key Search in the current page Select a book! Subtitle This feature is not yet implemented This is the native C Interface, here translators should add their names like this:
	Muayyad Saleh AlSadi <alsadi[AT]gmail[DOT]com> Thwab Help is available in Thwab format, click on [Index] button then choose [Thwab Reference].
Help is also available in HTML format found in [doc] directory Thwab Library Thwab is an Electronic Encyclopedia System
 {in which there are valuable Books.} V98:3 Title Translators Valid Warning: validate book e-sign Project-Id-Version: Thwab 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-05-05 18:05+0300
PO-Revision-Date: 2007-04-26 00:08+0300
Last-Translator: Muayyad Saleh AlSadi <alsadi@gmail.com>
Language-Team: Arabic <ar@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <span size="xx-large" color="darkblue">تم التحويل</span>
تم تحويل الكتاب إلى هيئة HTML خلال %.3f ثانية، يمكنك أن تجده في [%s]. <span size="xx-large" color="darkred">خطأ</span>
%s حول محول مسبقاً! على الأقل الدليل موجود مسبقاً. المؤلفون تحويل الكتاب إلى HTML تحويل الكتاب إلى HTML... حقوق النسخ غير قادر على التحويل، خلل داخلي. غير قادر على نشخ قوالب HTML. غير قادر على عمل المجلد غير قادر على إنشاء الملفات، افحص وجود مساحة خالية. التاريخ تفاصيل عرض معلومات عن الكتاب تمّ!
 خطأ في تحميل قالب HTML بحث البحث الضبابي تحقق جار توليد جدول المحتويات مساعدة مُعرّف الفهرس معلومات جار التحميل ... سيعمل البرنامج بشكل أبطأ عند تشغيله في المرة القادمة ليقوم بالتحديث. دون عنوان لم تفتح أي كتاب لم تفتح أي موضوع أو أن الموضوع دون محتويات. لم يتم اختيار شيء فتح كتاب متعلق بالكتاب الحالي انتظر... تقييم متعلقات حذف ملفات تسريع التشغيل لتوليد أخرى جديدة. نتيجة البحث البحث المفتاحي البحث في الصفحة الحالية اختر كتاباً العنوان الفرعي هذه الميزة ليست مطبقة حتى الآن قام بترجمة واجهة البرنامج إلى العربية:
	مؤيد صالح السعدي <alsadi[AT]gmail[DOT]com> تتوفر المساعدة على هيئة ملفات ثواب، انقر [الفهرس] ثم اختر [مرجع ثواب].
كما أنه يتوفر بهيئة HTML تجدها في مجلد [doc]. مكتبة الثواب ثواب نظام الموسوعة الإلكترونية الحرة
 { فِيهَا كُتُبٌ قَيِّمَةٌ } آية 3 سورة البينة . العنوان المترجمون السلامة تحذير التحقق من التوقيع الإلكتروني 